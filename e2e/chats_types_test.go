//go:build e2e
// +build e2e

package e2e

import (
	"context"
	"testing"

	"github.com/stretchr/testify/require"
	chatapiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
)

func TestChatsTypes(t *testing.T) {
	ctx := context.Background()
	client := client(ctx)

	someChatType := chatapiv2.ChatType_Buyer_Buyer
	someUserType := chatapiv2.UserType_Seller

	_, err := client.SetChatsTypes(ctx, &apiv1.SetChatsTypesRequest{
		ChatType: someChatType,
		UserType: someUserType,
		Settings: settings,
	})
	require.NoError(t, err)

	_, err = client.DeleteChatTypeSettings(ctx, &apiv1.DeleteChatTypeSettingsRequest{
		ChatType:     someChatType,
		UserType:     someUserType,
		SettingNames: []string{boolSettingKey, intSettingKey},
	})
	require.NoError(t, err)

	resp, err := client.GetChatsTypes(ctx, &apiv1.GetChatsTypesRequest{
		ChatsTypes: []chatapiv2.ChatType{someChatType},
		UsersTypes: []chatapiv2.UserType{someUserType},
	})
	require.NoError(t, err)
	require.Nil(t, resp.ChatTypesSettings[0].Settings.Settings[boolSettingKey])
	require.Nil(t, resp.ChatTypesSettings[0].Settings.Settings[intSettingKey])
	require.Equal(t,
		settings[stringSettingKey].Value.GetString_(),
		resp.ChatTypesSettings[0].Settings.Settings[stringSettingKey].Value.GetString_(),
	)

	_, err = client.DeleteChatTypeSettings(ctx, &apiv1.DeleteChatTypeSettingsRequest{
		ChatType:     someChatType,
		UserType:     someUserType,
		SettingNames: allSettingKey,
	})
	require.NoError(t, err)

	resp, err = client.GetChatsTypes(ctx, &apiv1.GetChatsTypesRequest{
		ChatsTypes: []chatapiv2.ChatType{someChatType},
		UsersTypes: []chatapiv2.UserType{someUserType},
	})
	require.NoError(t, err)
	require.Len(t, resp.ChatTypesSettings[0].Settings.Settings, 0)
}
