//go:build e2e
// +build e2e

package e2e

import (
	"context"
	"testing"

	"github.com/pborman/uuid"
	"github.com/stretchr/testify/require"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	mw "gitlab.ozon.ru/platform/scratch/pkg/mw/grpc"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/grpc"
)

const (
	boolSettingKey    = "bool-setting"
	intSettingKey     = "int-setting"
	floatSettingKey   = "float-setting"
	stringSettingKey  = "string-setting"
	stringsSettingKey = "strings-setting"

	testKeyPrefix = "test_"
)

var (
	allSettingKey = []string{boolSettingKey, intSettingKey, floatSettingKey, stringSettingKey, stringsSettingKey}

	settings = map[string]*apiv1.SetSetting{
		boolSettingKey: {
			Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}},
			Tags:  []string{"test-web", "test-mob"},
		},
		intSettingKey: {
			Value: &apiv1.Value{Value: &apiv1.Value_Int{Int: 42}},
			Tags:  []string{"test-web", "test-mob"},
		},
		floatSettingKey: {
			Value: &apiv1.Value{Value: &apiv1.Value_Float{Float: 42.2}},
			Tags:  []string{"test-web", "test-mob"},
		},
		stringSettingKey: {
			Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "string1"}},
			Tags:  []string{"test-web", "test-mob"},
		},
		stringsSettingKey: {
			Value: &apiv1.Value{Value: &apiv1.Value_Strings_{Strings: &apiv1.Value_Strings{Values: []string{"string1", "string2"}}}},
			Tags:  []string{"test-web", "test-mob"},
		},
	}
)

func client(ctx context.Context) apiv1.SettingsV1Client {
	//settingsConn, err := mw.Dial("messenger-settings-release-chat-2463.stg.a.o3.ru:82", grpc.WithInsecure())
	settingsConn, err := mw.Dial("localhost:7002", grpc.WithInsecure())
	if err != nil {
		logger.Fatalf(ctx, "can't create connection err=%v", err)
	}
	return apiv1.NewSettingsV1Client(settingsConn)
}

func TestChats(t *testing.T) {
	ctx := context.Background()
	client := client(ctx)

	chatID := testKeyPrefix + uuid.New()

	_, err := client.SetChats(ctx, &apiv1.SetChatsRequest{
		ChatId:   chatID,
		Settings: settings,
	})
	require.NoError(t, err)

	_, err = client.DeleteChatSettings(ctx, &apiv1.DeleteChatSettingsRequest{
		ChatId:       chatID,
		SettingNames: []string{boolSettingKey, intSettingKey},
	})
	require.NoError(t, err)

	resp, err := client.GetChats(ctx, &apiv1.GetChatsRequest{ChatsIds: []string{chatID}})
	require.NoError(t, err)

	require.Nil(t, resp.ChatSettingsByChatsIds[chatID].Settings.Settings[boolSettingKey])
	require.Nil(t, resp.ChatSettingsByChatsIds[chatID].Settings.Settings[intSettingKey])
	require.Equal(t,
		settings[stringSettingKey].Value.GetString_(),
		resp.ChatSettingsByChatsIds[chatID].Settings.Settings[stringSettingKey].Value.GetString_(),
	)
}
