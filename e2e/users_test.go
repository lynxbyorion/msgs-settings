//go:build e2e
// +build e2e

package e2e

import (
	"context"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	chatapiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
)

func TestUsers(t *testing.T) {
	ctx := context.Background()
	client := client(ctx)

	user := &chatapiv2.User{
		Type: chatapiv2.UserType_Seller,
		Id:   testKeyPrefix + strconv.FormatInt(time.Now().Unix(), 10),
	}

	_, err := client.SetUsers(ctx, &apiv1.SetUsersRequest{
		User:     user,
		Settings: settings,
	})
	require.NoError(t, err)

	delResp, err := client.DeleteUserSettings(ctx, &apiv1.DeleteUserSettingsRequest{
		User:         user,
		SettingNames: []string{boolSettingKey, intSettingKey},
	})
	require.NoError(t, err)
	require.ElementsMatch(t, delResp.Deleted, []string{boolSettingKey, intSettingKey})

	resp, err := client.GetUsers(ctx, &apiv1.GetUsersRequest{
		Users: []*chatapiv2.User{user},
	})
	require.NoError(t, err)

	require.Len(t, resp.UsersSettings, 1)
	require.Equal(t, user.Type, resp.UsersSettings[0].User.Type)
	require.Equal(t, strings.ToLower(user.Id), resp.UsersSettings[0].User.Id)
	require.Nil(t, resp.UsersSettings[0].Settings.Settings[boolSettingKey])
	require.Nil(t, resp.UsersSettings[0].Settings.Settings[intSettingKey])
	require.Equal(t,
		settings[stringSettingKey].Value.GetString_(),
		resp.UsersSettings[0].Settings.Settings[stringSettingKey].Value.GetString_(),
	)

	_, err = client.DeleteUserSettings(ctx, &apiv1.DeleteUserSettingsRequest{User: user, SettingNames: allSettingKey})
	require.NoError(t, err)

	resp, err = client.GetUsers(ctx, &apiv1.GetUsersRequest{Users: []*chatapiv2.User{user}})
	require.NoError(t, err)
	require.Len(t, resp.UsersSettings, 1)
	require.Len(t, resp.UsersSettings[0].Settings.Settings, 1) // default push-enable
}
