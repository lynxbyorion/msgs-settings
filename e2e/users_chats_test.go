//go:build e2e
// +build e2e

package e2e

import (
	"context"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	chatapiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
)

func TestUsersChats(t *testing.T) {
	ctx := context.Background()
	client := client(ctx)

	chatID := testKeyPrefix + strconv.FormatInt(time.Now().Unix(), 10)
	user := &chatapiv2.User{
		Type: chatapiv2.UserType_Seller,
		Id:   testKeyPrefix + strconv.FormatInt(time.Now().Unix(), 10),
	}

	_, err := client.SetUsersChats(ctx, &apiv1.SetUsersChatsRequest{
		ChatId:   chatID,
		User:     user,
		Settings: settings,
	})
	require.NoError(t, err)

	_, err = client.DeleteUsersChatsSettings(ctx, &apiv1.DeleteUsersChatsSettingsRequest{
		User:         user,
		ChatId:       chatID,
		SettingNames: []string{boolSettingKey, intSettingKey},
	})
	require.NoError(t, err)

	resp, err := client.GetUsersChats(ctx, &apiv1.GetUsersChatsRequest{
		UsersChatsIds: []*apiv1.GetUsersChatsRequest_UserChatsIDs{{User: user, ChatsIds: []string{chatID}}},
	})
	require.NoError(t, err)
	require.Len(t, resp.UsersChatsSettings, 1)
	require.Equal(t, user.Type, resp.UsersChatsSettings[0].User.Type)
	require.Equal(t, strings.ToLower(user.Id), resp.UsersChatsSettings[0].User.Id)
	require.Nil(t, resp.UsersChatsSettings[0].Settings.Settings[boolSettingKey])
	require.Nil(t, resp.UsersChatsSettings[0].Settings.Settings[intSettingKey])
	require.Equal(t,
		settings[stringSettingKey].Value.GetString_(),
		resp.UsersChatsSettings[0].Settings.Settings[stringSettingKey].Value.GetString_(),
	)

	_, err = client.DeleteUserSettings(ctx, &apiv1.DeleteUserSettingsRequest{User: user, SettingNames: allSettingKey})
	require.NoError(t, err)

	resp, err = client.GetUsersChats(ctx,
		&apiv1.GetUsersChatsRequest{UsersChatsIds: []*apiv1.GetUsersChatsRequest_UserChatsIDs{{User: user, ChatsIds: []string{chatID}}}})
	require.NoError(t, err)
	require.Len(t, resp.UsersChatsSettings, 1)
}
