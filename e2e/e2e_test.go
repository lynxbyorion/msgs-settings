//go:build e2e
// +build e2e

// nolint
package e2e

import (
	"context"
	"fmt"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	chatapiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	mw "gitlab.ozon.ru/platform/scratch/pkg/mw/grpc"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/grpc"
)

const (
	pushEnable = "push-enable"

	someUserID   = "1"
	someUserChat = "11111111-1111-1111-1111-111111111111"

	userID177061 = "177061"

	userID177072                  = "177072"
	chatWithSupportOfUserID177072 = "ccccc88b-50da-45a2-bab8-118e340fc8d2"
	//chatWithTravelOfUserId177070  = "dc05eb0c-28d8-447e-9f2e-0653e8a9b06f"
	//chatWithSeller2OfUserID177070 = "77e177e6-a842-4d43-8f00-b4888442226f"

	userID177071                  = "177071"
	chatWithSupportOfUserID177071 = "e7b96af9-d6b3-4aec-85b4-631ea27db466"

	userID177073DoNotUseInSet = "177073"
	//chatWithSupportOfUserID177073              = "2c8a98c4-ee27-4882-96ae-2ea801e04971"
	chatWithSeller2OfUserID177073DoNotUseInSet = "bcb61698-b03e-4e88-97b4-dd65df2de0f6"

	userID2                        = "2"
	chatOfCustomer177070AndSeller2 = "77e177e6-a842-4d43-8f00-b4888442226f"

	userID20DoNotUseInSet = "20"

	chatDoNotSetAnySettings = "777777e6-a888-4443-0000-888884422222"

	userIDBotDoNotUseInSet = "bot"

	chatToSetTwoSettings = "chat_where_we_set_2_settings_ho-ho-o"
)

func TestE2E(t *testing.T) {
	ctx := context.Background()
	//settingsConn, err := mw.Dial("messenger-settings-release-chat-2463.stg.a.o3.ru:82", grpc.WithInsecure())
	settingsConn, err := mw.Dial("localhost:7002", grpc.WithInsecure())
	if err != nil {
		logger.Fatalf(ctx, "can't create connection err=%v", err)
	}

	var (
		client   = apiv1.NewSettingsV1Client(settingsConn)
		someUser = &chatapiv2.User{
			Type: chatapiv2.UserType_Customer,
			Id:   someUserID,
		}
		someSettings = map[string]*apiv1.SetSetting{
			"push": {
				Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}},
				Tags:  []string{"web"},
			},
		}

		user177061 = &chatapiv2.User{
			Type: chatapiv2.UserType_Customer,
			Id:   userID177061,
		}
		user177072 = &chatapiv2.User{
			Type: chatapiv2.UserType_Customer,
			Id:   userID177072,
		}
		user177071 = &chatapiv2.User{
			Type: chatapiv2.UserType_Customer,
			Id:   userID177071,
		}
		user177073DoNotUseInSet = &chatapiv2.User{
			Type: chatapiv2.UserType_Customer,
			Id:   userID177073DoNotUseInSet,
		}
		user2 = &chatapiv2.User{
			Type: chatapiv2.UserType_Seller,
			Id:   userID2,
		}
		user20DoNotUseInSet = &chatapiv2.User{
			Type: chatapiv2.UserType_Seller,
			Id:   userID20DoNotUseInSet,
		}

		userBotDoNotUseInSet = &chatapiv2.User{
			Type: chatapiv2.UserType_ChatBot,
			Id:   userIDBotDoNotUseInSet,
		}

		mailFrequencySettingTo10 = map[string]*apiv1.SetSetting{
			"mail-frequency": {
				Value: &apiv1.Value{Value: &apiv1.Value_Int{Int: 10}},
			},
		}

		pushEnabledBecomesFloatSettings = map[string]*apiv1.SetSetting{
			pushEnable: {
				Value: &apiv1.Value{Value: &apiv1.Value_Float{Float: 123.4567}},
			},
		}
		pushEnabledFloat36975_34Settings = map[string]*apiv1.SetSetting{
			pushEnable: {
				Value: &apiv1.Value{Value: &apiv1.Value_Float{Float: 36975.34}},
			},
		}

		pushLimitFloatSettingsToNegVal = map[string]*apiv1.SetSetting{
			"push-limit": {
				Value: &apiv1.Value{Value: &apiv1.Value_Float{Float: -7.4567}},
			},
		}

		activeUserSettingToOff = map[string]*apiv1.SetSetting{
			"active-user": {
				Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: false}},
			},
		}

		activeUserSettingToOn = map[string]*apiv1.SetSetting{
			"active-user": {
				Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}},
			},
		}

		mailNameSettings = map[string]*apiv1.SetSetting{
			"mail-name": {
				Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "some.great.mail.for-user-setting@why.are.you.here"}},
				Tags:  []string{"SET-as-USERS", "why-not", "trying-to-change-this-world-again"},
			},
		}
		mailNameSecondSettings = map[string]*apiv1.SetSetting{
			"mail-name": {
				Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "some.great.mail.for-chat-room@why.are.you.here"}},
				Tags:  []string{"SET-as-CHATS", "why-not", "trying-to-change-this-world-again"},
			},
		}
		mailNameThirdSettings = map[string]*apiv1.SetSetting{
			"mail-name": {
				Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "some.great.mail.for-user-chat-room@why.are.you.here"}},
				Tags:  []string{"SET-as-USERS-CHATS", "why-not", "trying-to-change-this-world-again"},
			},
		}
		pushOnForChatsTypesSettings = map[string]*apiv1.SetSetting{
			"push": {
				Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}},
				Tags:  []string{"SET-as-USERS-TYPES-CHATS-TYPES"},
			},
		}
		pushOffForChatsTypesSettings = map[string]*apiv1.SetSetting{
			"push": {
				Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: false}},
				Tags:  []string{"SET-as-USERS-TYPES-CHATS-TYPES", "off"},
			},
		}

		twoSettingsOneAndTwo = map[string]*apiv1.SetSetting{
			"setting-one": {
				Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "setting one value"}},
				Tags:  []string{"setting-one-tag"},
			},
			"setting-two": {
				Value: &apiv1.Value{Value: &apiv1.Value_Float{Float: 8888.389}},
				Tags:  []string{"setting-two-tag"},
			},
		}
	)

	someUserUsersReqs := []*apiv1.SetUsersRequest{{User: someUser, Settings: someSettings}}
	chatsReqs := []*apiv1.SetChatsRequest{{ChatId: someUserChat, Settings: someSettings}}
	chatsTypesReqs := []*apiv1.SetChatsTypesRequest{{
		ChatType: chatapiv2.ChatType_Buyer_Support,
		UserType: chatapiv2.UserType_Customer,
		Settings: someSettings,
	}}
	usersChatsReqs := []*apiv1.SetUsersChatsRequest{
		{
			ChatId:   someUserChat,
			User:     someUser,
			Settings: someSettings,
		},
	}

	users177061And177071And177072And2UsersSetReqs := []*apiv1.SetUsersRequest{
		{User: user177061, Settings: activeUserSettingToOff},
		{User: user177071, Settings: pushEnabledBecomesFloatSettings},
		{User: user177072, Settings: mailFrequencySettingTo10},
		{User: user2, Settings: mailNameSettings},
	}
	user177061UsersSetReqs := []*apiv1.SetUsersRequest{
		{User: user177061, Settings: activeUserSettingToOff},
	}
	user177061UsersSetIntReqs := []*apiv1.SetUsersRequest{
		{User: user177061, Settings: mailFrequencySettingTo10},
	}
	user177071UsersSetReqs := []*apiv1.SetUsersRequest{
		{User: user177071, Settings: pushEnabledBecomesFloatSettings},
	}
	users177071UsersReSetReqs := []*apiv1.SetUsersRequest{
		{User: user177071, Settings: pushEnabledFloat36975_34Settings},
	}

	user177061UserDeleteReqs := []*apiv1.DeleteUserSettingsRequest{
		{User: user177061, SettingNames: []string{"active-user"}},
	}
	user177061NotExistingSettingUserDeleteReqs := []*apiv1.DeleteUserSettingsRequest{
		{User: user177061, SettingNames: []string{"not-existing-setting"}},
	}
	user177061DeleteTwoSettingsReqs := []*apiv1.DeleteUserSettingsRequest{
		{User: user177061, SettingNames: []string{"active-user", "mail-frequency"}},
	}
	user177071UserDeleteReqs := []*apiv1.DeleteUserSettingsRequest{
		{User: user177071, SettingNames: []string{pushEnable}},
	}

	chatSeller2WithCustomer177070SetChatsReqs := []*apiv1.SetChatsRequest{
		{ChatId: chatOfCustomer177070AndSeller2, Settings: mailNameSecondSettings},
	}

	chatToSetTwoSettingsSetChatsReqs := []*apiv1.SetChatsRequest{
		{ChatId: chatToSetTwoSettings, Settings: twoSettingsOneAndTwo},
	}
	chatDelOneOfTwoSettingsDeleteChatsReqs := []*apiv1.DeleteChatSettingsRequest{
		{ChatId: chatToSetTwoSettings, SettingNames: []string{"setting-one"}},
	}

	chatSeller2WithCustomer177070DeleteChatsReqs := []*apiv1.DeleteChatSettingsRequest{
		{ChatId: chatOfCustomer177070AndSeller2, SettingNames: []string{"mail-name"}}, // Settings: mailNameSecondSettings},
	}

	setTwoChatsAtOnceChatsReqs := []*apiv1.SetChatsRequest{
		{ChatId: chatWithSupportOfUserID177071, Settings: activeUserSettingToOff},
		{ChatId: chatWithSupportOfUserID177072, Settings: pushLimitFloatSettingsToNegVal},
	}

	setTwoChatsOfTwoUsers2And177070UsersChatsReqs := []*apiv1.SetUsersChatsRequest{
		{
			ChatId:   chatOfCustomer177070AndSeller2,
			User:     user2,
			Settings: mailNameThirdSettings,
		},
		{
			ChatId:   chatWithSupportOfUserID177071,
			User:     user177071,
			Settings: activeUserSettingToOn,
		},
	}
	users2AndChatFor2And177070SetUsersChatsReqs := []*apiv1.SetUsersChatsRequest{
		{
			ChatId:   chatOfCustomer177070AndSeller2,
			User:     user2,
			Settings: mailNameThirdSettings,
		},
	}

	users2AndChatFor2And177070DeleteUsersChatsReqs := []*apiv1.DeleteUsersChatsSettingsRequest{
		{
			ChatId:       chatOfCustomer177070AndSeller2,
			User:         user2,
			SettingNames: []string{"mail-name"},
		},
	}

	chatTypeBuyerSellerPlusUserTypeSellerChatsTypesReqs := []*apiv1.SetChatsTypesRequest{{
		ChatType: chatapiv2.ChatType_Buyer_Seller,
		UserType: chatapiv2.UserType_Seller,
		Settings: pushOnForChatsTypesSettings,
	}}
	setMultiSettingsTypeBuyerSupportPlusTwoUserTypesChatsTypesReqs := []*apiv1.SetChatsTypesRequest{
		{
			ChatType: chatapiv2.ChatType_Buyer_Support,
			UserType: chatapiv2.UserType_Customer,
			Settings: pushOnForChatsTypesSettings,
		},
		{
			ChatType: chatapiv2.ChatType_Buyer_Support,
			UserType: chatapiv2.UserType_Support,
			Settings: pushOffForChatsTypesSettings,
		},
	}

	chatTypeBuyerSellerPlusUserTypeSellerDeleteChatTypeReqs := []*apiv1.DeleteChatTypeSettingsRequest{{
		ChatType:     chatapiv2.ChatType_Buyer_Seller,
		UserType:     chatapiv2.UserType_Seller,
		SettingNames: []string{"push"},
	}}

	if err := setUsers(ctx, client, someUserUsersReqs); err != nil {
		logger.Fatalf(ctx, "failed SetUsers err=%v", err)
	}

	if err := setChats(ctx, client, chatsReqs); err != nil {
		logger.Fatalf(ctx, "failed setChats err=%v", err)
	}

	if err := setChatsTypes(ctx, client, chatsTypesReqs); err != nil {
		logger.Fatalf(ctx, "failed setChatsTypes err=%v", err)
	}

	if err := setUsersChats(ctx, client, usersChatsReqs); err != nil {
		logger.Fatalf(ctx, "failed setUsersChats err=%v", err)
	}

	if r, err := client.GetUsers(ctx, &apiv1.GetUsersRequest{Users: []*chatapiv2.User{someUser}}); err != nil {
		logger.Fatalf(ctx, "failed GetUsers err=%v", err)
	} else {
		fmt.Printf("GetUsers response = %v\n\n", r)
	}

	if r, err := client.GetChats(ctx, &apiv1.GetChatsRequest{ChatsIds: []string{someUserChat}}); err != nil {
		logger.Fatalf(ctx, "failed GetChats err=%v", err)
	} else {
		fmt.Printf("GetChats response = %v\n\n", r)
	}

	if r, err := client.GetChatsTypes(ctx, &apiv1.GetChatsTypesRequest{
		ChatsTypes: []chatapiv2.ChatType{chatapiv2.ChatType_Buyer_Support},
		UsersTypes: []chatapiv2.UserType{chatapiv2.UserType_Customer},
	}); err != nil {
		logger.Fatalf(ctx, "failed GetChatsTypes err=%v", err)
	} else {
		fmt.Printf("GetChatsTypes response = %v\n\n", r)
	}

	if r, err := client.GetUsersChats(ctx, &apiv1.GetUsersChatsRequest{
		UsersChatsIds: []*apiv1.GetUsersChatsRequest_UserChatsIDs{{User: someUser, ChatsIds: []string{someUserChat}}},
	}); err != nil {
		logger.Fatalf(ctx, "failed GetChatsUsers err=%v", err)
	} else {
		fmt.Printf("GetChatsUsers response = %v\n\n", r)
	}

	if r, err := client.GetAll(ctx, &apiv1.GetAllRequest{
		User:     someUser,
		ChatId:   someUserChat,
		ChatType: chatapiv2.ChatType_Buyer_Support,
	}); err != nil {
		logger.Fatalf(ctx, "failed GetAll err=%v", err)
	} else {
		fmt.Printf("GetAll response = %v\n\n", r)
	}

	fmt.Printf("SET =====================================\n\n")

	// Tag: "setUsers: many users & many settings"
	if err := setUsers(ctx, client, users177061And177071And177072And2UsersSetReqs); err != nil {
		logger.Fatalf(ctx, "failed SetUsers err=%v", err)
	}

	// Tag: "SetChats: set one chat"
	if err := setChats(ctx, client, chatSeller2WithCustomer177070SetChatsReqs); err != nil {
		logger.Fatalf(ctx, "failed setChats err=%v", err)
	}

	// Tag: "SetChats: set one chat two settings"
	if err := setChats(ctx, client, chatToSetTwoSettingsSetChatsReqs); err != nil {
		logger.Fatalf(ctx, "failed setChats err=%v", err)
	}

	// Tag: "SetChats: set two chats"
	if err := setChats(ctx, client, setTwoChatsAtOnceChatsReqs); err != nil {
		logger.Fatalf(ctx, "failed setChats err=%v", err)
	}

	// Tag: "setChatsUsers: two user&room"
	if err := setUsersChats(ctx, client, setTwoChatsOfTwoUsers2And177070UsersChatsReqs); err != nil {
		logger.Fatalf(ctx, "failed setUsersChats err=%v", err)
	}

	// Tag: "setChatsTypes: one user-type one chat-type" - chatType = BuyerSeller & UserType = Seller
	if err := setChatsTypes(ctx, client, chatTypeBuyerSellerPlusUserTypeSellerChatsTypesReqs); err != nil {
		logger.Fatalf(ctx, "failed setChatsTypes err=%v", err)
	}
	// chatType = BuyerSupport & UserType = Customer
	if err := setChatsTypes(ctx, client, setMultiSettingsTypeBuyerSupportPlusTwoUserTypesChatsTypesReqs); err != nil {
		logger.Fatalf(ctx, "failed setChatsTypes err=%v", err)
	}

	fmt.Printf("GETs =====================================\n\n")

	// GET USERS

	expectedUser2 := &chatapiv2.User{Type: chatapiv2.UserType_Seller, Id: userID2}

	// Tag: "GetUsers: return two users"
	if r, err := client.GetUsers(ctx, &apiv1.GetUsersRequest{Users: []*chatapiv2.User{
		user177061,
		user2,
	}}); err != nil {
		logger.Fatalf(ctx, "failed GetUsers err=%v", err)
	} else {
		require.Len(t, r.UsersSettings, 2)
		var u177060ArrInd, u2ArrInd = 0, 1
		if r.UsersSettings[0].User.Id == "2" {
			u2ArrInd = 0
			u177060ArrInd = 1
		}
		// user 177060
		user177060Set := r.UsersSettings[u177060ArrInd]
		require.Equal(t, "177061", user177060Set.User.Id)
		require.Equal(t, chatapiv2.UserType_Customer, user177060Set.User.Type)
		require.Equal(t, 2, len(user177060Set.Settings.Settings), "expect user has 2 setting")
		// Tag: "set-get bool setting"
		require.Equal(t, user177060Set.Settings.Settings["active-user"].Value.GetBool(), false)
		require.ElementsMatch(t, user177060Set.Settings.Settings["active-user"].Tags, []string{})
		// Tag: "default push-enable: true for customer + set another setting"
		require.Equal(t, user177060Set.Settings.Settings[pushEnable].Value.GetBool(), true)
		require.ElementsMatch(t, user177060Set.Settings.Settings[pushEnable].Tags, []string{})

		// user 2
		require.Equal(t, expectedUser2, r.UsersSettings[u2ArrInd].User)
		user2Settings := r.UsersSettings[u2ArrInd].Settings.Settings
		require.Equal(t, 2, len(user2Settings), "expect user has 2 settings")
		// Tag: "set-get string setting"
		require.Equal(t, "some.great.mail.for-user-setting@why.are.you.here", user2Settings["mail-name"].Value.GetString_())
		require.ElementsMatch(t, user2Settings["mail-name"].Tags, []string{"set-as-users", "why-not", "trying-to-change-this-world-again"})
		// Tag: "default push-enable: false for seller + set another setting"
		require.False(t, user2Settings[pushEnable].Value.GetBool())
		require.ElementsMatch(t, user2Settings[pushEnable].Tags, []string{})
		fmt.Printf("GetUsers response = %v\n\n", r)
	}

	// Tag: "GetUsers: return one user"
	if r, err := client.GetUsers(ctx, &apiv1.GetUsersRequest{Users: []*chatapiv2.User{user177071}}); err != nil {
		logger.Fatalf(ctx, "failed GetUsers err=%v", err)
	} else {
		require.Len(t, r.UsersSettings, 1)
		require.Equal(t, "177071", r.UsersSettings[0].User.Id)
		require.Equal(t, chatapiv2.UserType_Customer, r.UsersSettings[0].User.Type)
		require.Len(t, r.UsersSettings[0].Settings.Settings, 1, "expect user has 1 setting")
		// Tag: "set-get float setting"
		// Tag: "set-get def-bool to float setting"
		require.Equal(t, r.UsersSettings[0].Settings.Settings[pushEnable].Value.GetFloat(), float32(123.4567))
		fmt.Printf("GetUsers response = %v\n\n", r)
	}

	if r, err := client.GetUsers(ctx, &apiv1.GetUsersRequest{Users: []*chatapiv2.User{user177072}}); err != nil {
		logger.Fatalf(ctx, "failed GetUsers err=%v", err)
	} else {
		require.Len(t, r.UsersSettings, 1)
		require.Equal(t, "177072", r.UsersSettings[0].User.Id)
		require.Equal(t, chatapiv2.UserType_Customer, r.UsersSettings[0].User.Type)
		require.Len(t, r.UsersSettings[0].Settings.Settings, 2, "expect user has 2 setting")
		// Tag: "set-get int setting"
		require.Equal(t, r.UsersSettings[0].Settings.Settings["mail-frequency"].Value.GetInt(), int64(10))
		fmt.Printf("GetUsers response = %v\n\n", r)
	}

	if r, err := client.GetUsers(ctx, &apiv1.GetUsersRequest{Users: []*chatapiv2.User{
		user20DoNotUseInSet,
	}}); err != nil {
		logger.Fatalf(ctx, "failed GetUsers err=%v", err)
	} else {
		require.Len(t, r.UsersSettings, 1)
		require.Len(t, r.UsersSettings[0].Settings.Settings, 1)
		require.Equal(t, r.UsersSettings[0].User.Id, user20DoNotUseInSet.Id)
		require.Equal(t, r.UsersSettings[0].User.Type, chatapiv2.UserType_Seller)
		// Tag: "default push-enable: true for seller (only this setting)"
		require.Equal(t, r.UsersSettings[0].Settings.Settings[pushEnable].Value.GetBool(), false)
		require.Len(t, r.UsersSettings[0].Settings.Settings[pushEnable].Tags, 0)
		fmt.Printf("GetUsers response = %v\n\n", r)
	}

	if r, err := client.GetUsers(ctx, &apiv1.GetUsersRequest{Users: []*chatapiv2.User{
		user177073DoNotUseInSet,
	}}); err != nil {
		logger.Fatalf(ctx, "failed GetUsers err=%v", err)
	} else {
		require.Len(t, r.UsersSettings, 1)
		require.Len(t, r.UsersSettings[0].Settings.Settings, 1)
		require.Equal(t, r.UsersSettings[0].User.Id, user177073DoNotUseInSet.Id)
		require.Equal(t, r.UsersSettings[0].User.Type, chatapiv2.UserType_Customer)
		// Tag: "default push-enable: true for customer (only this setting)"
		require.Equal(t, r.UsersSettings[0].Settings.Settings[pushEnable].Value.GetBool(), true)
		require.Len(t, r.UsersSettings[0].Settings.Settings[pushEnable].Tags, 0)
		fmt.Printf("GetUsers response = %v\n\n", r)
	}

	// GET CHATS

	// Tag: "GetChats: return two chats"
	if r, err := client.GetChats(ctx, &apiv1.GetChatsRequest{ChatsIds: []string{
		chatWithSupportOfUserID177071,
		chatWithSupportOfUserID177072,
	}}); err != nil {
		logger.Fatalf(ctx, "failed GetChats err=%v", err)
	} else {
		require.Len(t, r.ChatSettingsByChatsIds, 2)

		require.Len(t, r.ChatSettingsByChatsIds[chatWithSupportOfUserID177071].Settings.Settings, 1)
		require.Equal(t, r.ChatSettingsByChatsIds[chatWithSupportOfUserID177071].Settings.Settings["active-user"].Value.GetBool(), false)

		require.Len(t, r.ChatSettingsByChatsIds[chatWithSupportOfUserID177072].Settings.Settings, 1)
		require.Equal(t, r.ChatSettingsByChatsIds[chatWithSupportOfUserID177072].Settings.Settings["push-limit"].Value.GetFloat(), float32(-7.4567))

		fmt.Printf("GetChats response = %v\n\n", r)
	}

	// Tag: "GetChats: return one chat"
	if r, err := client.GetChats(ctx, &apiv1.GetChatsRequest{ChatsIds: []string{chatOfCustomer177070AndSeller2}}); err != nil {
		logger.Fatalf(ctx, "failed GetChats err=%v", err)
	} else {
		require.Len(t, r.ChatSettingsByChatsIds, 1)
		require.Len(t, r.ChatSettingsByChatsIds[chatOfCustomer177070AndSeller2].Settings.Settings, 1)
		require.Equal(t, r.ChatSettingsByChatsIds[chatOfCustomer177070AndSeller2].Settings.Settings["mail-name"].Value.GetString_(),
			"some.great.mail.for-chat-room@why.are.you.here")
		require.ElementsMatch(t, r.ChatSettingsByChatsIds[chatOfCustomer177070AndSeller2].Settings.Settings["mail-name"].Tags,
			[]string{"set-as-chats", "why-not", "trying-to-change-this-world-again"})
		fmt.Printf("GetChats response = %v\n\n", r)
	}

	// Tag: "GetChats: return one chat no settings"
	if r, err := client.GetChats(ctx, &apiv1.GetChatsRequest{ChatsIds: []string{chatDoNotSetAnySettings}}); err != nil {
		logger.Fatalf(ctx, "failed GetChats err=%v", err)
	} else {
		require.Len(t, r.ChatSettingsByChatsIds, 1)
		require.Len(t, r.ChatSettingsByChatsIds[chatDoNotSetAnySettings].Settings.Settings, 0)
		fmt.Printf("GetChats response = %v\n\n", r)
	}

	// GET CHATS USERS

	// Tag: "GetChatsUsers: two user&room"
	if r, err := client.GetUsersChats(ctx, &apiv1.GetUsersChatsRequest{
		UsersChatsIds: []*apiv1.GetUsersChatsRequest_UserChatsIDs{
			{User: user2, ChatsIds: []string{chatOfCustomer177070AndSeller2}},
			{User: user177071, ChatsIds: []string{chatWithSupportOfUserID177071}},
		},
	}); err != nil {
		logger.Fatalf(ctx, "failed GetChatsUsers err=%v", err)
	} else {
		require.Equal(t, len(r.UsersChatsSettings), 2)

		var settingsOf2 = r.UsersChatsSettings[0].Settings.Settings
		var settingsOf177071 = r.UsersChatsSettings[1].Settings.Settings
		if r.UsersChatsSettings[0].User.Id == "177071" {
			settingsOf177071 = r.UsersChatsSettings[0].Settings.Settings
			settingsOf2 = r.UsersChatsSettings[1].Settings.Settings
		}
		// user 2
		require.Equal(t, 1, len(settingsOf2))
		require.Equal(t, "some.great.mail.for-user-chat-room@why.are.you.here", settingsOf2["mail-name"].Value.GetString_())
		require.ElementsMatch(t, []string{"set-as-users-chats", "why-not", "trying-to-change-this-world-again"}, settingsOf2["mail-name"].Tags)
		// user 177071
		require.Equal(t, 1, len(settingsOf177071))
		require.Equal(t, true, settingsOf177071["active-user"].Value.GetBool())

		fmt.Printf("GetChatsUsers response = %v\n\n", r)
	}

	// GET CHATS TYPES = !!!UserType!!! + ChatType

	// Tag: "GetChatsTypes: one user-type one chat-type" - chatType = BuyerSeller & UserType = Seller
	if r, err := client.GetChatsTypes(ctx, &apiv1.GetChatsTypesRequest{
		ChatsTypes: []chatapiv2.ChatType{chatapiv2.ChatType_Buyer_Seller},
		UsersTypes: []chatapiv2.UserType{chatapiv2.UserType_Seller},
	}); err != nil {
		logger.Fatalf(ctx, "failed GetChatsTypes err=%v", err)
	} else {
		require.Len(t, r.ChatTypesSettings, 1)
		settings := r.ChatTypesSettings[0].Settings.Settings
		require.Len(t, settings, 2)
		// new setting
		require.True(t, settings["push"].Value.GetBool())
		require.ElementsMatch(t, settings["push"].Tags, []string{"set-as-users-types-chats-types"})
		// default setting
		require.False(t, settings[pushEnable].Value.GetBool())
		require.Len(t, settings[pushEnable].Tags, 0)
		fmt.Printf("GetChatsTypes response = %v\n\n", r)
	}

	// Tag: "GetChatsTypes: two user-types two chat-types"
	if r, err := client.GetChatsTypes(ctx, &apiv1.GetChatsTypesRequest{
		ChatsTypes: []chatapiv2.ChatType{chatapiv2.ChatType_Buyer_Support, chatapiv2.ChatType_Buyer_Seller},
		UsersTypes: []chatapiv2.UserType{chatapiv2.UserType_Customer, chatapiv2.UserType_Seller},
	}); err != nil {
		logger.Fatalf(ctx, "failed GetChatsTypes err=%v", err)
	} else {
		require.Len(t, r.ChatTypesSettings, 4)
		fmt.Printf("GetChatsTypes response = %v\n\n", r)
	}

	// Tag: "GetChatsTypes: TWO user-types ONE chat-types"
	if r, err := client.GetChatsTypes(ctx, &apiv1.GetChatsTypesRequest{
		ChatsTypes: []chatapiv2.ChatType{chatapiv2.ChatType_Buyer_Support},
		UsersTypes: []chatapiv2.UserType{chatapiv2.UserType_Customer, chatapiv2.UserType_Support},
	}); err != nil {
		logger.Fatalf(ctx, "failed GetChatsTypes err=%v", err)
	} else {
		require.Len(t, r.ChatTypesSettings, 2)
		var settingOfCustomer = r.ChatTypesSettings[0].Settings.Settings
		var settingOfSupport = r.ChatTypesSettings[1].Settings.Settings
		if r.ChatTypesSettings[1].UserType == chatapiv2.UserType_Customer {
			settingOfSupport = r.ChatTypesSettings[0].Settings.Settings
			settingOfCustomer = r.ChatTypesSettings[1].Settings.Settings
		}
		// Customer
		require.Len(t, settingOfCustomer, 2)
		require.True(t, settingOfCustomer["push"].Value.GetBool())
		require.True(t, settingOfCustomer[pushEnable].Value.GetBool())
		// Support
		require.Len(t, settingOfSupport, 2)
		require.False(t, settingOfSupport["push"].Value.GetBool())
		require.True(t, settingOfSupport[pushEnable].Value.GetBool())
		fmt.Printf("GetChatsTypes response = %v\n\n", r)
	}

	// Tag: "default push-enable: true for customer-support-chat (only this setting)"
	if r, err := client.GetChatsTypes(ctx, &apiv1.GetChatsTypesRequest{
		ChatsTypes: []chatapiv2.ChatType{chatapiv2.ChatType_Buyer_Support},
		UsersTypes: []chatapiv2.UserType{chatapiv2.UserType_ChatBot},
	}); err != nil {
		logger.Fatalf(ctx, "failed GetChatsTypes err=%v", err)
	} else {
		require.Len(t, r.ChatTypesSettings, 1)
		setting := r.ChatTypesSettings[0].Settings.Settings
		require.Len(t, setting, 1)
		require.True(t, setting[pushEnable].Value.GetBool())
		require.Len(t, setting[pushEnable].Tags, 0)
		fmt.Printf("GetChatsTypes response = %v\n\n", r)
	}

	// Tag: "default push-enable: false for customer-seller-chat (only this setting)"
	if r, err := client.GetChatsTypes(ctx, &apiv1.GetChatsTypesRequest{
		ChatsTypes: []chatapiv2.ChatType{chatapiv2.ChatType_Buyer_Seller},
		UsersTypes: []chatapiv2.UserType{chatapiv2.UserType_ChatBot},
	}); err != nil {
		logger.Fatalf(ctx, "failed GetChatsTypes err=%v", err)
	} else {
		require.Len(t, r.ChatTypesSettings, 1)
		setting := r.ChatTypesSettings[0].Settings.Settings
		require.Len(t, setting, 1)
		require.False(t, setting[pushEnable].Value.GetBool())
		require.ElementsMatch(t, setting[pushEnable].Tags, []string{})
		fmt.Printf("GetChatsTypes response = %v\n\n", r)
	}

	// GET ALL

	// Tag: "GetAll: with all data set in sets"
	if r, err := client.GetAll(ctx, &apiv1.GetAllRequest{
		User:     user2,
		ChatId:   chatOfCustomer177070AndSeller2,
		ChatType: chatapiv2.ChatType_Buyer_Seller,
	}); err != nil {
		logger.Fatalf(ctx, "failed GetAll err=%v", err)
	} else {
		// user
		require.Equal(t, expectedUser2, r.User.User)
		userSettings := r.User.Settings.Settings
		require.Equal(t, 2, len(userSettings))
		require.Equal(t, "some.great.mail.for-user-setting@why.are.you.here", userSettings["mail-name"].Value.GetString_())
		require.ElementsMatch(t, userSettings["mail-name"].Tags, []string{"set-as-users", "why-not", "trying-to-change-this-world-again"})
		require.False(t, userSettings[pushEnable].Value.GetBool())
		require.ElementsMatch(t, userSettings[pushEnable].Tags, []string{})
		// chat-room
		require.Equal(t, chatOfCustomer177070AndSeller2, r.Chat.ChatId)
		chatSettings := r.Chat.Settings.Settings
		require.Equal(t, 1, len(chatSettings))
		require.Equal(t, "some.great.mail.for-chat-room@why.are.you.here", chatSettings["mail-name"].Value.GetString_())
		require.ElementsMatch(t, []string{"set-as-chats", "why-not", "trying-to-change-this-world-again"}, chatSettings["mail-name"].Tags)
		// Tag: "LastUpdateAt now"
		timeNowUtcRoundToMin := time.Now().Round(time.Minute).UTC().String()
		require.Equal(t, timeNowUtcRoundToMin,
			time.Unix(chatSettings["mail-name"].GetLastUpdateAt().GetSeconds(), 0).Round(time.Minute).UTC().String())
		// user - chat-room
		require.Equal(t, chatOfCustomer177070AndSeller2, r.UserChat.ChatId)
		require.Equal(t, expectedUser2, r.UserChat.User)
		userChatSettings := r.UserChat.Settings.Settings
		require.Equal(t, 1, len(userChatSettings))
		require.Equal(t, "some.great.mail.for-user-chat-room@why.are.you.here", userChatSettings["mail-name"].Value.GetString_())
		require.ElementsMatch(t, []string{"set-as-users-chats", "why-not", "trying-to-change-this-world-again"}, userChatSettings["mail-name"].Tags)
		// chat-type - user-type
		require.Equal(t, chatapiv2.ChatType_Buyer_Seller, r.ChatType.ChatType)
		require.Equal(t, chatapiv2.UserType_Seller, r.ChatType.UserType)
		chatTypeUserTypeSettings := r.ChatType.Settings.Settings
		require.Len(t, chatTypeUserTypeSettings, 2)
		require.True(t, chatTypeUserTypeSettings["push"].Value.GetBool()) // new setting
		require.ElementsMatch(t, chatTypeUserTypeSettings["push"].Tags, []string{"set-as-users-types-chats-types"})
		require.False(t, chatTypeUserTypeSettings[pushEnable].Value.GetBool()) // default setting
		require.Len(t, chatTypeUserTypeSettings[pushEnable].Tags, 0)
		// Tag: "LastUpdateAt now"
		require.Equal(t, timeNowUtcRoundToMin,
			time.Unix(chatTypeUserTypeSettings["push"].LastUpdateAt.GetSeconds(), 0).Round(time.Minute).UTC().String())
		// Tag: "LastUpdateAt for default setting"
		require.Equal(t, "0001-01-01 00:00:00 +0000 UTC", time.Unix(chatTypeUserTypeSettings[pushEnable].LastUpdateAt.GetSeconds(), 0).UTC().String())

		fmt.Printf("GetAll response = %v\n\n", r)
	}

	// Tag: "GetAll: only empty"
	if r, err := client.GetAll(ctx, &apiv1.GetAllRequest{
		User:     userBotDoNotUseInSet,
		ChatId:   chatWithSeller2OfUserID177073DoNotUseInSet,
		ChatType: chatapiv2.ChatType_OProfit,
	}); err != nil {
		logger.Fatalf(ctx, "failed GetAll err=%v", err)
	} else {
		// user
		expectedUserBot := &chatapiv2.User{Type: chatapiv2.UserType_ChatBot, Id: userIDBotDoNotUseInSet}
		require.Equal(t, expectedUserBot, r.User.User)
		require.Equal(t, 0, len(r.User.Settings.Settings))
		// chat-room
		require.Equal(t, chatWithSeller2OfUserID177073DoNotUseInSet, r.Chat.ChatId)
		require.Equal(t, 0, len(r.Chat.Settings.Settings))
		// user - chat-room
		require.Equal(t, chatWithSeller2OfUserID177073DoNotUseInSet, r.UserChat.ChatId)
		require.Equal(t, expectedUserBot, r.UserChat.User)
		require.Equal(t, 0, len(r.UserChat.Settings.Settings))
		// chat-type - user-type
		require.Equal(t, chatapiv2.ChatType_OProfit, r.ChatType.ChatType)
		require.Equal(t, chatapiv2.UserType_ChatBot, r.ChatType.UserType)
		require.Equal(t, 0, len(r.ChatType.Settings.Settings))
		fmt.Printf("GetAll response = %v\n\n", r)
	}

	fmt.Printf("RE-SET =====================================\n\n")
	// Tag: "re-setUsers: one user & one setting"
	if err := setUsers(ctx, client, users177071UsersReSetReqs); err != nil {
		logger.Fatalf(ctx, "failed SetUsers err=%v", err)
	}
	// check re-set
	if r, err := client.GetUsers(ctx, &apiv1.GetUsersRequest{Users: []*chatapiv2.User{user177071}}); err != nil {
		logger.Fatalf(ctx, "failed GetUsers err=%v", err)
	} else {
		require.Len(t, r.UsersSettings, 1)
		require.Equal(t, "177071", r.UsersSettings[0].User.Id)
		require.Equal(t, chatapiv2.UserType_Customer, r.UsersSettings[0].User.Type)
		require.Len(t, r.UsersSettings[0].Settings.Settings, 1, "expect user has 1 setting")
		// Tag: "re-set-get float setting"
		require.Equal(t, r.UsersSettings[0].Settings.Settings[pushEnable].Value.GetFloat(), float32(36975.34))
		fmt.Printf("GetUsers response = %v\n\n", r)
	}

	fmt.Printf("DELETE =====================================\n\n")

	// DELETE USERS SETTINGS
	// Tag: "DeleteUserSettings: one bool setting"
	if err := deleteUserSettings(ctx, client, user177061UserDeleteReqs); err != nil {
		logger.Fatalf(ctx, "failed DeleteUserSettings err=%v", err)
	}

	// check setting is deleted
	if r, err := client.GetUsers(ctx, &apiv1.GetUsersRequest{Users: []*chatapiv2.User{
		user177061,
	}}); err != nil {
		logger.Fatalf(ctx, "failed GetUsers err=%v", err)
	} else {
		fmt.Printf("GetUsers response = %v\n\n", r)

		require.Len(t, r.UsersSettings, 1)
		user177061User := r.UsersSettings[0].User
		user177061Settings := r.UsersSettings[0].Settings.Settings
		require.Equal(t, "177061", user177061User.Id)
		require.Equal(t, chatapiv2.UserType_Customer, user177061User.Type)
		require.Equal(t, 1, len(user177061Settings), "expect user has 1 setting")
		require.Equal(t, user177061Settings[pushEnable].Value.GetBool(), true)
		require.ElementsMatch(t, user177061Settings[pushEnable].Tags, []string{})
	}

	// Tag: "setUsers to restore setting after deleteUsers"
	if err := setUsers(ctx, client, user177061UsersSetReqs); err != nil {
		logger.Fatalf(ctx, "failed SetUsers err=%v", err)
	}
	// check restoring
	if r, err := client.GetUsers(ctx, &apiv1.GetUsersRequest{Users: []*chatapiv2.User{
		user177061,
	}}); err != nil {
		logger.Fatalf(ctx, "failed GetUsers err=%v", err)
	} else {
		fmt.Printf("GetUsers response = %v\n\n", r)

		require.Len(t, r.UsersSettings, 1)
		user177061User := r.UsersSettings[0].User
		user177061Settings := r.UsersSettings[0].Settings.Settings
		require.Equal(t, "177061", user177061User.Id)
		require.Equal(t, chatapiv2.UserType_Customer, user177061User.Type)
		require.Equal(t, 2, len(user177061Settings), "expect user has 1 setting")
		require.Equal(t, user177061Settings["active-user"].Value.GetBool(), false)
		require.Equal(t, user177061Settings[pushEnable].Value.GetBool(), true)
	}

	// DELETE CHATS SETTINGS
	// Tag: "DeleteChatSettings: one string setting"
	if err := deleteChatSettings(ctx, client, chatSeller2WithCustomer177070DeleteChatsReqs); err != nil {
		logger.Fatalf(ctx, "failed DeleteChatSettings err=%v", err)
	}

	if r, err := client.GetChats(ctx, &apiv1.GetChatsRequest{ChatsIds: []string{chatOfCustomer177070AndSeller2}}); err != nil {
		logger.Fatalf(ctx, "failed GetChats err=%v", err)
	} else {
		require.Equal(t, 1, len(r.ChatSettingsByChatsIds))
		require.Equal(t, 0, len(r.ChatSettingsByChatsIds[chatOfCustomer177070AndSeller2].Settings.Settings))
	}

	// Tag: "setChats to restore setting after DeleteChatSettings"
	if err := setChats(ctx, client, chatSeller2WithCustomer177070SetChatsReqs); err != nil {
		logger.Fatalf(ctx, "failed setChats err=%v", err)
	}
	// check restoring
	if r, err := client.GetChats(ctx, &apiv1.GetChatsRequest{ChatsIds: []string{chatOfCustomer177070AndSeller2}}); err != nil {
		logger.Fatalf(ctx, "failed GetChats err=%v", err)
	} else {
		require.Equal(t, 1, len(r.ChatSettingsByChatsIds))
		settings := r.ChatSettingsByChatsIds[chatOfCustomer177070AndSeller2].Settings.Settings
		require.Equal(t, 1, len(settings))
		require.Equal(t, "some.great.mail.for-chat-room@why.are.you.here", settings["mail-name"].Value.GetString_())
	}

	// Tag: "DeleteChatSettings: delete one setting of two"
	if err := deleteChatSettings(ctx, client, chatDelOneOfTwoSettingsDeleteChatsReqs); err != nil {
		logger.Fatalf(ctx, "failed DeleteChatSettings err=%v", err)
	}
	// check one setting left
	if r, err := client.GetChats(ctx, &apiv1.GetChatsRequest{ChatsIds: []string{chatToSetTwoSettings}}); err != nil {
		logger.Fatalf(ctx, "failed GetChats err=%v", err)
	} else {
		require.Equal(t, 1, len(r.ChatSettingsByChatsIds))
		settings := r.ChatSettingsByChatsIds[chatToSetTwoSettings].Settings.Settings
		require.Equal(t, 1, len(settings))
		require.Equal(t, float32(8888.389), settings["setting-two"].Value.GetFloat())
	}

	// DELETE USERS CHATS SETTINGS
	// Tag: "DeleteUserChatSettings: string"
	if err := deleteUserChatSettings(ctx, client, users2AndChatFor2And177070DeleteUsersChatsReqs); err != nil {
		logger.Fatalf(ctx, "failed DeleteUserChatSettings err=%v", err)
	}

	// check setting is deleted
	if r, err := client.GetUsersChats(ctx, &apiv1.GetUsersChatsRequest{
		UsersChatsIds: []*apiv1.GetUsersChatsRequest_UserChatsIDs{
			{User: user2, ChatsIds: []string{chatOfCustomer177070AndSeller2}},
		},
	}); err != nil {
		logger.Fatalf(ctx, "failed GetChatsUsers err=%v", err)
	} else {
		require.Equal(t, 1, len(r.UsersChatsSettings))
		require.Equal(t, 0, len(r.UsersChatsSettings[0].Settings.Settings))
	}

	// Tag: "setUsersChats to restore setting after DeleteUsersChatsSettings"
	if err := setUsersChats(ctx, client, users2AndChatFor2And177070SetUsersChatsReqs); err != nil {
		logger.Fatalf(ctx, "failed setUsersChats err=%v", err)
	}
	if r, err := client.GetUsersChats(ctx, &apiv1.GetUsersChatsRequest{
		UsersChatsIds: []*apiv1.GetUsersChatsRequest_UserChatsIDs{
			{User: user2, ChatsIds: []string{chatOfCustomer177070AndSeller2}},
		},
	}); err != nil {
		logger.Fatalf(ctx, "failed GetChatsUsers err=%v", err)
	} else {
		require.Equal(t, 1, len(r.UsersChatsSettings))
		settingsOf2 := r.UsersChatsSettings[0].Settings.Settings
		require.Equal(t, 1, len(settingsOf2))
		require.Equal(t, "some.great.mail.for-user-chat-room@why.are.you.here", settingsOf2["mail-name"].Value.GetString_())
	}

	// DELETE CHAT-TYPES SETTINGS
	// Tag: "DeleteChatTypeSettings: bool"
	if err := deleteChatTypeSettings(ctx, client, chatTypeBuyerSellerPlusUserTypeSellerDeleteChatTypeReqs); err != nil {
		logger.Fatalf(ctx, "failed DeleteChatTypeSettings err=%v", err)
	}
	// check
	if r, err := client.GetChatsTypes(ctx, &apiv1.GetChatsTypesRequest{
		ChatsTypes: []chatapiv2.ChatType{chatapiv2.ChatType_Buyer_Seller},
		UsersTypes: []chatapiv2.UserType{chatapiv2.UserType_Seller},
	}); err != nil {
		logger.Fatalf(ctx, "failed GetChatsTypes err=%v", err)
	} else {
		require.Len(t, r.ChatTypesSettings, 1)
		settings := r.ChatTypesSettings[0].Settings.Settings
		require.Len(t, settings, 1)
		// default setting
		require.False(t, settings[pushEnable].Value.GetBool())
	}

	// Tag: "setChatsTypes to restore setting after DeleteChatTypeSettings"
	if err := setChatsTypes(ctx, client, chatTypeBuyerSellerPlusUserTypeSellerChatsTypesReqs); err != nil {
		logger.Fatalf(ctx, "failed setChatsTypes err=%v", err)
	}
	// check
	if r, err := client.GetChatsTypes(ctx, &apiv1.GetChatsTypesRequest{
		ChatsTypes: []chatapiv2.ChatType{chatapiv2.ChatType_Buyer_Seller},
		UsersTypes: []chatapiv2.UserType{chatapiv2.UserType_Seller},
	}); err != nil {
		logger.Fatalf(ctx, "failed GetChatsTypes err=%v", err)
	} else {
		require.Len(t, r.ChatTypesSettings, 1)
		settings := r.ChatTypesSettings[0].Settings.Settings
		require.Len(t, settings, 2)
		require.True(t, settings["push"].Value.GetBool())
		// default setting
		require.False(t, settings[pushEnable].Value.GetBool())
	}

	// DELETE CHANGED DEFAULT SETTING
	// Tag: "DeleteUserSettings: one float-bool setting switch to default"
	if err := deleteUserSettings(ctx, client, user177071UserDeleteReqs); err != nil {
		logger.Fatalf(ctx, "failed DeleteUserSettings err=%v", err)
	}
	// check setting is deleted
	if r, err := client.GetUsers(ctx, &apiv1.GetUsersRequest{Users: []*chatapiv2.User{
		user177071,
	}}); err != nil {
		logger.Fatalf(ctx, "failed GetUsers err=%v", err)
	} else {
		require.Len(t, r.UsersSettings, 1)
		user177071User := r.UsersSettings[0].User
		user177071Settings := r.UsersSettings[0].Settings.Settings
		require.Equal(t, userID177071, user177071User.Id)
		require.Equal(t, chatapiv2.UserType_Customer, user177071User.Type)
		require.Equal(t, 1, len(user177071Settings), "expect user has 1 setting")
		require.Equal(t, user177071Settings[pushEnable].Value.GetBool(), true)
		require.ElementsMatch(t, user177071Settings[pushEnable].Tags, []string{})
	}
	// restore
	if err := setUsers(ctx, client, user177071UsersSetReqs); err != nil {
		logger.Fatalf(ctx, "failed SetUsers err=%v", err)
	}
	// check
	if r, err := client.GetUsers(ctx, &apiv1.GetUsersRequest{Users: []*chatapiv2.User{
		user177071,
	}}); err != nil {
		logger.Fatalf(ctx, "failed GetUsers err=%v", err)
	} else {
		require.Len(t, r.UsersSettings, 1)
		user177071User := r.UsersSettings[0].User
		user177071Settings := r.UsersSettings[0].Settings.Settings
		require.Equal(t, userID177071, user177071User.Id)
		require.Equal(t, chatapiv2.UserType_Customer, user177071User.Type)
		require.Equal(t, 1, len(user177071Settings), "expect user has 1 setting")
		require.Equal(t, float32(123.4567), user177071Settings[pushEnable].Value.GetFloat())
		require.ElementsMatch(t, user177071Settings[pushEnable].Tags, []string{})
	}

	// prepare: add 2nd setting
	if err := setUsers(ctx, client, user177061UsersSetIntReqs); err != nil {
		logger.Fatalf(ctx, "failed SetUsers err=%v", err)
	}
	// DELETE NOT-EXISTING SETTING
	// Tag: "DeleteUserSettings: not existing setting"
	if err := deleteUserSettings(ctx, client, user177061NotExistingSettingUserDeleteReqs); err != nil {
		logger.Fatalf(ctx, "failed DeleteUserSettings err=%v", err)
	}
	// check
	if r, err := client.GetUsers(ctx, &apiv1.GetUsersRequest{Users: []*chatapiv2.User{
		user177061,
	}}); err != nil {
		logger.Fatalf(ctx, "failed GetUsers err=%v", err)
	} else {
		require.Len(t, r.UsersSettings, 1)
		user177061User := r.UsersSettings[0].User
		user177061Settings := r.UsersSettings[0].Settings.Settings
		require.Equal(t, userID177061, user177061User.Id)
		require.Equal(t, chatapiv2.UserType_Customer, user177061User.Type)
		require.Equal(t, 3, len(user177061Settings), "expect user has 1 setting")
		require.Equal(t, true, user177061Settings[pushEnable].Value.GetBool())
		require.Equal(t, false, user177061Settings["active-user"].Value.GetBool())
		require.Equal(t, int64(10), user177061Settings["mail-frequency"].Value.GetInt())
	}

	// DELETE TWO SETTINGS
	// Tag: "DeleteUserSettings: delete two settings"
	// Tag: "DeleteUserSettings: int setting"
	if err := deleteUserSettings(ctx, client, user177061DeleteTwoSettingsReqs); err != nil {
		logger.Fatalf(ctx, "failed DeleteUserSettings err=%v", err)
	}
	// check
	if r, err := client.GetUsers(ctx, &apiv1.GetUsersRequest{Users: []*chatapiv2.User{
		user177061,
	}}); err != nil {
		logger.Fatalf(ctx, "failed GetUsers err=%v", err)
	} else {
		require.Len(t, r.UsersSettings, 1)
		user177061User := r.UsersSettings[0].User
		user177061Settings := r.UsersSettings[0].Settings.Settings
		require.Equal(t, userID177061, user177061User.Id)
		require.Equal(t, chatapiv2.UserType_Customer, user177061User.Type)
		require.Equal(t, 1, len(user177061Settings), "expect user has 1 setting")
		require.Equal(t, true, user177061Settings[pushEnable].Value.GetBool())
	}
}

func setUsers(ctx context.Context, client apiv1.SettingsV1Client, requests []*apiv1.SetUsersRequest) error {
	for _, r := range requests {
		if _, err := client.SetUsers(ctx, r); err != nil {
			return err
		}
	}
	return nil
}

func setChats(ctx context.Context, client apiv1.SettingsV1Client, requests []*apiv1.SetChatsRequest) error {
	for _, r := range requests {
		if _, err := client.SetChats(ctx, r); err != nil {
			return err
		}
	}
	return nil
}

func setChatsTypes(ctx context.Context, client apiv1.SettingsV1Client, requests []*apiv1.SetChatsTypesRequest) error {
	for _, r := range requests {
		if _, err := client.SetChatsTypes(ctx, r); err != nil {
			return err
		}
	}
	return nil
}

func setUsersChats(ctx context.Context, client apiv1.SettingsV1Client, requests []*apiv1.SetUsersChatsRequest) error {
	for _, r := range requests {
		if _, err := client.SetUsersChats(ctx, r); err != nil {
			return err
		}
	}
	return nil
}

func deleteUserSettings(ctx context.Context, client apiv1.SettingsV1Client, requests []*apiv1.DeleteUserSettingsRequest) error {
	for _, r := range requests {
		if _, err := client.DeleteUserSettings(ctx, r); err != nil {
			return err
		}
	}
	return nil
}

func deleteChatSettings(ctx context.Context, client apiv1.SettingsV1Client, requests []*apiv1.DeleteChatSettingsRequest) error {
	for _, r := range requests {
		if _, err := client.DeleteChatSettings(ctx, r); err != nil {
			return err
		}
	}
	return nil
}

func deleteUserChatSettings(ctx context.Context, client apiv1.SettingsV1Client, requests []*apiv1.DeleteUsersChatsSettingsRequest) error {
	for _, r := range requests {
		if _, err := client.DeleteUsersChatsSettings(ctx, r); err != nil {
			return err
		}
	}
	return nil
}

func deleteChatTypeSettings(ctx context.Context, client apiv1.SettingsV1Client, requests []*apiv1.DeleteChatTypeSettingsRequest) error {
	for _, r := range requests {
		if _, err := client.DeleteChatTypeSettings(ctx, r); err != nil {
			return err
		}
	}
	return nil
}
