// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.27.1
// 	protoc        v3.17.1
// source: gitlab.ozon.ru/messenger/messenger-settings/api/events_v1/events.proto

package events_v1

import (
	v1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	_ "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type Event_ChangeType int32

const (
	Event_Update Event_ChangeType = 0
	Event_Delete Event_ChangeType = 1
)

// Enum value maps for Event_ChangeType.
var (
	Event_ChangeType_name = map[int32]string{
		0: "Update",
		1: "Delete",
	}
	Event_ChangeType_value = map[string]int32{
		"Update": 0,
		"Delete": 1,
	}
)

func (x Event_ChangeType) Enum() *Event_ChangeType {
	p := new(Event_ChangeType)
	*p = x
	return p
}

func (x Event_ChangeType) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (Event_ChangeType) Descriptor() protoreflect.EnumDescriptor {
	return file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_enumTypes[0].Descriptor()
}

func (Event_ChangeType) Type() protoreflect.EnumType {
	return &file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_enumTypes[0]
}

func (x Event_ChangeType) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use Event_ChangeType.Descriptor instead.
func (Event_ChangeType) EnumDescriptor() ([]byte, []int) {
	return file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_rawDescGZIP(), []int{0, 0}
}

type Event struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Changes []*Event_Change `protobuf:"bytes,1,rep,name=changes,proto3" json:"changes,omitempty"`
}

func (x *Event) Reset() {
	*x = Event{}
	if protoimpl.UnsafeEnabled {
		mi := &file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Event) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Event) ProtoMessage() {}

func (x *Event) ProtoReflect() protoreflect.Message {
	mi := &file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Event.ProtoReflect.Descriptor instead.
func (*Event) Descriptor() ([]byte, []int) {
	return file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_rawDescGZIP(), []int{0}
}

func (x *Event) GetChanges() []*Event_Change {
	if x != nil {
		return x.Changes
	}
	return nil
}

type Event_Change struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Type Event_ChangeType `protobuf:"varint,1,opt,name=type,proto3,enum=messenger_settings.events_v1.Event_ChangeType" json:"type,omitempty"`
	// Types that are assignable to Setting:
	//	*Event_Change_ChatType
	//	*Event_Change_Chat
	//	*Event_Change_User
	//	*Event_Change_UserChat
	Setting isEvent_Change_Setting `protobuf_oneof:"Setting"`
}

func (x *Event_Change) Reset() {
	*x = Event_Change{}
	if protoimpl.UnsafeEnabled {
		mi := &file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Event_Change) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Event_Change) ProtoMessage() {}

func (x *Event_Change) ProtoReflect() protoreflect.Message {
	mi := &file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Event_Change.ProtoReflect.Descriptor instead.
func (*Event_Change) Descriptor() ([]byte, []int) {
	return file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_rawDescGZIP(), []int{0, 0}
}

func (x *Event_Change) GetType() Event_ChangeType {
	if x != nil {
		return x.Type
	}
	return Event_Update
}

func (m *Event_Change) GetSetting() isEvent_Change_Setting {
	if m != nil {
		return m.Setting
	}
	return nil
}

func (x *Event_Change) GetChatType() *v1.ChatTypeSetting {
	if x, ok := x.GetSetting().(*Event_Change_ChatType); ok {
		return x.ChatType
	}
	return nil
}

func (x *Event_Change) GetChat() *v1.ChatSetting {
	if x, ok := x.GetSetting().(*Event_Change_Chat); ok {
		return x.Chat
	}
	return nil
}

func (x *Event_Change) GetUser() *v1.UserSetting {
	if x, ok := x.GetSetting().(*Event_Change_User); ok {
		return x.User
	}
	return nil
}

func (x *Event_Change) GetUserChat() *v1.UserChatSetting {
	if x, ok := x.GetSetting().(*Event_Change_UserChat); ok {
		return x.UserChat
	}
	return nil
}

type isEvent_Change_Setting interface {
	isEvent_Change_Setting()
}

type Event_Change_ChatType struct {
	ChatType *v1.ChatTypeSetting `protobuf:"bytes,2,opt,name=chat_type,json=chatType,proto3,oneof"`
}

type Event_Change_Chat struct {
	Chat *v1.ChatSetting `protobuf:"bytes,3,opt,name=chat,proto3,oneof"`
}

type Event_Change_User struct {
	User *v1.UserSetting `protobuf:"bytes,4,opt,name=user,proto3,oneof"`
}

type Event_Change_UserChat struct {
	UserChat *v1.UserChatSetting `protobuf:"bytes,5,opt,name=user_chat,json=userChat,proto3,oneof"`
}

func (*Event_Change_ChatType) isEvent_Change_Setting() {}

func (*Event_Change_Chat) isEvent_Change_Setting() {}

func (*Event_Change_User) isEvent_Change_Setting() {}

func (*Event_Change_UserChat) isEvent_Change_Setting() {}

var File_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto protoreflect.FileDescriptor

var file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_rawDesc = []byte{
	0x0a, 0x46, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x6f, 0x7a, 0x6f, 0x6e, 0x2e, 0x72, 0x75,
	0x2f, 0x6d, 0x65, 0x73, 0x73, 0x65, 0x6e, 0x67, 0x65, 0x72, 0x2f, 0x6d, 0x65, 0x73, 0x73, 0x65,
	0x6e, 0x67, 0x65, 0x72, 0x2d, 0x73, 0x65, 0x74, 0x74, 0x69, 0x6e, 0x67, 0x73, 0x2f, 0x61, 0x70,
	0x69, 0x2f, 0x65, 0x76, 0x65, 0x6e, 0x74, 0x73, 0x5f, 0x76, 0x31, 0x2f, 0x65, 0x76, 0x65, 0x6e,
	0x74, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x1c, 0x6d, 0x65, 0x73, 0x73, 0x65, 0x6e,
	0x67, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x74, 0x74, 0x69, 0x6e, 0x67, 0x73, 0x2e, 0x65, 0x76, 0x65,
	0x6e, 0x74, 0x73, 0x5f, 0x76, 0x31, 0x1a, 0x3c, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x6f,
	0x7a, 0x6f, 0x6e, 0x2e, 0x72, 0x75, 0x2f, 0x6d, 0x65, 0x73, 0x73, 0x65, 0x6e, 0x67, 0x65, 0x72,
	0x2f, 0x6d, 0x65, 0x73, 0x73, 0x65, 0x6e, 0x67, 0x65, 0x72, 0x2d, 0x73, 0x65, 0x74, 0x74, 0x69,
	0x6e, 0x67, 0x73, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x76, 0x31, 0x2f, 0x61, 0x70, 0x69, 0x2e, 0x70,
	0x72, 0x6f, 0x74, 0x6f, 0x1a, 0x36, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x6f, 0x7a, 0x6f,
	0x6e, 0x2e, 0x72, 0x75, 0x2f, 0x70, 0x6c, 0x61, 0x74, 0x66, 0x6f, 0x72, 0x6d, 0x2f, 0x63, 0x68,
	0x61, 0x74, 0x2d, 0x61, 0x70, 0x69, 0x2f, 0x61, 0x70, 0x69, 0x2f, 0x76, 0x32, 0x2f, 0x63, 0x68,
	0x61, 0x74, 0x2d, 0x61, 0x70, 0x69, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x22, 0xcf, 0x03, 0x0a,
	0x05, 0x45, 0x76, 0x65, 0x6e, 0x74, 0x12, 0x44, 0x0a, 0x07, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65,
	0x73, 0x18, 0x01, 0x20, 0x03, 0x28, 0x0b, 0x32, 0x2a, 0x2e, 0x6d, 0x65, 0x73, 0x73, 0x65, 0x6e,
	0x67, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x74, 0x74, 0x69, 0x6e, 0x67, 0x73, 0x2e, 0x65, 0x76, 0x65,
	0x6e, 0x74, 0x73, 0x5f, 0x76, 0x31, 0x2e, 0x45, 0x76, 0x65, 0x6e, 0x74, 0x2e, 0x43, 0x68, 0x61,
	0x6e, 0x67, 0x65, 0x52, 0x07, 0x63, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x73, 0x1a, 0xd9, 0x02, 0x0a,
	0x06, 0x43, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x12, 0x42, 0x0a, 0x04, 0x74, 0x79, 0x70, 0x65, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x2e, 0x2e, 0x6d, 0x65, 0x73, 0x73, 0x65, 0x6e, 0x67, 0x65,
	0x72, 0x5f, 0x73, 0x65, 0x74, 0x74, 0x69, 0x6e, 0x67, 0x73, 0x2e, 0x65, 0x76, 0x65, 0x6e, 0x74,
	0x73, 0x5f, 0x76, 0x31, 0x2e, 0x45, 0x76, 0x65, 0x6e, 0x74, 0x2e, 0x43, 0x68, 0x61, 0x6e, 0x67,
	0x65, 0x54, 0x79, 0x70, 0x65, 0x52, 0x04, 0x74, 0x79, 0x70, 0x65, 0x12, 0x45, 0x0a, 0x09, 0x63,
	0x68, 0x61, 0x74, 0x5f, 0x74, 0x79, 0x70, 0x65, 0x18, 0x02, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x26,
	0x2e, 0x6d, 0x65, 0x73, 0x73, 0x65, 0x6e, 0x67, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x74, 0x74, 0x69,
	0x6e, 0x67, 0x73, 0x2e, 0x76, 0x31, 0x2e, 0x43, 0x68, 0x61, 0x74, 0x54, 0x79, 0x70, 0x65, 0x53,
	0x65, 0x74, 0x74, 0x69, 0x6e, 0x67, 0x48, 0x00, 0x52, 0x08, 0x63, 0x68, 0x61, 0x74, 0x54, 0x79,
	0x70, 0x65, 0x12, 0x38, 0x0a, 0x04, 0x63, 0x68, 0x61, 0x74, 0x18, 0x03, 0x20, 0x01, 0x28, 0x0b,
	0x32, 0x22, 0x2e, 0x6d, 0x65, 0x73, 0x73, 0x65, 0x6e, 0x67, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x74,
	0x74, 0x69, 0x6e, 0x67, 0x73, 0x2e, 0x76, 0x31, 0x2e, 0x43, 0x68, 0x61, 0x74, 0x53, 0x65, 0x74,
	0x74, 0x69, 0x6e, 0x67, 0x48, 0x00, 0x52, 0x04, 0x63, 0x68, 0x61, 0x74, 0x12, 0x38, 0x0a, 0x04,
	0x75, 0x73, 0x65, 0x72, 0x18, 0x04, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x22, 0x2e, 0x6d, 0x65, 0x73,
	0x73, 0x65, 0x6e, 0x67, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x74, 0x74, 0x69, 0x6e, 0x67, 0x73, 0x2e,
	0x76, 0x31, 0x2e, 0x55, 0x73, 0x65, 0x72, 0x53, 0x65, 0x74, 0x74, 0x69, 0x6e, 0x67, 0x48, 0x00,
	0x52, 0x04, 0x75, 0x73, 0x65, 0x72, 0x12, 0x45, 0x0a, 0x09, 0x75, 0x73, 0x65, 0x72, 0x5f, 0x63,
	0x68, 0x61, 0x74, 0x18, 0x05, 0x20, 0x01, 0x28, 0x0b, 0x32, 0x26, 0x2e, 0x6d, 0x65, 0x73, 0x73,
	0x65, 0x6e, 0x67, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x74, 0x74, 0x69, 0x6e, 0x67, 0x73, 0x2e, 0x76,
	0x31, 0x2e, 0x55, 0x73, 0x65, 0x72, 0x43, 0x68, 0x61, 0x74, 0x53, 0x65, 0x74, 0x74, 0x69, 0x6e,
	0x67, 0x48, 0x00, 0x52, 0x08, 0x75, 0x73, 0x65, 0x72, 0x43, 0x68, 0x61, 0x74, 0x42, 0x09, 0x0a,
	0x07, 0x53, 0x65, 0x74, 0x74, 0x69, 0x6e, 0x67, 0x22, 0x24, 0x0a, 0x0a, 0x43, 0x68, 0x61, 0x6e,
	0x67, 0x65, 0x54, 0x79, 0x70, 0x65, 0x12, 0x0a, 0x0a, 0x06, 0x55, 0x70, 0x64, 0x61, 0x74, 0x65,
	0x10, 0x00, 0x12, 0x0a, 0x0a, 0x06, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65, 0x10, 0x01, 0x42, 0x41,
	0x5a, 0x3f, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62, 0x2e, 0x6f, 0x7a, 0x6f, 0x6e, 0x2e, 0x72, 0x75,
	0x2f, 0x6d, 0x65, 0x73, 0x73, 0x65, 0x6e, 0x67, 0x65, 0x72, 0x2f, 0x6d, 0x65, 0x73, 0x73, 0x65,
	0x6e, 0x67, 0x65, 0x72, 0x5f, 0x73, 0x65, 0x74, 0x74, 0x69, 0x6e, 0x67, 0x73, 0x2f, 0x65, 0x76,
	0x65, 0x6e, 0x74, 0x73, 0x5f, 0x76, 0x31, 0x3b, 0x65, 0x76, 0x65, 0x6e, 0x74, 0x73, 0x5f, 0x76,
	0x31, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_rawDescOnce sync.Once
	file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_rawDescData = file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_rawDesc
)

func file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_rawDescGZIP() []byte {
	file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_rawDescOnce.Do(func() {
		file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_rawDescData = protoimpl.X.CompressGZIP(file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_rawDescData)
	})
	return file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_rawDescData
}

var file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_enumTypes = make([]protoimpl.EnumInfo, 1)
var file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_goTypes = []interface{}{
	(Event_ChangeType)(0),      // 0: messenger_settings.events_v1.Event.ChangeType
	(*Event)(nil),              // 1: messenger_settings.events_v1.Event
	(*Event_Change)(nil),       // 2: messenger_settings.events_v1.Event.Change
	(*v1.ChatTypeSetting)(nil), // 3: messenger_settings.v1.ChatTypeSetting
	(*v1.ChatSetting)(nil),     // 4: messenger_settings.v1.ChatSetting
	(*v1.UserSetting)(nil),     // 5: messenger_settings.v1.UserSetting
	(*v1.UserChatSetting)(nil), // 6: messenger_settings.v1.UserChatSetting
}
var file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_depIdxs = []int32{
	2, // 0: messenger_settings.events_v1.Event.changes:type_name -> messenger_settings.events_v1.Event.Change
	0, // 1: messenger_settings.events_v1.Event.Change.type:type_name -> messenger_settings.events_v1.Event.ChangeType
	3, // 2: messenger_settings.events_v1.Event.Change.chat_type:type_name -> messenger_settings.v1.ChatTypeSetting
	4, // 3: messenger_settings.events_v1.Event.Change.chat:type_name -> messenger_settings.v1.ChatSetting
	5, // 4: messenger_settings.events_v1.Event.Change.user:type_name -> messenger_settings.v1.UserSetting
	6, // 5: messenger_settings.events_v1.Event.Change.user_chat:type_name -> messenger_settings.v1.UserChatSetting
	6, // [6:6] is the sub-list for method output_type
	6, // [6:6] is the sub-list for method input_type
	6, // [6:6] is the sub-list for extension type_name
	6, // [6:6] is the sub-list for extension extendee
	0, // [0:6] is the sub-list for field type_name
}

func init() { file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_init() }
func file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_init() {
	if File_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Event); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Event_Change); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_msgTypes[1].OneofWrappers = []interface{}{
		(*Event_Change_ChatType)(nil),
		(*Event_Change_Chat)(nil),
		(*Event_Change_User)(nil),
		(*Event_Change_UserChat)(nil),
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_rawDesc,
			NumEnums:      1,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_goTypes,
		DependencyIndexes: file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_depIdxs,
		EnumInfos:         file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_enumTypes,
		MessageInfos:      file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_msgTypes,
	}.Build()
	File_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto = out.File
	file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_rawDesc = nil
	file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_goTypes = nil
	file_gitlab_ozon_ru_messenger_messenger_settings_api_events_v1_events_proto_depIdxs = nil
}
