module gitlab.ozon.ru/messenger/messenger-settings/pkg/api

go 1.16

require (
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.6.0
	gitlab.ozon.ru/platform/chat-api/pkg/api v1.1.10
	google.golang.org/genproto v0.0.0-20210921142501-181ce0d877f6
	google.golang.org/grpc v1.41.0
	google.golang.org/protobuf v1.27.1
)
