package settings

import (
	context "context"

	"github.com/opentracing/opentracing-go"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/settings"
	eventsv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/events_v1"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	chat_apiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func (s *Implementation) SetChatsTypes(ctx context.Context, req *apiv1.SetChatsTypesRequest) (*apiv1.SetChatsTypesResponse, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "Implementation.SetChatsTypes")
	defer span.Finish()

	params, err := setChatsTypesParams(req)
	if err != nil {
		return nil, err
	}

	ts, err := s.storage.SetChatsTypes(ctx, params.Key, params.Settings)
	if err != nil {
		logger.Errorf(ctx, "failed storage.SetChatsTypes err=%s", err)
		return nil, errInternalError
	}

	tsPB := timestamppb.New(ts)

	notification, err := chatTypeChangedEvent(tsPB, params)
	if err != nil {
		logger.Errorf(ctx, "failed chatTypeChangedEvent err=%v", err)
		return nil, errInternalError
	}

	s.eventProducer.SendEvent(ctx, notification)

	return &apiv1.SetChatsTypesResponse{
		Timestamp: tsPB,
	}, nil
}

type setChatsTypesParamsType struct {
	Key      settings.ChatsTypesKey
	Settings settings.Settings
}

func setChatsTypesParams(req *apiv1.SetChatsTypesRequest) (setChatsTypesParamsType, error) {
	if req.ChatType == chat_apiv2.ChatType_InvalidChat {
		return setChatsTypesParamsType{}, errChatTypeIsInvalid
	}

	if req.UserType == chat_apiv2.UserType_InvalidUser {
		return setChatsTypesParamsType{}, errUserTypeIsInvalid
	}

	ss, err := newSettings(req.Settings)
	if err != nil {
		return setChatsTypesParamsType{}, errSettingsNamesIsEmpty
	}

	return setChatsTypesParamsType{
		Key:      settings.ChatsTypesKey{ChatType: req.ChatType, UserType: req.UserType},
		Settings: ss,
	}, nil

}

func chatTypeChangedEvent(ts *timestamppb.Timestamp, params setChatsTypesParamsType) (*eventsv1.Event, error) {
	ss, err := params.Settings.ToProtoV1()
	if err != nil {
		return nil, err
	}

	ev := &eventsv1.Event_Change{
		Type: eventsv1.Event_Update,
		Setting: &eventsv1.Event_Change_ChatType{
			ChatType: &apiv1.ChatTypeSetting{
				ChatType: params.Key.ChatType,
				UserType: params.Key.UserType,
				Settings: &apiv1.Settings{Timestamp: ts, Settings: ss},
			},
		},
	}

	return &eventsv1.Event{
		Changes: []*eventsv1.Event_Change{ev},
	}, nil
}
