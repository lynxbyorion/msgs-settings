package settings

import (
	context "context"

	"github.com/opentracing/opentracing-go"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/settings"
	eventsv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/events_v1"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func (s *Implementation) SetUsers(ctx context.Context, req *apiv1.SetUsersRequest) (*apiv1.SetUsersResponse, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "Implementation.SetUsers")
	defer span.Finish()

	user, setSettings, err := setUsersParams(req)
	if err != nil {
		return nil, err
	}

	ts, err := s.storage.SetUsers(ctx, user, setSettings)
	if err != nil {
		logger.Errorf(ctx, "failed storage.SetUsers err=%s", err)
		return nil, errInternalError
	}

	tsPB := timestamppb.New(ts)

	notification, err := usersChangedEvent(tsPB, setSettings, user)
	if err != nil {
		logger.Errorf(ctx, "failed usersChangedEvent err=%v", err)
		return nil, errInternalError
	}

	s.eventProducer.SendEvent(ctx, notification)

	return &apiv1.SetUsersResponse{
		Timestamp: tsPB,
	}, nil
}

func setUsersParams(req *apiv1.SetUsersRequest) (settings.UserKey, settings.Settings, error) {
	u, err := newUserKey(req.User)
	if err != nil {
		return settings.UserKey{}, nil, status.Error(codes.InvalidArgument, err.Error())
	}

	ss, err := newSettings(req.Settings)
	if err != nil {
		return settings.UserKey{}, nil, err
	}

	return u, ss, nil
}

func usersChangedEvent(ts *timestamppb.Timestamp, setSettings settings.Settings, user settings.UserKey) (*eventsv1.Event, error) {
	ss, err := setSettings.ToProtoV1()
	if err != nil {
		return nil, err
	}

	ev := &eventsv1.Event_Change{
		Type: eventsv1.Event_Update,
		Setting: &eventsv1.Event_Change_User{
			User: &apiv1.UserSetting{
				User: user.ToPB(),
				Settings: &apiv1.Settings{
					Timestamp: ts,
					Settings:  ss,
				},
			},
		},
	}

	return &eventsv1.Event{
		Changes: []*eventsv1.Event_Change{ev},
	}, nil
}
