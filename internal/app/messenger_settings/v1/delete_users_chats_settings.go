package settings

import (
	"context"

	"github.com/opentracing/opentracing-go"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/settings"
	eventsv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/events_v1"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func (s *Implementation) DeleteUsersChatsSettings(ctx context.Context, req *apiv1.DeleteUsersChatsSettingsRequest) (*apiv1.DeleteUsersChatsSettingsResponse, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "Implementation.DeleteUsersChatsSettings")
	defer span.Finish()

	params, err := deleteChatByUserSettingsParams(req)
	if err != nil {
		return nil, err
	}

	res, err := s.storage.DeleteUserChatSetting(ctx, params.key, params.settingsNames)
	if err != nil {
		if err == settings.NotFoundInStorage {
			return nil, status.Errorf(codes.NotFound, "settings by userID = %s not found", params.key)
		}
		logger.Error(ctx, "failed storage.DeleteUserChatSetting err=%s", err)
		return nil, errInternalError
	}

	tsPB := timestamppb.New(res.Timestmap)

	s.eventProducer.SendEvent(ctx, deleteUsersChatsSettingsEvent(tsPB, params.key, res.Names))

	return &apiv1.DeleteUsersChatsSettingsResponse{
		Timestamp: tsPB,
		Deleted:   res.Names,
	}, nil
}

type deleteUsersChatsSettingsParams struct {
	key           settings.UserChatID
	settingsNames []string
}

func deleteChatByUserSettingsParams(req *apiv1.DeleteUsersChatsSettingsRequest) (*deleteUsersChatsSettingsParams, error) {
	if len(req.SettingNames) == 0 {
		return nil, errSettingsNamesIsEmpty
	}

	uc, err := settings.NewUserChatID(req.User, req.ChatId)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &deleteUsersChatsSettingsParams{
		key:           *uc,
		settingsNames: req.SettingNames,
	}, nil
}

func deleteUsersChatsSettingsEvent(ts *timestamppb.Timestamp, key settings.UserChatID, settingsNames []string) *eventsv1.Event {
	ev := &eventsv1.Event_Change{
		Type: eventsv1.Event_Delete,
		Setting: &eventsv1.Event_Change_UserChat{
			UserChat: &apiv1.UserChatSetting{
				User:   key.User.ToPB(),
				ChatId: key.ChatID,
				Settings: &apiv1.Settings{
					Timestamp: ts,
					Settings:  settingsOnlyKeys(settingsNames),
				},
			},
		},
	}

	return &eventsv1.Event{
		Changes: []*eventsv1.Event_Change{ev},
	}
}
