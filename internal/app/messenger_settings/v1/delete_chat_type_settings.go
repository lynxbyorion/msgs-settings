package settings

import (
	"context"

	"github.com/opentracing/opentracing-go"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/settings"
	eventsv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/events_v1"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func (s *Implementation) DeleteChatTypeSettings(ctx context.Context, req *apiv1.DeleteChatTypeSettingsRequest) (*apiv1.DeleteChatTypeSettingsResponse, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "Implementation.DeleteChatTypeSettings")
	defer span.Finish()

	params, err := deleteChatTypeSettingsParams(req)
	if err != nil {
		return nil, err
	}

	res, err := s.storage.DeleteChatTypeSettings(ctx, params.Keys, params.SettingsNames)
	if err != nil {
		if err == settings.NotFoundInStorage {
			return nil, status.Errorf(codes.NotFound, "sttings by key = %s not found", params.Keys)
		}
		logger.Error(ctx, "failed storage.DeleteChatTypeSettings err=%s", err)
		return nil, errInternalError
	}

	tsPB := timestamppb.New(res.Timestmap)

	s.eventProducer.SendEvent(ctx, deleteChatTypeSettingsEvent(tsPB, params.Keys, res.Names))

	return &apiv1.DeleteChatTypeSettingsResponse{
		Timestamp: tsPB,
		Deleted:   res.Names,
	}, nil
}

type deleteChatTypeSettingsParamsType struct {
	Keys          settings.ChatsTypesKey
	SettingsNames []string
}

func deleteChatTypeSettingsParams(req *apiv1.DeleteChatTypeSettingsRequest) (*deleteChatTypeSettingsParamsType, error) {
	if len(req.SettingNames) == 0 {
		return nil, errSettingsNamesIsEmpty
	}

	return &deleteChatTypeSettingsParamsType{
		Keys:          settings.ChatsTypesKey{ChatType: req.ChatType, UserType: req.UserType},
		SettingsNames: req.SettingNames,
	}, nil
}

func deleteChatTypeSettingsEvent(ts *timestamppb.Timestamp, key settings.ChatsTypesKey, names []string) *eventsv1.Event {
	ev := &eventsv1.Event_Change{
		Type: eventsv1.Event_Delete,
		Setting: &eventsv1.Event_Change_ChatType{
			ChatType: &apiv1.ChatTypeSetting{
				ChatType: key.ChatType,
				UserType: key.UserType,
				Settings: &apiv1.Settings{
					Timestamp: ts,
					Settings:  settingsOnlyKeys(names),
				},
			},
		},
	}

	return &eventsv1.Event{
		Changes: []*eventsv1.Event_Change{ev},
	}
}
