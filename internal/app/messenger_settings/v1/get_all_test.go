package settings

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/settings"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	chat_apiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
)

func TestImplementation_GetAll(t *testing.T) {
	t.Run("if not found return only default settings", func(t *testing.T) {
		s := Implementation{storage: &mockStorage{
			all: func(ctx context.Context, user settings.UserKey, chatID string, chatTypeKey chat_apiv2.ChatType) (*settings.All, error) {
				return &settings.All{}, nil
			},
		}}

		_, err := s.GetAll(context.Background(), &apiv1.GetAllRequest{
			User:     someUser,
			ChatId:   someChatID,
			ChatType: someChatType,
		})

		require.NoError(t, err)
	})

	t.Run("partly not found", func(t *testing.T) {
		s := Implementation{storage: &mockStorage{
			all: func(ctx context.Context, user settings.UserKey, chatID string, chatTypeKey chat_apiv2.ChatType) (*settings.All, error) {
				return &settings.All{
					Chat: &settings.Chat{
						ID:           1,
						ChatID:       someChatID,
						LastUpdateAt: somePgTime,
						Settings:     someSettings,
					},
					ChatType: nil,
					User: &settings.User{
						ID:           1,
						UserID:       someUserID,
						UserType:     someUserType,
						LastUpdateAt: somePgTime,
						Settings:     someSettings,
					},
					UserChat: nil,
				}, nil
			},
		}}

		resp, err := s.GetAll(context.Background(), &apiv1.GetAllRequest{
			User:     someUser,
			ChatId:   someChatID,
			ChatType: someChatType,
		})

		require.NoError(t, err)
		require.NotNil(t, resp)
		assert.NotNil(t, resp.Chat)
		assert.NotNil(t, resp.User)
		assert.NotNil(t, resp.ChatType)
		assert.NotNil(t, resp.UserChat)

	})
}
