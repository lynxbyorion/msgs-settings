package settings

import (
	context "context"

	"github.com/opentracing/opentracing-go"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/settings"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *Implementation) GetChatsTypes(ctx context.Context, req *apiv1.GetChatsTypesRequest) (*apiv1.GetChatsTypesResponse, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "Implementation.GetChatsTypes")
	defer span.Finish()

	params, err := getChatsTypesParams(req)
	if err != nil {
		return nil, err
	}

	chatTypes, err := s.storage.ChatsTypesByTypesAndUsersTypes(ctx, params.ChatsTypes, params.UsersTypes)
	if err != nil {
		logger.Errorf(ctx, "failed storage.ChatsTypesByTypesAndUsersTypes err=%s", err)
		return nil, errInternalError
	}

	chatTypes.AddEmptyChatTypes(params)
	chatTypes.AddDefault()

	chatTypesProto, err := chatTypes.ToProtoV1()
	if err != nil {
		return nil, err
	}

	return &apiv1.GetChatsTypesResponse{ChatTypesSettings: chatTypesProto}, nil
}

func getChatsTypesParams(req *apiv1.GetChatsTypesRequest) (*settings.ChatsTypesKeys, error) {
	if len(req.ChatsTypes) == 0 {
		return nil, status.Error(codes.InvalidArgument, "ChatsTypes is empty")
	}

	if len(req.UsersTypes) == 0 {
		return nil, status.Error(codes.InvalidArgument, "UsersTypes is empty")
	}

	return &settings.ChatsTypesKeys{
		ChatsTypes: req.ChatsTypes,
		UsersTypes: req.UsersTypes,
	}, nil
}
