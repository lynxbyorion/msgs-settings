package settings

import (
	"context"
	"errors"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/settings"
	v1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	chatapiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	someUserID   = "user-id"
	someUserType = chatapiv2.UserType_Customer
	someUser     = &chatapiv2.User{
		Type: someUserType,
		Id:   someUserID,
	}
	someChatID   = "some chat id"
	someChatType = chatapiv2.ChatType_Buyer_Support
	someSettings = settings.Settings{"push": {Value: []byte(`{"int": 4}`), Tags: []string{"web"}}}
	someTime     = time.Now()
	somePgTime   = settings.PgTime{Value: &someTime}
)

func TestImplementation_GetUsers(t *testing.T) {
	someReq := &v1.GetUsersRequest{Users: []*chatapiv2.User{someUser}}

	t.Run("InvalidArgument error if request without empty users", func(t *testing.T) {
		s := Implementation{storage: &mockStorage{}}
		req := &v1.GetUsersRequest{}

		_, err := s.GetUsers(context.Background(), req)

		st, _ := status.FromError(err)
		require.Equal(t, codes.InvalidArgument, st.Code())
	})

	t.Run("InternalError if DB return error", func(t *testing.T) {
		m := &mockStorage{
			usersByUsers: func(ctx context.Context, usersKeys settings.UsersKeys) (settings.Users, error) {
				return nil, errors.New("error")
			},
		}
		s := Implementation{storage: m}

		_, err := s.GetUsers(context.Background(), someReq)

		st, _ := status.FromError(err)
		require.Equal(t, codes.Internal, st.Code())
	})

	t.Run("merged default settings with saved settings", func(t *testing.T) {
		savedSettings := settings.Settings{
			"key1": settings.Setting{Value: []byte(`{"string": "value"}`), LastUpdateAt: time.Now()},
		}
		m := &mockStorage{
			usersByUsers: func(ctx context.Context, usersKeys settings.UsersKeys) (settings.Users, error) {
				return settings.Users{
					{
						UserID:   someUser.Id,
						UserType: someUser.Type,
						Settings: savedSettings,
					},
				}, nil
			},
		}
		s := Implementation{storage: m}

		resp, err := s.GetUsers(context.Background(), someReq)

		require.NoError(t, err)
		require.Len(t, resp.UsersSettings[0].Settings.Settings, 2)
	})
}
