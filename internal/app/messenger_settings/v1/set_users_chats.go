package settings

import (
	"context"
	"strings"

	"github.com/opentracing/opentracing-go"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/settings"
	eventsv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/events_v1"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func (s *Implementation) SetUsersChats(ctx context.Context, req *apiv1.SetUsersChatsRequest) (*apiv1.SetUsersChatsResponse, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "Implementation.SetUsersChats")
	defer span.Finish()

	params, err := setUsersChatsParams(req)
	if err != nil {
		return nil, err
	}

	ts, err := s.storage.SetUserChat(ctx, params.Key, params.Settings)
	if err != nil {
		logger.Errorf(ctx, "failed storage.SetUserChat err=%s", err)
		return nil, errInternalError
	}

	tsPB := timestamppb.New(ts)

	notification, err := usersChatsChangedEvent(tsPB, params)
	if err != nil {
		logger.Errorf(ctx, "failed usersChatsChangedEvent err=%v", err)
		return nil, errInternalError
	}

	s.eventProducer.SendEvent(ctx, notification)

	return &apiv1.SetUsersChatsResponse{
		Timestamp: tsPB,
	}, nil
}

type setUsersChatsParamsType struct {
	Key      settings.UserChatID
	Settings settings.Settings
}

func setUsersChatsParams(req *apiv1.SetUsersChatsRequest) (setUsersChatsParamsType, error) {
	if req.ChatId == "" {
		return setUsersChatsParamsType{}, errChatIDIsEmpty
	}
	user, err := newUserKey(req.User)
	if err != nil {
		return setUsersChatsParamsType{}, status.Error(codes.InvalidArgument, err.Error())
	}

	ss, err := newSettings(req.Settings)
	if err != nil {
		return setUsersChatsParamsType{}, status.Error(codes.InvalidArgument, err.Error())
	}

	return setUsersChatsParamsType{
		Key:      settings.UserChatID{User: user, ChatID: strings.ToLower(req.ChatId)},
		Settings: ss,
	}, nil
}

func usersChatsChangedEvent(ts *timestamppb.Timestamp, params setUsersChatsParamsType) (*eventsv1.Event, error) {
	ss, err := params.Settings.ToProtoV1()
	if err != nil {
		return nil, err
	}

	ev := &eventsv1.Event_Change{
		Type: eventsv1.Event_Update,
		Setting: &eventsv1.Event_Change_UserChat{
			UserChat: &apiv1.UserChatSetting{
				User:   params.Key.User.ToPB(),
				ChatId: params.Key.ChatID,
				Settings: &apiv1.Settings{
					Timestamp: ts,
					Settings:  ss,
				},
			},
		},
	}

	return &eventsv1.Event{
		Changes: []*eventsv1.Event_Change{ev},
	}, nil
}
