package settings

import (
	context "context"

	"github.com/opentracing/opentracing-go"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/settings"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *Implementation) GetUsersChats(ctx context.Context, req *apiv1.GetUsersChatsRequest) (*apiv1.GetUsersChatsResponse, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "Implementation.GetChatsUsers")
	defer span.Finish()

	usersChatIDs, err := getUsersChatsParams(req)
	if err != nil {
		return nil, err
	}

	userChatsByUsers, err := s.storage.UserChatsByUsers(ctx, usersChatIDs)
	if err != nil {
		logger.Errorf(ctx, "failed storage.UserChatsByUsers err=%s", err)
		return nil, errInternalError
	}

	userChatsByUsers.AddEmpty(usersChatIDs)

	return userChatsByUsers.ToProtoV1()
}

func getUsersChatsParams(req *apiv1.GetUsersChatsRequest) (settings.UsersChatIDs, error) {
	usersChatIDs, err := newUsersChatIDs(req.UsersChatsIds)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return usersChatIDs, nil
}
