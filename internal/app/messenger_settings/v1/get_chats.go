package settings

import (
	context "context"

	"github.com/opentracing/opentracing-go"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *Implementation) GetChats(ctx context.Context, req *apiv1.GetChatsRequest) (*apiv1.GetChatsResponse, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "Implementation.GetChats")
	defer span.Finish()

	chatsIDs, err := getChatsParams(req)
	if err != nil {
		return nil, err
	}

	chats, err := s.storage.ChatsByChatIDs(ctx, chatsIDs)
	if err != nil {
		logger.Errorf(ctx, "failed storage.ChatsByChatIDs err=%s", err)
		return nil, errInternalError
	}

	chats.AddEmptyChats(chatsIDs)
	chatsByChatIDs, err := chats.ToProtoV1()
	if err != nil {
		return nil, err
	}
	return &apiv1.GetChatsResponse{ChatSettingsByChatsIds: chatsByChatIDs}, nil
}

func getChatsParams(req *apiv1.GetChatsRequest) ([]string, error) {
	if len(req.ChatsIds) == 0 {
		return nil, status.Error(codes.InvalidArgument, "chatIDs is empty")
	}
	return stringsToLower(req.ChatsIds), nil
}
