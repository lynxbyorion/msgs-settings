package settings

import (
	context "context"
	"errors"
	"strings"
	"time"

	"gitlab.ozon.ru/messenger/messenger-settings/pkg/api/events_v1"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/settings"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	chatapiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
	scratch "gitlab.ozon.ru/platform/scratch"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	errInternalError = status.Error(codes.Internal, "internal error")

	errChatIDIsEmpty        = status.Error(codes.InvalidArgument, "ChatID is empty")
	errSettingsNamesIsEmpty = status.Error(codes.InvalidArgument, "SettingsNames is empty")
	errChatTypeIsInvalid    = status.Error(codes.InvalidArgument, "ChatType is invalid")
	errUserTypeIsInvalid    = status.Error(codes.InvalidArgument, "UserType is invalid")
)

type Storage interface {
	ChatsByChatIDs(ctx context.Context, guids []string) (settings.Chats, error)
	ChatsTypesByTypesAndUsersTypes(ctx context.Context, chatsTypes []chatapiv2.ChatType, usersTypes []chatapiv2.UserType) (settings.ChatTypes, error)
	UsersByUsers(ctx context.Context, usersKeys settings.UsersKeys) (settings.Users, error)
	UserChatsByUsers(ctx context.Context, usersChatIDs settings.UsersChatIDs) (settings.UsersChats, error)
	All(ctx context.Context, user settings.UserKey, chatID string, chatTypeKey chatapiv2.ChatType) (*settings.All, error)
	SetChats(ctx context.Context, key settings.ChatsKey, settings settings.Settings) (time.Time, error)
	SetChatsTypes(ctx context.Context, key settings.ChatsTypesKey, settings settings.Settings) (time.Time, error)
	SetUsers(ctx context.Context, user settings.UserKey, settings settings.Settings) (time.Time, error)
	SetUserChat(ctx context.Context, key settings.UserChatID, settings settings.Settings) (time.Time, error)
	DeleteChatSettings(ctx context.Context, chatID string, settingsKeys []string) (settings.DeleteResult, error)
	DeleteChatTypeSettings(ctx context.Context, key settings.ChatsTypesKey, settingsKeys []string) (settings.DeleteResult, error)
	DeleteUserSettings(ctx context.Context, key settings.UserKey, settingsKeys []string) (settings.DeleteResult, error)
	DeleteUserChatSetting(ctx context.Context, key settings.UserChatID, settingsKeys []string) (settings.DeleteResult, error)
}

type EventProducer interface {
	SendEvent(ctx context.Context, msg *events_v1.Event)
}

type Implementation struct {
	apiv1.UnimplementedSettingsV1Server
	storage       Storage
	eventProducer EventProducer
}

func NewSettingsV1(dbConn Storage, notification EventProducer) *Implementation {
	return &Implementation{
		storage:       dbConn,
		eventProducer: notification,
	}
}

// GetDescription is a simple alias to the ServiceDesc constructor.
// It makes it possible to register the service implementation @ the server.
func (s *Implementation) GetDescription() scratch.ServiceDesc {
	return apiv1.NewSettingsV1ServiceDesc(s)
}

func newUserKey(userKey *chatapiv2.User) (settings.UserKey, error) {
	if userKey.Id == "" || userKey.Type == chatapiv2.UserType_InvalidUser {
		return settings.UserKey{}, errors.New("user is invalid")
	}
	return settings.UserKey{
		ID:   strings.ToLower(userKey.Id),
		Type: userKey.Type,
	}, nil
}

func newUsersChatIDs(usersGuids []*apiv1.GetUsersChatsRequest_UserChatsIDs) (settings.UsersChatIDs, error) {
	if len(usersGuids) == 0 {
		return nil, errors.New("GetChatsUsersRequest_UserChatsIDs is empty")
	}

	s := make(settings.UsersChatIDs, len(usersGuids))
	for i, ugs := range usersGuids {
		if len(ugs.ChatsIds) == 0 {
			return nil, errors.New("GetChatsUsersRequest_UserChatsIDs.ChatsIds is empty")
		}

		user, err := settings.NewUserKey(ugs.User)
		if err != nil {
			return nil, err
		}

		s[i] = settings.UserChatIDs{
			User:     user,
			ChatsIDs: stringsToLower(ugs.ChatsIds),
		}
	}
	return s, nil
}

func newSettings(settingsProto map[string]*apiv1.SetSetting) (settings.Settings, error) {
	if len(settingsProto) == 0 {
		return nil, errors.New("settings is empty")
	}

	r := runtime.JSONPb{}
	ss := make(settings.Settings, len(settingsProto))
	for k, v := range settingsProto {
		b, err := r.Marshal(v.Value)
		if err != nil {
			return nil, err
		}

		ss[strings.ToLower(k)] = settings.Setting{
			Value:        b,
			LastUpdateAt: time.Now(),
			Tags:         stringsToLower(v.Tags),
		}
	}
	return ss, nil
}

func stringsToLower(strs []string) []string {
	res := make([]string, len(strs))
	for i, s := range strs {
		res[i] = strings.ToLower(s)
	}
	return res
}

func settingsOnlyKeys(keys []string) map[string]*apiv1.Setting {
	ss := make(map[string]*apiv1.Setting, len(keys))
	for _, k := range keys {
		ss[k] = nil
	}
	return ss
}
