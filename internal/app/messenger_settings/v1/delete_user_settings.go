package settings

import (
	"context"

	"github.com/opentracing/opentracing-go"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/settings"
	eventsv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/events_v1"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func (s *Implementation) DeleteUserSettings(ctx context.Context, req *apiv1.DeleteUserSettingsRequest) (*apiv1.DeleteUserSettingsResponse, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "Implementation.DeleteUserSettings")
	defer span.Finish()

	params, err := deleteUserSettingsParams(req)
	if err != nil {
		return nil, err
	}

	res, err := s.storage.DeleteUserSettings(ctx, params.Key, params.SettingsNames)
	if err != nil {
		if err == settings.NotFoundInStorage {
			return nil, status.Errorf(codes.NotFound, "settings by user = %v not found", params.Key)
		}
		logger.Error(ctx, "failed storage.DeleteUserSettings err=%s", err)
		return nil, errInternalError
	}

	tsPB := timestamppb.New(res.Timestmap)

	s.eventProducer.SendEvent(ctx, deleteUserSettingsEvent(tsPB, params.Key, res.Names))

	return &apiv1.DeleteUserSettingsResponse{
		Timestamp: tsPB,
		Deleted:   res.Names,
	}, nil
}

type deleteUserSettingsParamsType struct {
	Key           settings.UserKey
	SettingsNames []string
}

func deleteUserSettingsParams(req *apiv1.DeleteUserSettingsRequest) (*deleteUserSettingsParamsType, error) {
	if len(req.SettingNames) == 0 {
		return nil, errSettingsNamesIsEmpty
	}

	user, err := settings.NewUserKey(req.User)
	if err != nil {
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return &deleteUserSettingsParamsType{
		Key:           user,
		SettingsNames: req.SettingNames,
	}, nil
}

func deleteUserSettingsEvent(ts *timestamppb.Timestamp, key settings.UserKey, settingsNames []string) *eventsv1.Event {
	ev := &eventsv1.Event_Change{
		Type: eventsv1.Event_Delete,
		Setting: &eventsv1.Event_Change_User{
			User: &apiv1.UserSetting{
				User: key.ToPB(),
				Settings: &apiv1.Settings{
					Timestamp: ts,
					Settings:  settingsOnlyKeys(settingsNames),
				},
			},
		},
	}

	return &eventsv1.Event{
		Changes: []*eventsv1.Event_Change{ev},
	}
}
