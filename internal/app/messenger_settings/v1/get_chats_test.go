package settings

import (
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/settings"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	chatapiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
)

func TestMessengerSettingsV1_GetChats(t *testing.T) {
	chatID := "0000000"
	s := Implementation{
		storage: &mockStorage{
			chatsByChatIDs: func(ctx context.Context, guids []string) (settings.Chats, error) {
				return settings.Chats{{
					ID:           1,
					ChatID:       chatID,
					LastUpdateAt: somePgTime,
					Settings: settings.Settings{"push": {
						Value:        []byte(`{"bool": true}`),
						LastUpdateAt: time.Now(),
						Tags:         []string{"app"},
					}},
				}}, nil
			},
		},
	}

	resp, err := s.GetChats(context.Background(), &apiv1.GetChatsRequest{ChatsIds: []string{chatID}})

	require.NoError(t, err)
	assert.Equal(t, true, resp.ChatSettingsByChatsIds[chatID].Settings.Settings["push"].Value.GetBool())

}

type mockStorage struct {
	Storage
	usersByUsers   func(ctx context.Context, usersKeys settings.UsersKeys) (settings.Users, error)
	chatsByChatIDs func(ctx context.Context, guids []string) (settings.Chats, error)
	all            func(ctx context.Context, user settings.UserKey, chatID string, chatTypeKey chatapiv2.ChatType) (*settings.All, error)
}

func (s *mockStorage) UsersByUsers(ctx context.Context, usersKeys settings.UsersKeys) (settings.Users, error) {
	return s.usersByUsers(ctx, usersKeys)
}

func (s *mockStorage) ChatsByChatIDs(ctx context.Context, guids []string) (settings.Chats, error) {
	return s.chatsByChatIDs(ctx, guids)
}

func (s *mockStorage) All(ctx context.Context, user settings.UserKey, chatID string, chatTypeKey chatapiv2.ChatType) (*settings.All, error) {
	return s.all(ctx, user, chatID, chatTypeKey)
}
