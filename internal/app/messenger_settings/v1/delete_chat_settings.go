package settings

import (
	"context"
	"strings"

	"github.com/opentracing/opentracing-go"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/settings"
	eventsv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/events_v1"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func (s *Implementation) DeleteChatSettings(ctx context.Context, req *apiv1.DeleteChatSettingsRequest) (*apiv1.DeleteChatSettingsResponse, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "Implementation.DeleteChatSettings")
	defer span.Finish()

	params, err := deleteChatSettingsParams(req)
	if err != nil {
		return nil, err
	}

	res, err := s.storage.DeleteChatSettings(ctx, params.ChatID, params.SettingsNames)
	if err != nil {
		if err == settings.NotFoundInStorage {
			return nil, status.Errorf(codes.NotFound, "settings for chatID = %s not found", req.ChatId)
		}
		logger.Errorf(ctx, "failed storage.DeleteChatSettings err=%s", err)
		return nil, errInternalError
	}

	tsPB := timestamppb.New(res.Timestmap)

	s.eventProducer.SendEvent(ctx, deleteChatSettingsEvent(tsPB, params.ChatID, res.Names))

	return &apiv1.DeleteChatSettingsResponse{
		Timestamp: tsPB,
		Deleted:   res.Names,
	}, nil
}

type deleteChatSettingsParamsType struct {
	ChatID        string
	SettingsNames []string
}

func deleteChatSettingsParams(req *apiv1.DeleteChatSettingsRequest) (*deleteChatSettingsParamsType, error) {
	if req.ChatId == "" {
		return nil, errChatIDIsEmpty
	}
	if len(req.SettingNames) == 0 {
		return nil, errSettingsNamesIsEmpty
	}

	return &deleteChatSettingsParamsType{
		ChatID:        strings.ToLower(req.ChatId),
		SettingsNames: req.SettingNames,
	}, nil
}

func deleteChatSettingsEvent(ts *timestamppb.Timestamp, key string, settingsNames []string) *eventsv1.Event {
	ev := &eventsv1.Event_Change{
		Type: eventsv1.Event_Delete,
		Setting: &eventsv1.Event_Change_Chat{
			Chat: &apiv1.ChatSetting{
				ChatId: key,
				Settings: &apiv1.Settings{
					Timestamp: ts,
					Settings:  settingsOnlyKeys(settingsNames),
				},
			},
		},
	}

	return &eventsv1.Event{
		Changes: []*eventsv1.Event_Change{ev},
	}
}
