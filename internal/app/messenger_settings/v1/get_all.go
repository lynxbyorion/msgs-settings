package settings

import (
	context "context"
	"strings"

	"github.com/opentracing/opentracing-go"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/settings"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *Implementation) GetAll(ctx context.Context, req *apiv1.GetAllRequest) (*apiv1.GetAllResponse, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "Implementation.GetAll")
	defer span.Finish()

	params, err := getAllParams(req)
	if err != nil {
		return nil, err
	}

	all, err := s.storage.All(ctx, params.UserKeys, params.ChatID, params.ChatType)
	if err != nil {
		logger.Errorf(ctx, "failed storage.All err=%s", err)
		return nil, errInternalError
	}

	all.AddEmpty(params)
	all.AddDefault()

	return all.ToProtoV1()
}

func getAllParams(req *apiv1.GetAllRequest) (settings.AllKeys, error) {
	userKey, err := newUserKey(req.User)
	if err != nil {
		return settings.AllKeys{}, status.Error(codes.InvalidArgument, err.Error())
	}

	if len(req.ChatId) == 0 {
		return settings.AllKeys{}, errChatIDIsEmpty
	}

	if req.ChatType == 0 {
		return settings.AllKeys{}, errChatTypeIsInvalid
	}

	return settings.AllKeys{
		UserKeys: userKey,
		ChatID:   strings.ToLower(req.ChatId),
		ChatType: req.ChatType,
	}, nil
}
