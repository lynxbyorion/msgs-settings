package settings

import (
	"context"

	"github.com/opentracing/opentracing-go"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/settings"
	eventsv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/events_v1"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func (s *Implementation) SetChats(ctx context.Context, req *apiv1.SetChatsRequest) (*apiv1.SetChatsResponse, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "Implementation.SetChats")
	defer span.Finish()

	chatID, setSettings, err := setChatsParams(req)
	if err != nil {
		return nil, err
	}

	ts, err := s.storage.SetChats(ctx, chatID, setSettings)
	if err != nil {
		logger.Errorf(ctx, "failed storage.SetChats err=%s", err)
		return nil, errInternalError
	}

	tsPB := timestamppb.New(ts)

	notification, err := chatChangedEvent(tsPB, setSettings, chatID)
	if err != nil {
		logger.Errorf(ctx, "failed chatChangedEvent err=%v", err)
		return nil, errInternalError
	}

	s.eventProducer.SendEvent(ctx, notification)

	return &apiv1.SetChatsResponse{
		Timestamp: tsPB,
	}, nil
}

func setChatsParams(req *apiv1.SetChatsRequest) (settings.ChatsKey, settings.Settings, error) {
	if len(req.ChatId) == 0 {
		return settings.ChatsKey{}, nil, status.Error(codes.InvalidArgument, "chatId is empty")
	}

	ss, err := newSettings(req.Settings)
	if err != nil {
		return settings.ChatsKey{}, nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return settings.NewChatsKey(req.ChatId), ss, nil

}

func chatChangedEvent(ts *timestamppb.Timestamp, setSettings settings.Settings, key settings.ChatsKey) (*eventsv1.Event, error) {
	ss, err := setSettings.ToProtoV1()
	if err != nil {
		return nil, err
	}

	ev := &eventsv1.Event_Change{
		Type:    eventsv1.Event_Update,
		Setting: &eventsv1.Event_Change_Chat{Chat: &apiv1.ChatSetting{ChatId: key.ChatID, Settings: &apiv1.Settings{Timestamp: ts, Settings: ss}}},
	}

	return &eventsv1.Event{
		Changes: []*eventsv1.Event_Change{ev},
	}, nil
}
