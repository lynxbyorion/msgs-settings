package settings

import (
	context "context"

	"github.com/opentracing/opentracing-go"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/settings"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *Implementation) GetUsers(ctx context.Context, req *apiv1.GetUsersRequest) (*apiv1.GetUsersResponse, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "Implementation.GetUsers")
	defer span.Finish()

	usersKeys, err := getUsersParams(req)
	if err != nil {
		return nil, err
	}

	users, err := s.storage.UsersByUsers(ctx, usersKeys)
	if err != nil {
		logger.Errorf(ctx, "failed storage.UsersByUsers err=%s", err)
		return nil, errInternalError
	}

	users.AddEmpty(usersKeys)
	users.AddDefault()

	usersProto, err := users.ToProtoV1()
	if err != nil {
		return nil, err
	}

	return &apiv1.GetUsersResponse{UsersSettings: usersProto}, nil
}

func getUsersParams(req *apiv1.GetUsersRequest) (settings.UsersKeys, error) {
	if len(req.Users) == 0 {
		return nil, status.Error(codes.InvalidArgument, "Users is empty")
	}

	return settings.NewUsersKeys(req.Users)
}
