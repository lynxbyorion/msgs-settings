package kafka

import (
	"context"

	"github.com/Shopify/sarama"
	"github.com/opentracing/opentracing-go"
	"gitlab.ozon.ru/messenger/messenger-settings/pkg/api/events_v1"
	"gitlab.ozon.ru/platform/kafka-client-go/pkg/kafka"
	"gitlab.ozon.ru/platform/kafka-client-go/pkg/kafka/client"
	"gitlab.ozon.ru/platform/kafka-client-go/pkg/kafka/middleware"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/protobuf/proto"
)

type Producer struct {
	producer kafka.AsyncProducer
	topic    string
}

func NewProducer(topic string, brokers []string) (*Producer, error) {
	cfg := sarama.NewConfig()
	cfg.Version = sarama.V1_1_0_0
	cfg.ChannelBufferSize = 1024
	cfg.Producer.Idempotent = true
	cfg.Producer.RequiredAcks = sarama.WaitForAll
	cfg.Producer.Retry.Max = 100
	cfg.Producer.Partitioner = sarama.NewHashPartitioner
	cfg.Net.MaxOpenRequests = 1

	producer, err := client.NewAsyncProducer(
		brokers,
		cfg,
		middleware.AsyncProducerMetricMiddleware,
		middleware.AsyncProducerTraceMiddleware,
	)
	if err != nil {
		return nil, err
	}

	go func() {
		for err := range producer.Errors() {
			ctx := context.Background()
			logger.Error(ctx, "Kafka async producer error", err)
		}
	}()

	return &Producer{
		topic:    topic,
		producer: producer,
	}, nil
}

func (m *Producer) SendEvent(ctx context.Context, msg *events_v1.Event) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "kafka-send-message")
	defer span.Finish()

	b, err := proto.Marshal(msg)
	if err != nil {
		logger.Errorf(ctx, "failed sendEventAsync proto.Marshal err=%v", err)
		return
	}
	span.LogKV("message", string(b))

	m.producer.SendMessageWithContext(ctx,
		&sarama.ProducerMessage{Topic: m.topic, Value: sarama.ByteEncoder(b)},
	)
}

func (m *Producer) Stop() error {
	return m.producer.Close()
}
