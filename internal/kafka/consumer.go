package kafka

import (
	"context"
	"time"

	"github.com/Shopify/sarama"
	"gitlab.ozon.ru/messenger/messenger-settings/pkg/api/events_v1"
	"gitlab.ozon.ru/platform/kafka-client-go/pkg/kafka/client"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/protobuf/proto"
)

const (
	retryAfter       = 2 * time.Second
	offsetsRetention = 1 * time.Hour
)

func NewConsumerGroup(clientID, groupID string, brokers []string) (sarama.ConsumerGroup, error) {
	config := sarama.NewConfig()
	config.ClientID = clientID
	config.Consumer.Return.Errors = true
	config.Version = sarama.V1_1_1_0
	config.Consumer.Offsets.Retention = offsetsRetention

	return client.NewConsumerGroup(brokers, groupID, config)
}

// ListenTopic creates channels for listening kafka topics from newest offset
func ListenTopic(ctx context.Context, topics []string, consumerGroup sarama.ConsumerGroup, stop chan struct{}) chan *events_v1.Event {
	go func() {
		for err := range consumerGroup.Errors() {
			logger.Errorf(ctx, "kafka error: %s", err)
		}
	}()

	consuming := make(chan *events_v1.Event)
	go func(c chan<- *events_v1.Event) {
		for {
			select {
			case <-stop:
				close(c)
				return
			default:
			}

			err := consumerGroup.Consume(ctx, topics, &consumerGroupHandler{c: c, logger: logger.FromContext(ctx).Errorf})
			if err != nil {
				if err == sarama.ErrNotCoordinatorForConsumer {
					logger.Fatalf(ctx, "kafka fatal err=%s", sarama.ErrNotCoordinatorForConsumer)
				}

				logger.Errorf(ctx, "can't consume topics topics=%s err=%s", topics, err)
				time.Sleep(retryAfter)
				continue
			}
		}
	}(consuming)

	return consuming
}

type consumerGroupHandler struct {
	logger func(format string, args ...interface{})
	c      chan<- *events_v1.Event
}

func (consumerGroupHandler) Setup(_ sarama.ConsumerGroupSession) error   { return nil }
func (consumerGroupHandler) Cleanup(_ sarama.ConsumerGroupSession) error { return nil }
func (h consumerGroupHandler) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	for {
		select {
		case msg, ok := <-claim.Messages():
			if !ok {
				return nil
			}
			var e events_v1.Event
			if err := proto.Unmarshal(msg.Value, &e); err != nil {
				return err
			}
			h.c <- &e
		case <-session.Context().Done():
			return nil
		}
	}
}
