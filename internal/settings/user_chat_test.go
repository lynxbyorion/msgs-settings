package settings

import (
	"testing"

	"github.com/stretchr/testify/require"
	chatapiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
)

var (
	someUserID        = "user-id"
	someOtherUserID   = "other-user-id"
	someUserType      = chatapiv2.UserType_Customer
	someOtherUserType = chatapiv2.UserType_Seller
)

func TestUsersChats_AddEmpty(t *testing.T) {
	t.Run("add", func(t *testing.T) {
		usersChats := UsersChats{{
			UserID:   someUserID,
			UserType: someUserType,
			ChatID:   someChatID,
			Settings: someSettings,
		}}

		keys := UsersChatIDs{{
			User: UserKey{
				ID:   someOtherUserID,
				Type: someOtherUserType,
			},
			ChatsIDs: []string{someOtherChatID, "chat-id"},
		}}

		usersChats.AddEmpty(keys)

		require.Len(t, usersChats, 3)
	})
}
