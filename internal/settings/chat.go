package settings

import (
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type Chat struct {
	ID           int64
	ChatID       string   `json:"chat_id" db:"chat_id"`
	LastUpdateAt PgTime   `json:"last_update_at" db:"last_update_at"`
	Settings     Settings `json:"settings" db:"settings"`
}

func (s *Chat) ToProtoV1() (*apiv1.ChatSetting, error) {
	settings, err := s.Settings.ToProtoV1()
	if err != nil {
		return nil, err
	}

	var timePB *timestamppb.Timestamp
	if s.LastUpdateAt.Value != nil {
		timePB = timestamppb.New(*s.LastUpdateAt.Value)
	}

	return &apiv1.ChatSetting{
		ChatId: s.ChatID,
		Settings: &apiv1.Settings{
			Timestamp: timePB,
			Settings:  settings,
		},
	}, nil
}

func (s *Chat) AddSettings(newSettings Settings) {
	for k, v := range newSettings {
		s.Settings[k] = v
	}
}

type Chats []Chat

func (s Chats) ToProtoV1() (map[string]*apiv1.ChatSetting, error) {
	chats := make(map[string]*apiv1.ChatSetting, len(s))
	var err error
	for _, v := range s {
		chats[v.ChatID], err = v.ToProtoV1()
		if err != nil {
			return nil, err
		}
	}
	return chats, nil
}

func (s *Chats) AddEmptyChats(ids []string) {
	var empty Chats
	for _, id := range ids {
		if !s.isExist(id) {
			empty = append(empty, Chat{
				ChatID:   id,
				Settings: Settings{},
			})
		}
	}
	*s = append(*s, empty...)
}

func (s Chats) isExist(chatID string) bool {
	for _, chat := range s {
		if chat.ChatID == chatID {
			return true
		}
	}
	return false
}
