package settings

import (
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	chat_apiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
)

const (
	pushEnable = "push_enable"
)

func init() {
	initDefaultSettings(
		usersByUserType(
			userTypeAsKey(chat_apiv2.UserType_Customer,
				setting(pushEnable, &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}),
			),
			userTypeAsKey(chat_apiv2.UserType_Principal,
				setting(pushEnable, &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}),
			),
			userTypeAsKey(chat_apiv2.UserType_Seller,
				setting(pushEnable, &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}),
			),
		),
		chatTypesByChatType(
			chatTypeAsKey(chat_apiv2.ChatType_Buyer_Support,
				setting(pushEnable, &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}),
			),
			chatTypeAsKey(chat_apiv2.ChatType_Buyer_Seller,
				setting(pushEnable, &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}),
			),
			chatTypeAsKey(chat_apiv2.ChatType_Principal_Support,
				setting(pushEnable, &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}),
			),
		),
	)
}
