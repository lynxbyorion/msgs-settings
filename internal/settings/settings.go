package settings

import (
	"database/sql/driver"
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	chatapiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
	"google.golang.org/protobuf/types/known/timestamppb"
)

var (
	NotFoundInStorage error
)

type ChatsKey struct {
	ChatID string `json:"chat_id"`
}

func NewChatsKey(guid string) ChatsKey {
	return ChatsKey{ChatID: strings.ToLower(guid)}
}

type ChatsTypesKey struct {
	ChatType chatapiv2.ChatType
	UserType chatapiv2.UserType
}

type UserKey struct {
	ID   string             `json:"user_id" db:"user_id"`
	Type chatapiv2.UserType `json:"user_type" db:"user_type"`
}

func NewUserKey(user *chatapiv2.User) (UserKey, error) {
	if user.Id == "" {
		return UserKey{}, errors.New("user.id is empty")
	}
	if user.Type == chatapiv2.UserType_InvalidUser {
		return UserKey{}, errors.New("user.type is invalid")
	}
	return UserKey{
		ID:   strings.ToLower(user.Id),
		Type: user.Type,
	}, nil
}

func (s UserKey) ToPB() *chatapiv2.User {
	return &chatapiv2.User{
		Type: s.Type,
		Id:   s.ID,
	}
}

type UsersKeys []UserKey

func NewUsersKeys(usersKeys []*chatapiv2.User) (UsersKeys, error) {
	uks := make(UsersKeys, len(usersKeys))
	for i, u := range usersKeys {
		var err error
		uks[i], err = NewUserKey(u)
		if err != nil {
			return nil, err
		}
	}
	return uks, nil
}

type ChatsTypesKeys struct {
	ChatsTypes []chatapiv2.ChatType
	UsersTypes []chatapiv2.UserType
}

type AllKeys struct {
	UserKeys UserKey
	ChatID   string
	ChatType chatapiv2.ChatType
}

type UserChatID struct {
	User   UserKey `json:"user"`
	ChatID string  `json:"chat_id"`
}

func NewUserChatID(user *chatapiv2.User, chatID string) (*UserChatID, error) {
	u, err := NewUserKey(user)
	if err != nil {
		return nil, err
	}

	if chatID == "" {
		return nil, errors.New("chatID is empty")
	}

	return &UserChatID{
		User:   u,
		ChatID: strings.ToLower(chatID),
	}, nil
}

type UserChatIDs struct {
	User     UserKey
	ChatsIDs []string
}

type UsersChatIDs []UserChatIDs

type ChatIDs []string

type DeleteResult struct {
	Timestmap time.Time
	Names     []string
}

type All struct {
	Chat     *Chat     `json:"chat"`
	ChatType *ChatType `json:"chat_type"`
	User     *User     `json:"user"`
	UserChat *UserChat `json:"user_chat"`
}

func (s *All) ToProtoV1() (*apiv1.GetAllResponse, error) {
	chatType, err := s.chatTypeToProtoV1()
	if err != nil {
		return nil, fmt.Errorf("failed chat type to proto: %w", err)
	}

	chat, err := s.chatToProtoV1()
	if err != nil {
		return nil, fmt.Errorf("failed chat to proto: %w", err)
	}

	user, err := s.userToProtoV1()
	if err != nil {
		return nil, fmt.Errorf("failed user to proto: %w", err)
	}

	userChat, err := s.userChatToProtoV1()
	if err != nil {
		return nil, fmt.Errorf("failed user chat to proto: %w", err)
	}

	return &apiv1.GetAllResponse{
		ChatType: chatType,
		Chat:     chat,
		User:     user,
		UserChat: userChat,
	}, nil
}

func (s *All) AddDefault() {
	s.User.AddDefault()
	s.ChatType.AddDefault()
}

func (s *All) chatTypeToProtoV1() (*apiv1.ChatTypeSetting, error) {
	if s.ChatType == nil {
		return nil, nil
	}
	return s.ChatType.ToProtoV1()
}

func (s *All) chatToProtoV1() (*apiv1.ChatSetting, error) {
	if s.Chat == nil {
		return nil, nil
	}
	return s.Chat.ToProtoV1()
}

func (s *All) userToProtoV1() (*apiv1.UserSetting, error) {
	if s.User == nil {
		return nil, nil
	}
	return s.User.ToProtoV1()
}

func (s *All) userChatToProtoV1() (*apiv1.UserChatSetting, error) {
	if s.UserChat == nil {
		return nil, nil
	}
	return s.UserChat.ToProtoV1()
}

func (s *All) AddEmpty(keys AllKeys) {
	if s.User == nil {
		s.User = &User{
			UserID:   keys.UserKeys.ID,
			UserType: keys.UserKeys.Type,
			Settings: Settings{},
		}
	}

	if s.ChatType == nil {
		s.ChatType = &ChatType{
			ChatType: keys.ChatType,
			UserType: keys.UserKeys.Type,
			Settings: Settings{},
		}
	}

	if s.Chat == nil {
		s.Chat = &Chat{
			ChatID:   keys.ChatID,
			Settings: Settings{},
		}
	}

	if s.UserChat == nil {
		s.UserChat = &UserChat{
			UserID:   keys.UserKeys.ID,
			UserType: keys.UserKeys.Type,
			ChatID:   keys.ChatID,
			Settings: Settings{},
		}
	}
}

type Setting struct {
	Value        json.RawMessage `json:"value"`
	LastUpdateAt time.Time       `json:"last_update_at"`
	Tags         []string        `json:"tags"`
}

type Data struct {
	Raw []byte `db:"raw"`
}

type Settings map[string]Setting

func (s Settings) Exists(keys []string) []string {
	var res []string
	for _, k := range keys {
		if _, ok := s[k]; ok {
			res = append(res, k)
		}
	}
	return res
}

func (s Settings) Value() (driver.Value, error) {
	bs, err := json.Marshal(s)
	if err != nil {
		return nil, err
	}
	return string(bs), nil
}

func (s *Settings) Scan(value interface{}) error {
	b, ok := value.([]byte)
	if !ok {
		return errors.New("type assertion to []byte failed")
	}

	return json.Unmarshal(b, &s)
}

func (s Settings) ToProtoV1() (map[string]*apiv1.Setting, error) {
	r := make(map[string]*apiv1.Setting, len(s))
	rpb := runtime.JSONPb{}
	for k, v := range s {
		var res apiv1.Value
		if err := rpb.Unmarshal(v.Value, &res); err != nil {
			return nil, fmt.Errorf("failed jsonpb.Unmarshal: %w", err)
		}

		r[k] = &apiv1.Setting{
			Value:        &res,
			LastUpdateAt: timestamppb.New(v.LastUpdateAt),
			Tags:         v.Tags,
		}
	}
	return r, nil
}

func (s Settings) Merge(extraSettings Settings) {
	for k, v := range extraSettings {
		_, ok := s[k]
		if !ok {
			s[k] = v
		}
	}
}

type PgTime struct {
	Value *time.Time
}

func (s *PgTime) UnmarshalJSON(rawTime []byte) error {
	p, err := time.Parse("\"2006-01-02T15:04:05.999999999\"", string(rawTime))
	if err != nil {
		return fmt.Errorf("failed to parse postgres time err=%w", err)
	}
	s.Value = &p
	return nil
}

func (s *PgTime) Scan(value interface{}) error {
	t, ok := value.(time.Time)
	if !ok {
		return errors.New("type assertion to *time.Time failed")
	}

	s.Value = &t

	return nil
}
