package settings

import (
	"testing"

	"github.com/stretchr/testify/require"
)

var (
	someChatID      = "00000000-0000-0000-0000-000000000000"
	someOtherChatID = "11111111-1111-1111-1111-111111111111"
	someSettings    = Settings{"push": {Value: []byte(`{"int": 4}`), Tags: []string{"web"}}}
)

func TestChats_AddEmptyChats(t *testing.T) {
	t.Run("add for empty", func(t *testing.T) {
		chs := Chats{}
		ids := []string{someChatID}

		chs.AddEmptyChats(ids)

		require.Len(t, chs, 1)
		require.Equal(t, someChatID, chs[0].ChatID)
	})

	t.Run("add", func(t *testing.T) {
		chs := Chats{{
			ID:       1,
			ChatID:   someChatID,
			Settings: someSettings,
		}}
		ids := []string{someOtherChatID}

		chs.AddEmptyChats(ids)

		require.Len(t, chs, 2)
		require.Equal(t, someOtherChatID, chs[1].ChatID)
	})
}
