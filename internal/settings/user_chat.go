package settings

import (
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	chatapiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type UserChat struct {
	ID           int64
	UserID       string             `json:"user_id" db:"user_id"`
	UserType     chatapiv2.UserType `json:"user_type" db:"user_type"`
	ChatID       string             `json:"chat_id" db:"chat_id"`
	LastUpdateAt PgTime             `json:"last_update_at" db:"last_update_at"`
	Settings     Settings           `json:"settings" db:"settings"`
}

func (s *UserChat) AddSettings(newSettings Settings) {
	for k, v := range newSettings {
		s.Settings[k] = v
	}
}

func (s UserChat) ToProtoV1() (*apiv1.UserChatSetting, error) {
	settings, err := s.Settings.ToProtoV1()
	if err != nil {
		return nil, err
	}

	var lastUpdateAtPB *timestamppb.Timestamp
	if s.LastUpdateAt.Value != nil {
		lastUpdateAtPB = timestamppb.New(*s.LastUpdateAt.Value)
	}

	return &apiv1.UserChatSetting{
		User: &chatapiv2.User{
			Id:   s.UserID,
			Type: s.UserType,
		},
		ChatId: s.ChatID,
		Settings: &apiv1.Settings{
			Timestamp: lastUpdateAtPB,
			Settings:  settings,
		},
	}, nil
}

type UsersChats []UserChat

func (s UsersChats) ToProtoV1() (*apiv1.GetUsersChatsResponse, error) {
	usersChatsSettings := make([]*apiv1.UserChatSetting, len(s))
	var err error
	for i, uch := range s {
		usersChatsSettings[i], err = uch.ToProtoV1()
		if err != nil {
			return nil, err
		}
	}

	return &apiv1.GetUsersChatsResponse{UsersChatsSettings: usersChatsSettings}, nil
}

func (s *UsersChats) AddEmpty(keys UsersChatIDs) {
	var empty UsersChats
	for _, key := range keys {
		for _, ch := range key.ChatsIDs {
			if !s.isExist(key.User, ch) {
				empty = append(empty, UserChat{
					UserID:   key.User.ID,
					UserType: key.User.Type,
					ChatID:   ch,
					Settings: Settings{},
				})
			}
		}
	}
	*s = append(*s, empty...)
}

func (s UsersChats) isExist(user UserKey, chatID string) bool {
	for _, uc := range s {
		if uc.UserID == user.ID && uc.UserType == user.Type && uc.ChatID == chatID {
			return true
		}
	}
	return false
}
