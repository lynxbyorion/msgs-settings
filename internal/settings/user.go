package settings

import (
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	chat_apiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type User struct {
	ID           int64
	UserID       string              `json:"user_id" db:"user_id"`
	UserType     chat_apiv2.UserType `json:"user_type" db:"user_type"`
	LastUpdateAt PgTime              `json:"last_update_at" db:"last_update_at"`
	Settings     Settings            `json:"settings" db:"settings"`
}

func (s User) ToProtoV1() (*apiv1.UserSetting, error) {
	settings, err := s.Settings.ToProtoV1()
	if err != nil {
		return nil, err
	}

	var timePB *timestamppb.Timestamp
	if s.LastUpdateAt.Value != nil {
		timePB = timestamppb.New(*s.LastUpdateAt.Value)
	}

	return &apiv1.UserSetting{
		User: &chat_apiv2.User{
			Id:   s.UserID,
			Type: s.UserType,
		},
		Settings: &apiv1.Settings{
			Timestamp: timePB,
			Settings:  settings,
		},
	}, nil
}

func (s User) AddDefault() {
	s.Settings.Merge(curDefaultSettings.usersByUserType[s.UserType])
}

type Users []User

func (s *Users) AddEmpty(keys UsersKeys) {
	var emptyUsers []User
	for _, k := range keys {
		if !s.isExist(k) {
			emptyUsers = append(emptyUsers, User{
				UserID:   k.ID,
				UserType: k.Type,
				Settings: Settings{},
			})
		}
	}
	*s = append(*s, emptyUsers...)
}

func (s Users) isExist(key UserKey) bool {
	for _, u := range s {
		if u.UserID == key.ID && u.UserType == key.Type {
			return true
		}
	}
	return false
}

func (s Users) ToProtoV1() ([]*apiv1.UserSetting, error) {
	usersProto := make([]*apiv1.UserSetting, len(s))
	var err error
	for i, v := range s {
		usersProto[i], err = v.ToProtoV1()
		if err != nil {
			return nil, err
		}
	}

	return usersProto, nil
}

func (s Users) AddDefault() {
	for _, u := range s {
		u.AddDefault()
	}
}
