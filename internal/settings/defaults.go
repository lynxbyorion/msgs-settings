package settings

import (
	"strings"

	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	chatapiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
)

type defaultSettings struct {
	usersByUserType     map[chatapiv2.UserType]Settings
	chatTypesByChatType map[chatapiv2.ChatType]Settings
}

var curDefaultSettings defaultSettings

func initDefaultSettings(usersByUserType map[chatapiv2.UserType]Settings, chatTypesByChatType map[chatapiv2.ChatType]Settings) {
	curDefaultSettings = defaultSettings{
		usersByUserType:     usersByUserType,
		chatTypesByChatType: chatTypesByChatType,
	}
}

func usersByUserType(usersByUserType ...userTypeWithKey) map[chatapiv2.UserType]Settings {
	res := make(map[chatapiv2.UserType]Settings, len(usersByUserType))
	for _, m := range usersByUserType {
		res[m.key] = m.Settings()
	}
	return res
}

func chatTypesByChatType(chatTypesWithKeys ...chatTypesWithKeys) map[chatapiv2.ChatType]Settings {
	res := make(map[chatapiv2.ChatType]Settings, len(chatTypesWithKeys))
	for _, m := range chatTypesWithKeys {
		res[m.key] = m.Settings()
	}
	return res
}

type settingsList struct {
	settings []settingWithKey
}

func (s *settingsList) Settings() Settings {
	res := make(Settings, len(s.settings))
	for _, v := range s.settings {
		res[v.key] = v.value
	}
	return res
}

type userTypeWithKey struct {
	key chatapiv2.UserType
	settingsList
}

func userTypeAsKey(key chatapiv2.UserType, settings ...settingWithKey) userTypeWithKey {
	return userTypeWithKey{
		key:          key,
		settingsList: settingsList{settings: settings},
	}
}

type chatTypesWithKeys struct {
	key chatapiv2.ChatType
	settingsList
}

func chatTypeAsKey(key chatapiv2.ChatType, settings ...settingWithKey) chatTypesWithKeys {
	return chatTypesWithKeys{
		key:          key,
		settingsList: settingsList{settings},
	}
}

type settingWithKey struct {
	key   string
	value Setting
}

func setting(key string, value *apiv1.Value, tags ...string) settingWithKey {
	rpb := runtime.JSONPb{}
	r, err := rpb.Marshal(value)
	if err != nil {
		panic(err)
	}
	return settingWithKey{key: strings.ToLower(key), value: Setting{Value: r, Tags: tags}}
}
