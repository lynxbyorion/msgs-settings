package settings

import (
	"testing"

	"github.com/stretchr/testify/require"
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	chatapiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
)

var (
	someAPIV1Settings = &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}
)

func Test_initDefaultSettings(t *testing.T) {
	expSettings := defaultSettings{
		usersByUserType: map[chatapiv2.UserType]Settings{
			chatapiv2.UserType_Customer: {
				pushEnable: Setting{
					Value: []byte(`{"bool":true}`),
					Tags:  []string{"app", "web"},
				},
			},
			chatapiv2.UserType_Seller: {
				pushEnable: Setting{
					Value: []byte(`{"bool":true}`),
				},
			},
		},
		chatTypesByChatType: map[chatapiv2.ChatType]Settings{
			chatapiv2.ChatType_Buyer_Support: {
				pushEnable: Setting{
					Value: []byte(`{"bool":true}`),
				},
			},
			chatapiv2.ChatType_Buyer_Seller: {
				pushEnable: Setting{
					Value: []byte(`{"bool":true}`),
				},
			},
		},
	}

	initDefaultSettings(
		usersByUserType(
			userTypeAsKey(chatapiv2.UserType_Customer,
				setting(pushEnable, someAPIV1Settings, "app", "web"),
			),
			userTypeAsKey(chatapiv2.UserType_Seller,
				setting(pushEnable, someAPIV1Settings),
			),
		),
		chatTypesByChatType(
			chatTypeAsKey(chatapiv2.ChatType_Buyer_Support,
				setting(pushEnable, someAPIV1Settings),
			),
			chatTypeAsKey(chatapiv2.ChatType_Buyer_Seller,
				setting(pushEnable, someAPIV1Settings),
			),
		),
	)

	require.Equal(t, expSettings, curDefaultSettings)
}
