package settings

import (
	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	chatapiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
	"google.golang.org/protobuf/types/known/timestamppb"
)

type ChatType struct {
	ID           int64
	ChatType     chatapiv2.ChatType `json:"chat_type" db:"chat_type"`
	UserType     chatapiv2.UserType `json:"user_type" db:"user_type"`
	LastUpdateAt PgTime             `json:"last_update_at" db:"last_update_at"`
	Settings     Settings           `json:"settings" db:"settings"`
}

func (s *ChatType) ToProtoV1() (*apiv1.ChatTypeSetting, error) {
	settings, err := s.Settings.ToProtoV1()
	if err != nil {
		return nil, err
	}

	var timePB *timestamppb.Timestamp
	if s.LastUpdateAt.Value != nil {
		timePB = timestamppb.New(*s.LastUpdateAt.Value)
	}

	return &apiv1.ChatTypeSetting{
		ChatType: s.ChatType,
		UserType: s.UserType,
		Settings: &apiv1.Settings{
			Timestamp: timePB,
			Settings:  settings,
		},
	}, nil
}

func (s *ChatType) AddDefault() {
	s.Settings.Merge(curDefaultSettings.chatTypesByChatType[s.ChatType])
}

type ChatTypes []ChatType

func (s *ChatTypes) AddEmptyChatTypes(keys *ChatsTypesKeys) {
	var emptyChatTypes []ChatType
	for _, ct := range keys.ChatsTypes {
		for _, ut := range keys.UsersTypes {
			if !s.isExist(ct, ut) {
				emptyChatTypes = append(emptyChatTypes, ChatType{
					ChatType: ct,
					UserType: ut,
					Settings: Settings{},
				})
			}
		}
	}
	*s = append(*s, emptyChatTypes...)
}

func (s ChatTypes) isExist(chatType chatapiv2.ChatType, userType chatapiv2.UserType) bool {
	for _, cht := range s {
		if cht.ChatType == chatType && cht.UserType == userType {
			return true
		}
	}
	return false
}

func (s ChatTypes) ToProtoV1() ([]*apiv1.ChatTypeSetting, error) {
	chatTypes := make([]*apiv1.ChatTypeSetting, len(s))
	var err error
	for i, v := range s {
		chatTypes[i], err = v.ToProtoV1()
		if err != nil {
			return nil, err
		}
	}
	return chatTypes, nil
}

func (s ChatTypes) AddDefault() {
	for _, ct := range s {
		ct.AddDefault()
	}
}
