package storage

import (
	"context"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"testing"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/pressly/goose"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/config"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/settings"
	chatapiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
	"gitlab.ozon.ru/platform/database-go/pg"
	"gitlab.ozon.ru/platform/database-go/sql"
)

var (
	migrationDir = "/../../migrations"
)

var (
	someUserID   = "123"
	someUserType = chatapiv2.UserType(1)
	someUserKey  = settings.UserKey{
		ID:   someUserID,
		Type: someUserType,
	}
	someChatID        = "00000000-0000-0000-0000-000000000000"
	someChatKey       = settings.NewChatsKey(someChatID)
	someChatType      = chatapiv2.ChatType(2)
	someSettingName   = "push"
	someSettingsNames = []string{someSettingName}
)

func dbOpts() string {
	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)

	if os.Getenv("CI") != "true" {
		cfgName := "conf"
		viper.SetConfigName(cfgName)
		viper.AddConfigPath(pathToConfigDir())

		if err := viper.ReadInConfig(); err != nil {
			log.Fatal(err)
		}
	}
	viper.AutomaticEnv()

	dbMap := viper.GetStringMapString("db")
	if dbMap == nil {
		log.Fatal("failed to read config")
	}

	port, err := strconv.Atoi(dbMap["db_port"])
	if err != nil {
		log.Fatal(err)
	}

	host, ok := dbMap["db_host"]
	if !ok {
		host = os.Getenv("CI_DB_HOST")
	}

	return connectionString(
		dbMap[string(config.DbUser)],
		dbMap[string(config.DbName)],
		host,
		dbMap["db_pass"],
		port,
	)
}

func SetupDB(t *testing.T) (*sqlx.DB, func()) {
	db, err := pg.OpenSqlx(pg.BouncerModeTransactional, dbOpts())
	if err != nil {
		log.Fatal(err)
	}

	schemaName := strings.Replace("schema_"+time.Now().Format("20060102_150405"), ".", "_", -1)
	schemaName += "_" + hashForScheme()
	if _, err = db.Exec("CREATE SCHEMA " + schemaName); err != nil {
		log.Fatal(err)
	}
	if _, err = db.Exec("SET search_path TO " + schemaName); err != nil {
		log.Fatal(err)
	}
	if err = db.Close(); err != nil {
		log.Fatal(err)
	}

	db, err = pg.OpenSqlx(pg.BouncerModeNone, dbOpts()+" search_path="+schemaName)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(pathToConfigDir() + migrationDir)
	if err := goose.Status(db.DB.DB, pathToConfigDir()+migrationDir); err != nil {
		log.Fatal(err)
	}

	if err := goose.Up(db.DB.DB, pathToConfigDir()+migrationDir); err != nil {
		log.Fatal(err)
	}
	teardown := func() {
		if t.Failed() {
			fmt.Printf("Test failed: database schema name is %s\n", schemaName)
			return
		}
		if _, err := db.Exec("DROP SCHEMA " + schemaName + " CASCADE"); err != nil {
			log.Fatal(err)
		}
		if err := db.Close(); err != nil {
			log.Fatal(err)
		}
	}
	return db.DB, teardown
}

func hashForScheme() string {
	b := make([]byte, 4)
	_, err := rand.Read(b)
	if err != nil {
		log.Fatal(err)
	}
	return fmt.Sprintf("%x", b)
}

func pathToConfigDir() string {
	_, basename, _, _ := runtime.Caller(1)

	p, err := filepath.Abs(path.Dir(basename))
	if err != nil {
		log.Fatal(err)
	}

	return p
}
func connectionString(user, name, host, pass string, port int) string {
	conn := fmt.Sprintf(
		"user=%s dbname=%s host=%s port=%d sslmode=disable",
		user,
		name,
		host,
		port,
	)
	if pass != "" {
		conn += " password=" + pass
	}
	return conn
}

type wrapperDB struct {
	sql.Balancer
	db *sqlx.DB
}

func (s *wrapperDB) Next(context.Context, sql.BalancerNodeRole) sql.Database {
	return s
}

func (s *wrapperDB) Select(ctx context.Context, dest interface{}, query string, args ...interface{}) error {
	return s.db.SelectContext(ctx, dest, query, args...)
}

func (s *wrapperDB) Get(ctx context.Context, dest interface{}, query string, args ...interface{}) error {
	return s.db.GetContext(ctx, dest, query, args...)
}

func (s *wrapperDB) Exec(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	return s.db.ExecContext(ctx, query, args...)
}

func mockGetAppName(ctx context.Context) string {
	return "some-app-name"
}

func TestPgStorage_ChatsByChatIDs(t *testing.T) {
	db, closeDB := SetupDB(t)
	defer closeDB()
	require.NoError(t, initDB(db))

	s := NewPgStorage(&wrapperDB{db: db}, mockGetAppName)

	chats, err := s.ChatsByChatIDs(context.Background(), []string{someChatID})

	require.NoError(t, err)
	require.Len(t, chats, 1)
}

func TestPgStorage_UsersByUsers(t *testing.T) {
	db, closeDB := SetupDB(t)
	defer closeDB()
	require.NoError(t, initDB(db))

	s := NewPgStorage(&wrapperDB{db: db}, mockGetAppName)

	users, err := s.UsersByUsers(context.Background(), settings.UsersKeys{settings.UserKey{ID: someUserID, Type: someUserType}})

	require.NoError(t, err)
	require.Len(t, users, 1)
}

func TestPgStorage_ChatsTypesByTypesAndUsersTypes(t *testing.T) {
	db, closeDB := SetupDB(t)
	defer closeDB()
	require.NoError(t, initDB(db))

	s := NewPgStorage(&wrapperDB{db: db}, mockGetAppName)

	chats, err := s.ChatsTypesByTypesAndUsersTypes(context.Background(), []chatapiv2.ChatType{3, 2}, []chatapiv2.UserType{1, 2})

	require.NoError(t, err)
	require.Len(t, chats, 3)
}

func TestPgStorage_UserChatsByUsers(t *testing.T) {
	db, closeDB := SetupDB(t)
	defer closeDB()
	require.NoError(t, initDB(db))

	s := NewPgStorage(&wrapperDB{db: db}, mockGetAppName)

	userChatsByUsers, err := s.UserChatsByUsers(context.Background(), settings.UsersChatIDs{
		{
			User:     settings.UserKey{ID: "123", Type: 2},
			ChatsIDs: []string{"11111111-1111-1111-1111-111111111111"},
		},
	})

	require.NoError(t, err)
	require.Equal(t, 1, len(userChatsByUsers))
}

func TestPgStorage_All(t *testing.T) {
	db, closeDB := SetupDB(t)
	defer closeDB()
	require.NoError(t, initDB(db))

	_, err := db.Exec("DELETE FROM chats_types where chat_type=2")
	require.NoError(t, err)

	user := settings.UserKey{ID: someUserID, Type: someUserType}
	s := NewPgStorage(&wrapperDB{db: db}, mockGetAppName)

	res, err := s.All(context.Background(), user, someChatID, someChatType)

	require.NoError(t, err)
	assert.Nil(t, res.ChatType)
	assert.NotNil(t, res.Chat)
	assert.NotNil(t, res.User)
	assert.NotNil(t, res.UserChat)
}

func TestPgStorage_SetChats(t *testing.T) {
	chatSettings := settings.Settings{
		"push": {Value: json.RawMessage("false"), LastUpdateAt: time.Now(), Tags: []string{"app", "web"}},
	}

	t.Run("update chats settings", func(t *testing.T) {
		db, closeDB := SetupDB(t)
		defer closeDB()
		require.NoError(t, initDB(db))

		s := NewPgStorage(&wrapperDB{db: db}, mockGetAppName)

		ts, err := s.SetChats(context.Background(), someChatKey, chatSettings)

		require.NoError(t, err)
		var chat settings.Chat
		require.NoError(t, db.QueryRowx(`SELECT * FROM chats WHERE chat_id=$1`, someChatID).StructScan(&chat))
		require.Equal(t, chatSettings["push"].Value, chat.Settings["push"].Value)
		require.True(t, ts.Before(time.Now()))
	})

	t.Run("insert chats settings", func(t *testing.T) {
		db, closeDB := SetupDB(t)
		defer closeDB()

		s := NewPgStorage(&wrapperDB{db: db}, mockGetAppName)
		chatID := settings.NewChatsKey("b9128b8e-88a6-4209-9798-8ad761bc852d")

		ts, err := s.SetChats(context.Background(), chatID, chatSettings)

		require.NoError(t, err)
		var chat settings.Chat
		require.NoError(t, db.QueryRowx(`SELECT * FROM chats WHERE chat_id=$1`, chatID.ChatID).StructScan(&chat))
		require.Equal(t, chatSettings["push"].Value, chat.Settings["push"].Value)
		var count int
		require.NoError(t, db.QueryRowx(`SELECT COUNT(*) FROM chats_history`).Scan(&count))
		require.Equal(t, 1, count)
		require.True(t, ts.Before(time.Now()))

	})
}

func TestPgStorage_SetChatsTypes(t *testing.T) {
	key := settings.ChatsTypesKey{
		ChatType: chatapiv2.ChatType_Buyer_Seller,
		UserType: someUserType,
	}
	chatSettings := settings.Settings{
		"push": {Value: json.RawMessage("false"), LastUpdateAt: time.Now(), Tags: []string{"app", "web"}},
	}

	t.Run("update chat type settings", func(t *testing.T) {
		db, closeDB := SetupDB(t)
		defer closeDB()
		require.NoError(t, initDB(db))

		s := NewPgStorage(&wrapperDB{db: db}, mockGetAppName)

		ts, err := s.SetChatsTypes(context.Background(), key, chatSettings)

		require.NoError(t, err)
		var chatType settings.ChatType
		require.NoError(t, db.QueryRowx(`SELECT * FROM chats_types WHERE chat_type=$1 AND user_type=$2`, key.ChatType, key.UserType).StructScan(&chatType))
		require.Equal(t, chatSettings["push"].Value, chatType.Settings["push"].Value)
		require.Equal(t, *chatType.LastUpdateAt.Value, ts)
	})

	t.Run("insert chat type settings", func(t *testing.T) {
		db, closeDB := SetupDB(t)
		defer closeDB()

		s := NewPgStorage(&wrapperDB{db: db}, mockGetAppName)

		ts, err := s.SetChatsTypes(context.Background(), key, chatSettings)

		require.NoError(t, err)
		var chatType settings.ChatType
		require.NoError(t, db.QueryRowx(`SELECT * FROM chats_types WHERE chat_type=$1 AND user_type=$2`, key.ChatType, key.UserType).StructScan(&chatType))
		require.Equal(t, chatSettings["push"].Value, chatType.Settings["push"].Value)
		require.Equal(t, *chatType.LastUpdateAt.Value, ts)
	})
}

func TestPgStorage_SetUsers(t *testing.T) {
	chatSettings := settings.Settings{
		"push": {Value: json.RawMessage("false"), LastUpdateAt: time.Now().UTC(), Tags: []string{"app", "web"}},
	}

	t.Run("update user settings", func(t *testing.T) {
		db, closeDB := SetupDB(t)
		defer closeDB()
		require.NoError(t, initDB(db))

		s := NewPgStorage(&wrapperDB{db: db}, mockGetAppName)

		ts, err := s.SetUsers(context.Background(), someUserKey, chatSettings)

		require.NoError(t, err)
		var user settings.User
		require.NoError(t, db.QueryRowx(`SELECT * FROM users WHERE user_id = $1 AND user_type = $2`, someUserID, someUserType).StructScan(&user))
		require.Equal(t, chatSettings["push"].Value, user.Settings["push"].Value)
		require.Equal(t, *user.LastUpdateAt.Value, ts)
	})

	t.Run("insert user settings", func(t *testing.T) {
		db, closeDB := SetupDB(t)
		defer closeDB()

		s := NewPgStorage(&wrapperDB{db: db}, mockGetAppName)

		ts, err := s.SetUsers(context.Background(), someUserKey, chatSettings)

		require.NoError(t, err)
		var users []settings.User
		require.NoError(t, db.Select(&users, `SELECT * FROM users WHERE user_id = $1 AND user_type = $2`, someUserID, someUserType))
		require.Len(t, users, 1)
		require.Equal(t, *users[0].LastUpdateAt.Value, ts)
	})
}

func TestPgStorage_SetUserChat(t *testing.T) {
	key := settings.UserChatID{
		User: settings.UserKey{
			ID:   someUserID,
			Type: someUserType,
		},
		ChatID: "00000000-0000-0000-0000-000000000000",
	}
	chatSettings := settings.Settings{
		"push": {Value: json.RawMessage(`{"Value": {"Bool": true}}`), LastUpdateAt: time.Now(), Tags: []string{"app", "web"}},
	}

	t.Run("update user chat settings", func(t *testing.T) {
		db, closeDB := SetupDB(t)
		defer closeDB()
		require.NoError(t, initDB(db))

		s := NewPgStorage(&wrapperDB{db: db}, mockGetAppName)

		ts, err := s.SetUserChat(context.Background(), key, chatSettings)

		require.NoError(t, err)
		var userChat settings.UserChat
		require.NoError(t, db.QueryRowx(`SELECT * FROM users_chats WHERE chat_id = $1 AND user_id = $2 AND user_type = $3`, key.ChatID, key.User.ID, key.User.Type).StructScan(&userChat))
		require.Equal(t, chatSettings["push"].Value, userChat.Settings["push"].Value)
		require.Equal(t, *userChat.LastUpdateAt.Value, ts)
	})

	t.Run("insert user chat settings", func(t *testing.T) {
		db, closeDB := SetupDB(t)
		defer closeDB()

		s := NewPgStorage(&wrapperDB{db: db}, mockGetAppName)

		ts, err := s.SetUserChat(context.Background(), key, chatSettings)

		require.NoError(t, err)
		var userChat settings.UserChat
		require.NoError(t, db.QueryRowx(`SELECT * FROM users_chats WHERE chat_id = $1 AND user_id = $2 AND user_type = $3`, key.ChatID, key.User.ID, key.User.Type).StructScan(&userChat))
		require.Equal(t, chatSettings["push"].Value, userChat.Settings["push"].Value)
		require.Equal(t, *userChat.LastUpdateAt.Value, ts)
	})

}

func TestPgStorage_DeleteChatSettings(t *testing.T) {
	t.Run("delete exists setting", func(t *testing.T) {
		db, closeDB := SetupDB(t)
		defer closeDB()
		require.NoError(t, initDB(db))

		s := NewPgStorage(&wrapperDB{db: db}, mockGetAppName)

		res, err := s.DeleteChatSettings(context.Background(), someChatID, []string{someSettingName})

		require.NoError(t, err)
		var chat settings.Chat
		require.NoError(t, db.Get(&chat, `SELECT * FROM chats WHERE chat_id =$1`, someChatID))
		require.Len(t, chat.Settings, 0)
		require.Equal(t, *chat.LastUpdateAt.Value, res.Timestmap)
		require.Equal(t, []string{someSettingName}, res.Names)
	})

	t.Run("delete exists setting and ignore other", func(t *testing.T) {
		db, closeDB := SetupDB(t)
		defer closeDB()
		require.NoError(t, initDB(db))

		s := NewPgStorage(&wrapperDB{db: db}, mockGetAppName)

		res, err := s.DeleteChatSettings(context.Background(), someChatID, []string{someSettingName, "other-setting"})

		require.NoError(t, err)
		var chat settings.Chat
		require.NoError(t, db.Get(&chat, `SELECT * FROM chats WHERE chat_id =$1`, someChatID))
		require.Len(t, chat.Settings, 0)
		require.Equal(t, *chat.LastUpdateAt.Value, res.Timestmap)
		require.Equal(t, []string{someSettingName}, res.Names)
	})

	t.Run("return error notFountInStorage if unexists collection", func(t *testing.T) {
		db, closeDB := SetupDB(t)
		defer closeDB()
		require.NoError(t, initDB(db))

		s := NewPgStorage(&wrapperDB{db: db}, mockGetAppName)

		_, err := s.DeleteChatSettings(context.Background(), "some-other-chat", []string{someSettingName})

		require.Equal(t, settings.NotFoundInStorage, err)
	})

	t.Run("error if not exists settings", func(t *testing.T) {
		db, closeDB := SetupDB(t)
		defer closeDB()
		require.NoError(t, initDB(db))

		s := NewPgStorage(&wrapperDB{db: db}, mockGetAppName)

		_, err := s.DeleteChatSettings(context.Background(), someChatID, []string{"other-setting"})

		require.Equal(t, settings.NotFoundInStorage, err)
	})
}

func TestPgStorage_DeleteChatTypeSettings(t *testing.T) {
	ctx, s, db, closeDB := setupPgStorage(t)
	defer closeDB()

	res, err := s.DeleteChatTypeSettings(ctx, settings.ChatsTypesKey{ChatType: 3, UserType: someUserType}, someSettingsNames)

	require.NoError(t, err)
	var chatType settings.ChatType
	require.NoError(t, db.Get(
		&chatType, `SELECT * FROM chats_types WHERE chat_type = $1 AND user_type = $2`, 3, someUserType))
	require.Len(t, chatType.Settings, 0)
	require.Equal(t, *chatType.LastUpdateAt.Value, res.Timestmap)
	require.Equal(t, []string{someSettingName}, res.Names)
}

func TestPgStorage_DeleteUserSettings(t *testing.T) {
	ctx, s, db, closeDB := setupPgStorage(t)
	defer closeDB()

	res, err := s.DeleteUserSettings(ctx, settings.UserKey{ID: someUserID, Type: someUserType}, someSettingsNames)

	require.NoError(t, err)
	var user settings.User
	require.NoError(t, db.Get(
		&user, `SELECT * FROM users WHERE user_id = $1 AND user_type = $2`, someUserID, someUserType))
	require.Len(t, user.Settings, 0)
	require.Equal(t, *user.LastUpdateAt.Value, res.Timestmap)
}

func TestPgStorage_DeleteUserChatSetting(t *testing.T) {
	ctx, s, db, closeDB := setupPgStorage(t)
	defer closeDB()

	res, err := s.DeleteUserChatSetting(ctx, settings.UserChatID{User: someUserKey, ChatID: someChatID}, someSettingsNames)

	require.NoError(t, err)
	var userChat settings.UserChat
	require.NoError(t, db.Get(
		&userChat,
		`SELECT * FROM users_chats WHERE user_id = $1 AND user_type = $2 AND chat_id = $3`,
		someUserID, someUserType, someChatID))
	require.Len(t, userChat.Settings, 0)
	require.Equal(t, *userChat.LastUpdateAt.Value, res.Timestmap)
}

func setupPgStorage(t *testing.T) (context.Context, *PgStorage, *sqlx.DB, func()) {
	db, closeDB := SetupDB(t)
	require.NoError(t, initDB(db))
	return context.Background(), NewPgStorage(&wrapperDB{db: db}, mockGetAppName), db, closeDB
}

func initDB(db *sqlx.DB) error {
	querys := []string{
		`
INSERT INTO chats (chat_id, settings)
VALUES
       ('00000000-0000-0000-0000-000000000000', '{"push": {"value": "true", "last_update_at": "2021-06-02T13:50:10.7471+03:00", "tags": ["app"]}}'),
       ('11111111-1111-1111-1111-111111111111', '{"push": {"value": "true", "last_update_at": "2021-06-02T13:50:10.7471+03:00", "tags": ["app"]}}'),
       ('b9128b8e-88a6-4209-9798-8ad761bc853d', '{}');`,
		`
INSERT INTO chats_types (chat_type, user_type, settings)
VALUES
       (3, 1, '{"push": {"value": "true", "last_update_at": "2021-06-02T13:50:10.7471+03:00", "tags": ["app"]}}'),
       (3, 2, '{"push": {"value": "true", "last_update_at": "2021-06-02T13:50:10.7471+03:00", "tags": ["app"]}}'),
       (2, 1, '{}');`,
		`
INSERT INTO users (user_id, user_type, settings)
VALUES
       ('123', 1, '{"push": {"value": "true", "last_update_at": "2021-06-02T13:50:10.7471+03:00", "tags": ["app"]}}'),
       ('aaa', 1, '{"push": {"value": "true", "last_update_at": "2021-06-02T13:50:10.7471+03:00", "tags": ["app"]}}')`,
		`
INSERT INTO users_chats (user_id, user_type, chat_id, settings)
VALUES
       ('123', 1, '00000000-0000-0000-0000-000000000000', '{"push": {"value": "true", "last_update_at": "2021-06-02T13:50:10.7471+03:00", "tags": ["app"]}}'),
       ('123', 2, '11111111-1111-1111-1111-111111111111', '{"push": {"value": "true", "last_update_at": "2021-06-02T13:50:10.7471+03:00", "tags": ["app"]}}'),
       ('124', 1, '33333333-3333-3333-3333-333333333333', '{}');`,
	}

	for _, q := range querys {
		r, err := db.Query(q)
		if err != nil {
			return err
		}
		if err := r.Close(); err != nil {
			return err
		}

	}
	return nil
}
