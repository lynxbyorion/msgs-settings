package storage

import (
	"context"
	"database/sql/driver"
	"encoding/json"
	"errors"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	"github.com/opentracing/opentracing-go"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/settings"
	chatapiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
	"gitlab.ozon.ru/platform/database-go/sql"
	"gitlab.ozon.ru/platform/database-go/sql/balancer/role"
	"gitlab.ozon.ru/platform/tracer-go/logger"
)

const (
	defaultDBDriver = "pgx"
)

type PgStorage struct {
	db              sql.Balancer
	serviceNameFunc func(context.Context) string
}

func NewPgStorage(dbConn sql.Balancer, getNameFunc func(context.Context) string) *PgStorage {
	return &PgStorage{
		db:              dbConn,
		serviceNameFunc: getNameFunc,
	}
}

func (s *PgStorage) ChatsByChatIDs(ctx context.Context, chatIDs []string) (settings.Chats, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "PgStorage.ChatsByChatIDs")
	defer span.Finish()

	q, ar, err := sqlx.In(`SELECT * from chats where chat_id IN (?)`, chatIDs)
	if err != nil {
		return nil, err
	}
	q = s.rebind(q)

	var chats settings.Chats
	if err := s.db.Next(ctx, role.Read).Select(ctx, &chats, q, ar...); err != nil {
		if err == sql.ErrNoRows {
			return settings.Chats{}, nil
		}
		return nil, err
	}

	return chats, nil
}

func (s *PgStorage) ChatsTypesByTypesAndUsersTypes(ctx context.Context, chatsTypes []chatapiv2.ChatType, usersTypes []chatapiv2.UserType) (settings.ChatTypes, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "PgStorage.ChatsTypesByTypesAndUsersTypes")
	defer span.Finish()

	const q = `SELECT * FROM chats_types WHERE chat_type = ANY($1::int[]) AND user_type = ANY($2::int[])`

	var chatsTypesSettings settings.ChatTypes
	if err := s.db.Next(ctx, role.Read).Select(ctx, &chatsTypesSettings, q, pq.Array(chatsTypes), pq.Array(usersTypes)); err != nil {
		if err == sql.ErrNoRows {
			return settings.ChatTypes{}, nil
		}
		return nil, err
	}

	return chatsTypesSettings, nil
}

func (s *PgStorage) UsersByUsers(ctx context.Context, usersKeys settings.UsersKeys) (settings.Users, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "PgStorage.UsersByUsers")
	defer span.Finish()

	const q = `
SELECT * FROM users
JOIN (SELECT unnest($1::VARCHAR(64)[]), unnest($2::SMALLINT[]))
	f(user_id, user_type) using(user_id, user_type)
`
	uIDCol, uTypeCol := usersKeysByColumns(usersKeys)

	var usersSettings settings.Users
	if err := s.db.Next(ctx, role.Read).Select(ctx, &usersSettings, q, pq.Array(uIDCol), pq.Array(uTypeCol)); err != nil {
		if err == sql.ErrNoRows {
			return settings.Users{}, nil
		}
		return nil, err
	}
	return usersSettings, nil
}

func (s *PgStorage) UserChatsByUsers(ctx context.Context, usersChatIDs settings.UsersChatIDs) (settings.UsersChats, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "PgStorage.UserChatsByUsers")
	defer span.Finish()

	query := "SELECT * FROM users_chats WHERE false"
	var allArgs []interface{}
	for _, ugs := range usersChatIDs {
		val, args, err := sqlx.In(" OR (user_id = ? AND user_type = ? AND chat_id IN(?))", ugs.User.ID, ugs.User.Type, ugs.ChatsIDs)
		if err != nil {
			return nil, err
		}

		query += val
		allArgs = append(allArgs, args...)
	}
	query = s.rebind(query)

	var usersChats []settings.UserChat
	if err := s.db.Next(ctx, role.Read).Select(ctx, &usersChats, query, allArgs...); err != nil {
		if err == sql.ErrNoRows {
			return settings.UsersChats{}, nil
		}
		return nil, err
	}

	return usersChats, nil
}

func (s *PgStorage) All(ctx context.Context, user settings.UserKey, chatID string, chatTypeKey chatapiv2.ChatType) (*settings.All, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "PgStorage.All")
	defer span.Finish()

	const q = `
SELECT json_build_object(
	'chat', (SELECT chats FROM chats WHERE chat_id=$1 LIMIT 1),
	'chat_type', (SELECT chats_types FROM chats_types WHERE chat_type=$2 AND user_type = $4 LIMIT 1),
	'user', (SELECT users FROM users WHERE user_id=$3 AND user_type = $4 LIMIT 1),
	'user_chat', (SELECT users_chats FROM users_chats WHERE user_id=$3 AND user_type = $4 AND chat_id=$1 LIMIT 1)
)
`
	var all all
	if err := s.db.Next(ctx, role.Read).Get(ctx, &all, q, chatID, chatTypeKey, user.ID, user.Type); err != nil {
		if err == sql.ErrNoRows {
			return &settings.All{}, nil
		}
		return nil, err
	}

	return all.toSettings(), nil
}

func (s *PgStorage) SetChats(ctx context.Context, key settings.ChatsKey, chatSettings settings.Settings) (time.Time, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "PgStorage.SetChats")
	defer span.Finish()

	const q = `
WITH main_func AS (
	INSERT INTO chats (chat_id, last_update_at, settings)
	VALUES ($1, NOW(), $2)
	ON CONFLICT (chat_id)
		DO UPDATE SET
		last_update_at = NOW(),
		settings = chats.settings || excluded.settings
	RETURNING chat_id, last_update_at, settings
)
INSERT INTO chats_history (chat_id, timestamp, author, action, settings)
SELECT chat_id, last_update_at, $3, 'UPDATED', settings
FROM main_func
RETURNING timestamp;`

	var ts time.Time
	err := s.db.Next(ctx, role.Write).Get(ctx, &ts, q, key.ChatID, chatSettings, s.serviceNameFunc(ctx))
	if err != nil {
		return time.Time{}, err
	}

	return ts, nil
}

func (s *PgStorage) SetChatsTypes(ctx context.Context, key settings.ChatsTypesKey, chatTypesSettings settings.Settings) (time.Time, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "PgStorage.SetChatsTypes")
	defer span.Finish()

	const q = `
WITH main_func AS (
	INSERT INTO chats_types (chat_type, user_type, last_update_at, settings)
	VALUES ($1, $2, NOW(), $3)
	ON CONFLICT (chat_type, user_type)
		DO UPDATE SET
		last_update_at = now(),
		settings = chats_types.settings || excluded.settings
	RETURNING chat_type, user_type, last_update_at, settings
 )
INSERT INTO chats_types_history (chat_type, user_type, timestamp, author, action, settings)
SELECT chat_type, user_type, last_update_at, $4, 'UPDATED', settings FROM main_func
RETURNING timestamp;`

	var ts time.Time
	err := s.db.Next(ctx, role.Write).Get(ctx, &ts, q, key.ChatType, key.UserType, chatTypesSettings, s.serviceNameFunc(ctx))
	if err != nil {
		return time.Time{}, err
	}

	return ts, nil
}

func (s *PgStorage) SetUsers(ctx context.Context, user settings.UserKey, userSettings settings.Settings) (time.Time, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "PgStorage.SetUsers")
	defer span.Finish()

	const q = `
WITH main_func AS (
	INSERT INTO users (user_id, user_type, last_update_at, settings)
	VALUES ($1, $2, NOW(), $3)
	ON CONFLICT (user_id, user_type)
		DO UPDATE SET
		last_update_at = NOW(),
		settings = users.settings || excluded.settings
	RETURNING user_id, user_type, last_update_at, settings
)
INSERT INTO users_history (user_id, user_type, timestamp, author, action, settings)
SELECT user_id, user_type, last_update_at, $4, 'UPDATED', settings FROM main_func
RETURNING timestamp;`

	var ts time.Time
	err := s.db.Next(ctx, role.Write).Get(ctx, &ts, q, user.ID, user.Type, userSettings, s.serviceNameFunc(ctx))
	if err != nil {
		return time.Time{}, err
	}

	return ts, nil
}

func (s *PgStorage) SetUserChat(ctx context.Context, key settings.UserChatID, userChatSettings settings.Settings) (time.Time, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "PgStorage.SetUserChat")
	defer span.Finish()

	const q = `
WITH main_func AS (
	INSERT INTO users_chats (user_id, user_type, chat_id, last_update_at, settings)
	VALUES ($1, $2, $3, NOW(), $4)
	ON CONFLICT (user_id, user_type, chat_id)
		DO UPDATE SET
		last_update_at = NOW(),
		settings = users_chats.settings || excluded.settings
	RETURNING user_id, user_type, chat_id, last_update_at, settings
)
INSERT INTO users_chats_history (user_id, user_type, chat_id, timestamp, author, action, settings)
SELECT user_id, user_type, chat_id, last_update_at, $5, 'UPDATED', settings FROM main_func
RETURNING timestamp;`

	var ts time.Time
	err := s.db.Next(ctx, role.Write).Get(ctx, &ts, q, key.User.ID, key.User.Type, key.ChatID, userChatSettings, s.serviceNameFunc(ctx))
	if err != nil {
		return time.Time{}, err
	}

	return ts, nil
}

func (s *PgStorage) DeleteChatSettings(ctx context.Context, chatID string, settingsKeys []string) (settings.DeleteResult, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "PgStorage.DeleteChatsByChatIDs")
	defer span.Finish()

	const q = `
WITH cur_state AS (
	select settings FROM chats WHERE chat_id=$2
),
main_func AS (
	UPDATE chats
	SET last_update_at = NOW(),
		settings = settings - $1::text[]
	WHERE chat_id = $2
	RETURNING chat_id, last_update_at, settings
)
INSERT INTO chats_history (chat_id, timestamp, author, action, settings)
SELECT chat_id, last_update_at, $3, 'DELETED', settings FROM main_func
WHERE (SELECT settings FROM cur_state) ?| $1
RETURNING timestamp, (SELECT settings FROM cur_state) AS settings;`

	return s.deleteSettings(ctx, q, settingsKeys, chatID, s.serviceNameFunc(ctx))
}

func (s *PgStorage) DeleteChatTypeSettings(ctx context.Context, key settings.ChatsTypesKey, settingsKeys []string) (settings.DeleteResult, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "PgStorage.DeleteChatTypeSettings")
	defer span.Finish()

	const q = `
WITH cur_state AS (
	select settings FROM chats_types WHERE chat_type=$2 AND user_type=$3
),
main_func AS (
	UPDATE chats_types
	SET last_update_at = NOW(),
		settings = settings - $1::text[]
	WHERE chat_type = $2 AND user_type = $3
	RETURNING chat_type, user_type, last_update_at, settings
)
INSERT INTO chats_types_history (chat_type, user_type, timestamp, author, action, settings)
SELECT chat_type, user_type, last_update_at, $4, 'DELETED', settings FROM main_func
WHERE (SELECT settings FROM cur_state) ?| $1
RETURNING timestamp, (SELECT settings FROM cur_state) AS settings;`

	return s.deleteSettings(ctx, q, settingsKeys, key.ChatType, key.UserType, s.serviceNameFunc(ctx))
}

func (s *PgStorage) DeleteUserSettings(ctx context.Context, user settings.UserKey, settingsKeys []string) (settings.DeleteResult, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "PgStorage.DeleteUserSettings")
	defer span.Finish()

	const q = `
WITH cur_state AS (
	select settings FROM users WHERE user_id=$2 AND user_type = $3
),
main_func AS (
	UPDATE users
	SET last_update_at = NOW(),
		settings = settings - $1::text[]
	WHERE user_id = $2 AND user_type = $3
	RETURNING user_id, user_type, last_update_at, settings
)
INSERT INTO users_history (user_id, user_type, timestamp, author, action, settings)
SELECT user_id, user_type, last_update_at, $4, 'DELETED', settings FROM main_func
WHERE (SELECT settings FROM cur_state) ?| $1
RETURNING timestamp, (SELECT settings FROM cur_state) AS settings;`

	return s.deleteSettings(ctx, q, settingsKeys, user.ID, user.Type, s.serviceNameFunc(ctx))
}

func (s *PgStorage) DeleteUserChatSetting(ctx context.Context, key settings.UserChatID, settingsKeys []string) (settings.DeleteResult, error) {
	span, ctx := opentracing.StartSpanFromContext(ctx, "PgStorage.DeleteUserChatSettings")
	defer span.Finish()

	const q = `
WITH cur_state AS (
	select settings FROM users_chats WHERE user_id = $2 AND user_type=$3 AND chat_id=$4
),
main_func AS (
	UPDATE users_chats
	SET last_update_at = NOW(),
		settings = settings - $1::text[]
	WHERE user_id = $2 AND user_type = $3 AND chat_id = $4
	RETURNING user_id, user_type, chat_id, last_update_at, settings
)
INSERT INTO users_chats_history (user_id, user_type, chat_id, timestamp, author, action, settings)
SELECT user_id, user_type, chat_id, last_update_at, $5, 'DELETED', settings FROM main_func
WHERE (SELECT settings FROM cur_state) ?| $1
RETURNING timestamp, (SELECT settings FROM cur_state) AS settings;`

	return s.deleteSettings(ctx, q, settingsKeys, key.User.ID, key.User.Type, key.ChatID, s.serviceNameFunc(ctx))
}

func (s *PgStorage) rebind(q string) string {
	return sqlx.Rebind(sqlx.BindType(defaultDBDriver), q)
}

func (s *PgStorage) deleteSettings(ctx context.Context, query string, settingsKeys []string, args ...interface{}) (settings.DeleteResult, error) {
	logger.Debugf(ctx, "deleteSettings query: %s", query)
	var res struct {
		Timestamp time.Time         `db:"timestamp"`
		Settings  settings.Settings `db:"settings"`
	}
	err := s.db.Next(ctx, role.Write).Get(ctx, &res, query, append([]interface{}{pq.Array(settingsKeys)}, args...)...)
	if err != nil {
		if err == sql.ErrNoRows {
			return settings.DeleteResult{}, settings.NotFoundInStorage
		}
		return settings.DeleteResult{}, err
	}

	return settings.DeleteResult{
		Timestmap: res.Timestamp,
		Names:     res.Settings.Exists(settingsKeys),
	}, nil
}

type all struct {
	Chat     *settings.Chat     `json:"chat" db:"chat"`
	ChatType *settings.ChatType `json:"chat_type" db:"chat_type"`
	User     *settings.User     `json:"user" db:"user"`
	UserChat *settings.UserChat `json:"user_chat" db:"user_chat"`
}

func (s all) toSettings() *settings.All {
	return &settings.All{
		Chat:     s.Chat,
		ChatType: s.ChatType,
		User:     s.User,
		UserChat: s.UserChat,
	}
}

func (s all) Value() (driver.Value, error) {
	bs, err := json.Marshal(s)
	if err != nil {
		return nil, err
	}
	return string(bs), nil
}

func (s *all) Scan(value interface{}) error {
	b, ok := value.([]byte)
	if !ok {
		return errors.New("type assertion to []byte failed")
	}

	return json.Unmarshal(b, &s)
}

func usersKeysByColumns(keys settings.UsersKeys) ([]string, []int64) {
	u := make([]string, len(keys))
	t := make([]int64, len(keys))
	for i, k := range keys {
		u[i] = k.ID
		t[i] = int64(k.Type)
	}
	return u, t
}
