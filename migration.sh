#!/bin/sh

if [ "${STAGE}" = "development" ]; then
  exit 0
fi

MIGRATION_DIR=/migrations
DB_PASS_PATH="/var/run/secrets/pg__messenger-settings__password"

if [ "${STAGE}" = "staging" ]; then
    WARDEN_HOST="warden.platform.stg.s.o3.ru"
elif [ "${STAGE}" = "production" ]; then
    WARDEN_HOST="warden.platform.prod.s.o3.ru"
fi

if [ -f ${DB_PASS_PATH} ]; then
    PASSWORD=$(cat ${DB_PASS_PATH})
fi

PG_ADDRESS=$(wget -cq ${WARDEN_HOST}/endpoints\?service=messenger-settings.pg:bouncer -O - | jq '.[] | select(.Role == "master").Address')
HOST_DB=$(echo "${PG_ADDRESS}" | jq -r '. | split(":")[0]')
PORT_DB=$(echo "${PG_ADDRESS}" | jq -r '. | split(":")[1]')

DB_DSN="user=messenger-settings-user password=${PASSWORD} dbname=messenger-settings host=${HOST_DB}  port=${PORT_DB}  sslmode=disable  binary_parameters=yes"

if [ "$1" = "--dryrun" ]; then
    MIGRATION_ACTION="status"
else
    MIGRATION_ACTION="up"
fi

name=$(echo $DSN | sed -E 's/.*dbname\=([^ ]+).*/\1/')
echo "======== Begin migrating messenger-settings ($HOST_DB:$PORT_DB) ========"
goose -dir "${MIGRATION_DIR}" postgres "${DB_DSN}" "$MIGRATION_ACTION" || exit 1
echo "========   End migrating messenger-settings ($HOST_DB:$PORT_DB) ========"
