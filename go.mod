module gitlab.ozon.ru/messenger/messenger-settings

go 1.16

require (
	github.com/Shopify/sarama v1.30.0
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.6.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/lib/pq v1.10.3
	github.com/opentracing/opentracing-go v1.2.0
	github.com/pborman/uuid v1.2.1
	github.com/pressly/goose v2.7.0+incompatible
	github.com/spf13/viper v1.9.0
	github.com/stretchr/testify v1.7.0
	gitlab.ozon.ru/messenger/messenger-settings/pkg/api v0.0.0-00010101000000-000000000000
	gitlab.ozon.ru/platform/chat-api/pkg/api v1.1.11
	gitlab.ozon.ru/platform/database-go v1.4.5
	gitlab.ozon.ru/platform/database-go/entrypoint v1.7.9
	gitlab.ozon.ru/platform/kafka-client-go v0.4.2
	gitlab.ozon.ru/platform/realtime-config-go v1.12.3
	gitlab.ozon.ru/platform/scratch v1.10.16
	gitlab.ozon.ru/platform/tracer-go v1.23.7
	google.golang.org/grpc v1.41.0
	google.golang.org/protobuf v1.27.1
)

replace gitlab.ozon.ru/messenger/messenger-settings/pkg/api => ./pkg/api
