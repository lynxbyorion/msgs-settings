-- +goose Up
-- SQL in this section is executed when the migration is applied.

DELETE FROM chats;
DELETE FROM chats_types;
DELETE FROM users;
DELETE FROM users_chats;

DELETE FROM chats_history;
DELETE FROM chats_types_history;
DELETE FROM users_history;
DELETE FROM users_chats_history;

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.

