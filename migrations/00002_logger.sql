-- +goose Up
-- SQL in this section is executed when the migration is applied.

CREATE TABLE chats_history
(
    "id"        BIGSERIAL PRIMARY KEY,

    "chat_id"   TEXT                        NOT NULL,

    "timestamp" TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    "author"    TEXT,
    "action"    TEXT                        NOT NULL,
    "settings"  JSONB                       NOT NULL
);
CREATE INDEX chats_timestamp_idx ON chats_history ("chat_id", "timestamp");

CREATE TABLE chats_types_history
(
    "id"        BIGSERIAL PRIMARY KEY,

    "chat_type" SMALLINT                    NOT NULL,
    "user_type" SMALLINT                    NOT NULL,

    "timestamp" TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    "author"    TEXT,
    "action"    TEXT                        NOT NULL,
    "settings"  JSONB                       NOT NULL
);
CREATE INDEX chats_types_timestamp_idx ON chats_types_history ("chat_type", "user_type", "timestamp");

CREATE TABLE users_history
(
    "id"        BIGSERIAL PRIMARY KEY,

    "user_id"   TEXT                        NOT NULL,
    "user_type" SMALLINT                    NOT NULL,

    "timestamp" TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    "author"    TEXT,
    "action"    TEXT                        NOT NULL,
    "settings"  JSONB                       NOT NULL
);
CREATE INDEX users_timestamp_idx ON users_history ("user_id", "user_type", "timestamp");

CREATE TABLE users_chats_history
(
    "id"        BIGSERIAL PRIMARY KEY,

    "user_id"   TEXT                        NOT NULL,
    "user_type" SMALLINT                    NOT NULL,
    "chat_id"   TEXT                        NOT NULL,

    "timestamp" TIMESTAMP WITHOUT TIME ZONE NOT NULL,
    "author"    TEXT,
    "action"    TEXT                        NOT NULL,
    "settings"  JSONB                       NOT NULL
);
CREATE INDEX users_chats_timestamp_idx ON users_chats_history ("user_id", "user_type", "chat_id", "timestamp");

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.

DROP TABLE chats_history;
DROP TABLE chats_types_history;
DROP TABLE users_history;
DROP TABLE users_chats_history;
