-- +goose Up
-- SQL in this section is executed when the migration is applied.

CREATE TABLE IF NOT EXISTS chats
(
    "id"             BIGSERIAL PRIMARY KEY,

    "chat_id"        TEXT    NOT NULL UNIQUE,
    "last_update_at" TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    "settings"       JSONB    NOT NULL DEFAULT '{}'
);

CREATE TABLE IF NOT EXISTS chats_types
(
    "id"             BIGSERIAL PRIMARY KEY,

    chat_type      SMALLINT NOT NULL,
    user_type      SMALLINT NOT NULL,
    UNIQUE (chat_type, user_type),
    "last_update_at" TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    "settings"       JSONB       NOT NULL DEFAULT '{}'
);

CREATE TABLE IF NOT EXISTS users
(
    "id"             BIGSERIAL PRIMARY KEY,

    user_id          VARCHAR(64) NOT NULL,
    user_type        SMALLINT NOT NULL,
    UNIQUE (user_id, user_type),
    "last_update_at" TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    "settings"       JSONB       NOT NULL DEFAULT '{}'
);

CREATE TABLE IF NOT EXISTS users_chats
(
    "id"             BIGSERIAL PRIMARY KEY,

    user_id          VARCHAR(64) NOT NULL,
    user_type        SMALLINT NOT NULL,
    "chat_id"        TEXT        NOT NULL,
    UNIQUE (user_id, user_type, "chat_id"),
    "last_update_at" TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT NOW(),
    "settings"       JSONB       NOT NULL DEFAULT '{}'
);

CREATE UNIQUE INDEX IF NOT EXISTS
chats_chat_id_idx ON chats (chat_id);

CREATE UNIQUE INDEX IF NOT EXISTS
chats_types_type_and_user_type_idx ON chats_types (chat_type, user_type);

CREATE UNIQUE INDEX IF NOT EXISTS
users_user_idx ON users (user_id, user_type);

CREATE UNIQUE INDEX IF NOT EXISTS
users_chats_user_idx ON users_chats (user_id, user_type);

CREATE UNIQUE INDEX IF NOT EXISTS
users_chats_user_chat_id_idx ON users_chats (user_id, user_type, chat_id);

-- +goose Down
-- SQL in this section is executed when the migration is rolled back.

DROP TABLE chats;
DROP TABLE chats_types;
DROP TABLE users;
DROP TABLE users_chats;



