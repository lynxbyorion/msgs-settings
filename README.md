# Сервис настроек платформы ozon-messenger

Сервис поддерживает работу с четырмя типами коллекций настроек:
 - по чату
 - по типу чата
 - пользователю
 - пользователю в чате

Запись в каждой коллекции может хранить неограниченное кол-во _типов настроек_. Все коллекции друг от друга независимы.

## Манипуляции с настройками

Настройки можно добавлять, удалять изменять.

 - по чату
 ```
    rpc GetChats (GetChatsRequest) returns (GetChatsResponse) {}
    rpc SetChats (SetChatsRequest) returns (SetChatsResponse) {}
    rpc DeleteChatSettings(DeleteChatSettingsRequest) returns (DeleteChatSettingsResponse) {}
 ```
 - по типу чата
 ```
    rpc GetChatsTypes (GetChatsTypesRequest) returns (GetChatsTypesResponse) {}
    rpc SetChatsTypes (SetChatsTypesRequest) returns (SetChatsTypesResponse) {}
    rpc DeleteChatTypeSettings(DeleteChatTypeSettingsRequest) returns (DeleteChatTypeSettingsResponse) {}
 ```
 - пользователю
 ```
    rpc GetUsers (GetUsersRequest) returns (GetUsersResponse) {}
    rpc SetUsers (SetUsersRequest) returns (SetUsersResponse) {}
    rpc DeleteUserSettings(DeleteUserSettingsRequest) returns (DeleteUserSettingsResponse) {}
 ```
 - пользователю в чате
 ```
    rpc GetUsersChats (GetUsersChatsRequest) returns (GetUsersChatsResponse) {}
    rpc SetUsersChats (SetUsersChatsRequest) returns (SetUsersChatsResponse) {}
    rpc DeleteUsersChatsSettings(DeleteUsersChatsSettingsRequest) returns (DeleteUsersChatsSettingsResponse) {}
 ```


Так же можно получить сразу из всех коллекций настроек записи:
```
    rpc GetAll (GetAllRequest) returns (GetAllResponse) {}
```


## Типы настроек

Поддерживаемые типы настроек описаны в сообщении `Value` в файле `api/v1/api.proto`.


## Настройки по умолчанию

Сервис поддерживает возможность настроек по умолчанию. Т.е. при запросе настроек всегда будут отдаваться настройки по умолчанию если только они не были изменены явно. Удаление настройки на настройки по умолчанию не влияют.

Настройки по умолчанию устанавливаются в коде приложения в файле `internal/settings/defaults.go`.

## Уведомления об изменении записи в коллекции настроек

При добавлении, изменении или удалении настроек или группы нестроек в Кафку в топик `messenger_settings_notification` будет отправлено уведомление об изменениях. Контракт сообщения описан в файле `api/events_v1/events.proto`.

При изменении или добавлении настроек сообщение будет содержать новые настройки, при удалении, в сообщении будет перечень названий настроек, которые были удалены.

## История изменений

Добавление, изменение или удаление настроек фиксируется в БД. В таблицу `history` записываются новое состояние `incoming` и состояние на данный момент в `current_state`.
