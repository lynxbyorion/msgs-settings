include scratch.mk

# start postgres container and run migrations
DB_HOST := localhost
DB_NAME := messenger-settings
DB_USER := messenger-settings-admin
DB_PASS := test
DB_PORT := 54323

.PHONY: init-db
init-db:
	@docker run -d --name ${DB_NAME} -e POSTGRES_DB=${DB_NAME} -e POSTGRES_USER=${DB_USER} -e POSTGRES_PASSWORD=${DB_PASS} -p ${DB_PORT}:5432  postgres:11 postgres -c log_statement=all
	#	Wait 2 second (started postgresql)...
	@sleep 2

	@cd migrations && goose postgres "user=${DB_USER} dbname=${DB_NAME} password=${DB_PASS} host=${DB_HOST} port=${DB_PORT} sslmode=disable" up

.PHONY: drop-db
drop-db:
	@docker stop ${DB_NAME}; docker rm ${DB_NAME}

test-with-e2e:
	go test ./... --tags=e2e
