package main

import (
	"context"
	"strings"
	"time"

	settings "gitlab.ozon.ru/messenger/messenger-settings/internal/app/messenger_settings/v1"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/config"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/kafka"
	"gitlab.ozon.ru/messenger/messenger-settings/internal/storage"
	"gitlab.ozon.ru/platform/scratch"
	_ "gitlab.ozon.ru/platform/scratch/app/pflag"
	"gitlab.ozon.ru/platform/scratch/config/secret"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/grpc/metadata"
)

func main() {
	ctx := context.Background()

	pass := secret.GetValue(ctx, "pg__messenger-settings__password").String()
	if pass == "" {
		logger.Fatalf(ctx, "failed getting password")
	}

	dbConn, err := newDB(pgConnOpts{
		Host: config.GetValue(ctx, config.DbHost).String(),
		Port: config.GetValue(ctx, config.DbPort).Int(),
		Name: config.GetValue(ctx, config.DbName).String(),
		User: config.GetValue(ctx, config.DbUser).String(),
		Pass: pass,

		MaxOpenConns: config.GetValue(ctx, config.MaxOpenConns).Int(),
		MaxIdleConns: config.GetValue(ctx, config.MaxIdleConns).Int(),
	})
	if err != nil {
		logger.Fatalf(ctx, "failed init db err=%v", err)
	}

	a, err := scratch.New()
	if err != nil {
		logger.Fatalf(ctx, "can't create app: %s", err)
	}

	pgStorage := storage.NewPgStorage(dbConn, getAppName)
	producer, err := kafka.NewProducer(
		config.GetValue(ctx, config.TopicName).String(),
		strings.Split(config.GetValue(ctx, config.KafkaEndpoints).String(), ","),
	)
	if err != nil {
		logger.Fatalf(ctx, "failed init producer service err=%v", err)
	}

	if err := a.Run(settings.NewSettingsV1(pgStorage, producer)); err != nil {
		logger.Fatalf(context.Background(), "can't run app: %s", err)
	}

	go func() {
		<-time.After(5 * time.Second)
		logger.Fatalf(ctx, "failed gracefully shutdown")
	}()
	if err := producer.Stop(); err != nil {
		logger.Error(ctx, "failed stop producer err=%v", err)
	}
}

func getAppName(ctx context.Context) string {
	md, ok := metadata.FromIncomingContext(ctx)
	if ok {

		v, ok := md["x-o3-service-name"]
		if ok && len(v) != 0 {
			return v[0]
		}
	}
	return ""
}
