package main

import (
	"fmt"

	"gitlab.ozon.ru/platform/database-go/entrypoint"
	"gitlab.ozon.ru/platform/database-go/sql"
	"gitlab.ozon.ru/platform/database-go/sql/balancer/role"
	"gitlab.ozon.ru/platform/scratch/closer"
)

type pgConnOpts struct {
	Name string
	User string
	Pass string
	Host string
	Port int

	MaxOpenConns int
	MaxIdleConns int
}

func newDB(opts pgConnOpts) (sql.Balancer, error) {
	var bal sql.Balancer
	var err error
	if opts.Host != "" {
		bal, err = entrypoint.New(fmt.Sprintf("user=%s dbname=%s password=%s host=%s port=%d sslmode=disable",
			opts.User,
			opts.Name,
			opts.Pass,
			opts.Host,
			opts.Port,
		))
	} else {
		bal, err = entrypoint.New(
			fmt.Sprintf("o3:///%s.pg:%s?user=%s&password=%s&dbname=%[1]s",
				opts.Name, entrypoint.PortBouncer, opts.User, opts.Pass),
			entrypoint.WithMasterMappingRoles(role.Write, role.Read),
			entrypoint.WithMaxOpenConnections(opts.MaxOpenConns),
			entrypoint.WithMaxIdleConnections(opts.MaxIdleConns),
		)
	}
	if err != nil {
		return nil, err
	}

	closer.Add(bal.Close)

	return bal, nil
}
