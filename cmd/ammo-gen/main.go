package main

import (
	"context"
	"crypto/rand"
	mrand "math/rand"
	"sync"
	"time"

	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	chatapiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
	mw "gitlab.ozon.ru/platform/scratch/pkg/mw/grpc"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/grpc"
)

const (
	prefix = "test_"

	boolSettingKey    = "bool-setting"
	intSettingKey     = "int-setting"
	floatSettingKey   = "float-setting"
	stringSettingKey  = "string-setting"
	stringsSettingKey = "strings-setting"
)

func client(ctx context.Context) apiv1.SettingsV1Client {
	settingsConn, err := mw.Dial("messenger-settings-latest.stg.a.o3.ru:82", grpc.WithInsecure())
	//settingsConn, err := mw.Dial("localhost:7002", grpc.WithInsecure())
	if err != nil {
		logger.Fatalf(ctx, "can't create connection err=%v", err)
	}
	return apiv1.NewSettingsV1Client(settingsConn)
}

var (
	settings = map[string]*apiv1.SetSetting{
		boolSettingKey: {
			Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}},
			Tags:  []string{"test-web", "test-mob"},
		},
		intSettingKey: {
			Value: &apiv1.Value{Value: &apiv1.Value_Int{Int: 42}},
			Tags:  []string{"test-web", "test-mob"},
		},
		floatSettingKey: {
			Value: &apiv1.Value{Value: &apiv1.Value_Float{Float: 42.2}},
			Tags:  []string{"test-web", "test-mob"},
		},
		stringSettingKey: {
			Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "string1"}},
			Tags:  []string{"test-web", "test-mob"},
		},
		stringsSettingKey: {
			Value: &apiv1.Value{Value: &apiv1.Value_Strings_{Strings: &apiv1.Value_Strings{Values: []string{"string1", "string2"}}}},
			Tags:  []string{"test-web", "test-mob"},
		},
	}
)

func main() {
	mrand.Seed(time.Now().Unix())
	ctx := context.Background()

	c := client(ctx)

	keys := newKeys()

	for i := 0; i < 10; i++ {
		if _, err := c.SetUsers(ctx, &apiv1.SetUsersRequest{
			User:     keys.users[i],
			Settings: settings,
		}); err != nil {
			logger.Errorf(ctx, err.Error())
		}

		if _, err := c.SetUsersChats(ctx, &apiv1.SetUsersChatsRequest{
			ChatId:   keys.chats[i].Id,
			User:     keys.users[i],
			Settings: settings,
		}); err != nil {
			logger.Error(ctx, err.Error())
		}

		if _, err := c.SetChatsTypes(ctx, &apiv1.SetChatsTypesRequest{
			ChatType: keys.getRandChat().Type,
			UserType: keys.getRandUser().Type,
			Settings: settings,
		}); err != nil {
			logger.Error(ctx, err.Error())
		}

		if _, err := c.SetChats(ctx, &apiv1.SetChatsRequest{
			ChatId:   keys.getRandChat().Id,
			Settings: settings,
		}); err != nil {
			logger.Error(ctx, err.Error())
		}

		for i := 0; i < 100; i++ {
			wg := sync.WaitGroup{}

			wg.Add(1)
			go func() {
				if _, err := c.GetUsers(ctx, &apiv1.GetUsersRequest{Users: keys.getRandNUsers(10)}); err != nil {
					logger.Error(ctx, err.Error())
				}
				wg.Done()
			}()

			wg.Add(1)
			go func() {
				uchs := make([]*apiv1.GetUsersChatsRequest_UserChatsIDs, 10)
				for i := range uchs {
					uchs[i] = &apiv1.GetUsersChatsRequest_UserChatsIDs{
						User:     keys.getRandUser(),
						ChatsIds: []string{keys.getRandChat().Id},
					}
				}

				if _, err := c.GetUsersChats(ctx, &apiv1.GetUsersChatsRequest{UsersChatsIds: uchs}); err != nil {
					logger.Error(ctx, err.Error())
				}
				wg.Done()
			}()

			wg.Add(1)
			go func() {
				defer wg.Done()
				ch := keys.getRandChat()
				if _, err := c.GetAll(ctx, &apiv1.GetAllRequest{
					User:     keys.getRandUser(),
					ChatId:   ch.Id,
					ChatType: ch.Type,
				}); err != nil {
					logger.Error(ctx, err.Error())
				}
			}()

			wg.Add(1)
			go func() {
				defer wg.Done()
				ct := make([]chatapiv2.ChatType, 5)
				for i := range ct {
					ct[i] = keys.getRandChat().Type
				}

				ut := make([]chatapiv2.UserType, 5)
				for i := range ut {
					ut[i] = keys.getRandUser().Type
				}

				_, err := c.GetChatsTypes(ctx, &apiv1.GetChatsTypesRequest{
					ChatsTypes: ct,
					UsersTypes: ut,
				})
				if err != nil {
					logger.Error(ctx, err.Error())
				}
			}()

			go func() {
				ids := make([]string, 30)
				for i := range ids {
					ids[i] = keys.getRandChat().Id
				}
				_, err := c.GetChats(ctx, &apiv1.GetChatsRequest{ChatsIds: ids})
				if err != nil {
					logger.Error(ctx, err.Error())
				}
			}()

			wg.Wait()
		}
	}
}

type keys struct {
	users []*chatapiv2.User
	chats []*chatapiv2.Chat
}

func (k keys) getRandUser() *chatapiv2.User {
	return k.users[mrand.Intn(len(k.users))] // #nosec
}

func (k keys) getRandNUsers(count int) []*chatapiv2.User {
	us := make([]*chatapiv2.User, count)
	for i := range us {
		us[i] = k.getRandUser()
	}
	return us
}

func (k keys) getRandChat() *chatapiv2.Chat {
	return k.chats[mrand.Intn(len(k.chats))] //#nosec
}

func newKeys() keys {
	return keys{
		users: users(10),
		chats: chats(30),
	}
}

func users(count int) []*chatapiv2.User {
	hashes := genHashes(count)
	users := make([]*chatapiv2.User, len(hashes))
	for i, h := range hashes {
		users[i] = &chatapiv2.User{
			Id:   prefix + h,
			Type: genUserType(),
		}
	}
	return users
}

func chats(count int) []*chatapiv2.Chat {
	hs := genHashes(count)
	ids := make([]*chatapiv2.Chat, len(hs))
	for i, h := range hs {
		ids[i] = &chatapiv2.Chat{
			Type: getChatType(),
			Id:   prefix + h,
		}
	}
	return ids
}

func genHashes(count int) []string {
	res := make([]string, count)
	for i := 0; i < count; i++ {
		r, err := rand.Prime(rand.Reader, 64)
		if err != nil {
			panic(-1)
		}
		res[i] = r.String()
	}
	return res
}

var (
	userTypes = []chatapiv2.UserType{
		chatapiv2.UserType_Customer,
		chatapiv2.UserType_Seller,
		chatapiv2.UserType_Support,
		chatapiv2.UserType_ChatBot,
		chatapiv2.UserType_Principal,
		chatapiv2.UserType_OperatorTPVZ,
	}

	chatTypes = []chatapiv2.ChatType{
		chatapiv2.ChatType_Buyer_Support,
		chatapiv2.ChatType_Buyer_Buyer,
		chatapiv2.ChatType_Buyer_Seller,
		chatapiv2.ChatType_Seller_Support,
		chatapiv2.ChatType_Buyer_Travel,
		chatapiv2.ChatType_OProfit,
		chatapiv2.ChatType_Customer_Customer,
		chatapiv2.ChatType_Principal_Support,
		chatapiv2.ChatType_TurboPVZ_Support,
	}
)

func genUserType() chatapiv2.UserType {
	return userTypes[mrand.Intn(len(userTypes))] // #nosec
}

func getChatType() chatapiv2.ChatType {
	return chatTypes[mrand.Intn(len(chatTypes))] // #nosec
}
