package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"gitlab.ozon.ru/messenger/messenger-settings/internal/kafka"
)

var (
	kafkaClient = "test-client"
	topic       = "messenge"
	brokers     = []string{"stgkafkabroker1.s.o3.ru:9092", "stgkafkabroker2.s.o3.ru:9092", "stgkafkabroker3.s.o3.ru:9092", "stgkafkabroker4.s.o3.ru:9092", "stgkafkabroker5.s.o3.ru:9092"}
)

func main() {
	ctx := context.Background()

	cg, err := kafka.NewConsumerGroup(kafkaClient, topic, brokers)
	if err != nil {
		panic(err)
	}

	stop := make(chan struct{})
	msgsChan := kafka.ListenTopic(ctx, []string{topic}, cg, stop)

	for {
		select {
		case m := <-msgsChan:
			b, err := json.MarshalIndent(m, "", "  ")
			if err != nil {
				panic(err)
			}
			fmt.Println(string(b) + "\n")
		case <-sigTerm():
			return
		}
	}
}

func sigTerm() <-chan os.Signal {
	gracefulStop := make(chan os.Signal, 1)
	signal.Notify(gracefulStop, os.Interrupt, os.Signal(syscall.SIGTERM))
	return gracefulStop
}
