package main

import (
	"context"
	"fmt"
	"log"

	apiv1 "gitlab.ozon.ru/messenger/messenger-settings/pkg/api/v1"
	chat_apiv2 "gitlab.ozon.ru/platform/chat-api/pkg/api/v2"
	mw "gitlab.ozon.ru/platform/scratch/pkg/mw/grpc"
	"gitlab.ozon.ru/platform/tracer-go/logger"
	"google.golang.org/grpc"
)

type ChatTypeKey struct {
	ChatType chat_apiv2.ChatType
	UserType chat_apiv2.UserType
}

var chatTypeData = map[ChatTypeKey]map[string]*apiv1.SetSetting{
	{
		ChatType: chat_apiv2.ChatType_Buyer_Support,
		UserType: chat_apiv2.UserType_Customer,
	}: {
		"chat_title":    {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "Поддержка Ozon"}}},
		"chat_subtitle": {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "Онлайн 24 часа"}}},
		//"chat_avatar_icon": {
		//	Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "https://cdn-stg.ozonru.me/s3/fs-chat-api/cfdf1373-4ee4-11ec-bd02-06a332735108.png"}},
		//},
		"chat_header_background_mobile": {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "#5C96FF"}}},
		"chat_is_official":              {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}},
		"chat_show_author_on_message":   {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}},
		"push_title":                    {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "Поддержка Ozon"}}},
	},

	{
		ChatType: chat_apiv2.ChatType_Buyer_Seller,
		UserType: chat_apiv2.UserType_Customer,
	}: {
		"chat_header_background_mobile": {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "#96A3AE"}}},
		"chat_show_author_on_message":   {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: false}}},
	},

	{
		ChatType: chat_apiv2.ChatType_Seller_Support,
		UserType: chat_apiv2.UserType_Customer,
	}: {},

	{
		ChatType: chat_apiv2.ChatType_Buyer_Travel,
		UserType: chat_apiv2.UserType_Customer,
	}: {
		"chat_title":    {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "Поддержка Ozon Travel"}}},
		"chat_subtitle": {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "Онлайн 24 часа"}}},
		//"chat_avatar_icon": {
		//	Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "https://cdn-stg.ozonru.me/s3/fs-chat-api/868fadda-4fc7-11ec-b486-568b508270f0.png"}},
		//},
		"chat_is_official":              {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}},
		"chat_header_background_mobile": {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "#5C96FF"}}},
		"chat_show_author_on_message":   {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}},
		"push_title":                    {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "Поддержка Ozon Travel"}}},
	},

	{
		ChatType: chat_apiv2.ChatType_OProfit,
		UserType: chat_apiv2.UserType_Customer,
	}: {
		"chat_title":    {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "Поддержка OProfit"}}},
		"chat_subtitle": {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "Онлайн 24 часа"}}},
		//"chat_avatar_icon": {
		//	Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "https://cdn-stg.ozonru.me/s3/fs-chat-api/94501adb-7905-11ec-ae05-1e502c919f1f.png"}},
		//},
		"chat_is_official":              {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}},
		"chat_header_background_mobile": {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "#5C96FF"}}},
		"chat_show_author_on_message":   {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}},
		"push_title":                    {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "Поддержка OProfit"}}},
	},

	{
		ChatType: chat_apiv2.ChatType_Customer_Customer,
		UserType: chat_apiv2.UserType_Customer,
	}: {
		"chat_header_background_mobile": {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "#3D5165"}}},
		"chat_show_author_on_message":   {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: false}}},
	},

	{
		ChatType: chat_apiv2.ChatType_Notification_Orders,
		UserType: chat_apiv2.UserType_Customer,
	}: {
		"chat_title":    {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "Заказы"}}},
		"chat_subtitle": {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "Изменения статусов заказов и возвратов"}}},
		//"chat_avatar_icon": {
		//	Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "https://cdn1.ozone.ru/s3/fs-chat-api/d0a9f87f-785f-11ec-8241-9a13529e2c7a.png"}},
		//},
		"chat_is_official":            {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}},
		"chat_footer_input_invisible": {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}},
		"chat_show_author_on_message": {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: false}}},
		"push_enable":                 {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: false}}},
	},

	{
		ChatType: chat_apiv2.ChatType_Notification_Subscriptions,
		UserType: chat_apiv2.UserType_Customer,
	}: {
		"push_enable":   {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: false}}},
		"chat_title":    {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "Мои подписки"}}},
		"chat_subtitle": {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "Персональные предложения, подарки, отзывы"}}},
		//"chat_avatar_icon": {
		//	Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "https://cdn1.ozone.ru/s3/fs-chat-api/d0c85dd1-785f-11ec-82a9-c2d164ca701e.png"}},
		//},
		"chat_is_official":              {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}},
		"chat_footer_input_invisible":   {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}},
		"chat_show_author_on_message":   {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: false}}},
		"chat_header_background_mobile": {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "#F5A70A"}}},
	},

	{
		ChatType: chat_apiv2.ChatType_Notification_Promo,
		UserType: chat_apiv2.UserType_Customer,
	}: {
		"push_enable":   {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: false}}},
		"chat_title":    {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "Скидки и акции"}}},
		"chat_subtitle": {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "Лучшие скидки для вас"}}},
		//"chat_avatar_icon": {
		//	Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "https://cdn1.ozone.ru/s3/fs-chat-api/d0aa369b-785f-11ec-8241-9a13529e2c7a.png"}},
		//},
		"chat_is_official":              {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}},
		"chat_footer_input_invisible":   {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}},
		"chat_show_author_on_message":   {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: false}}},
		"chat_header_background_mobile": {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "#8A82F6"}}},
	},

	{
		ChatType: chat_apiv2.ChatType_Notification_Questions,
		UserType: chat_apiv2.UserType_Customer,
	}: {
		"push_enable":   {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: false}}},
		"chat_title":    {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "Вопросы и ответы по товарам"}}},
		"chat_subtitle": {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: ""}}},
		//"chat_avatar_icon": {
		//	Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "https://cdn1.ozone.ru/s3/fs-chat-api/70db975d-7934-11ec-8241-9a13529e2c7a.png"}},
		//},
		"chat_is_official":              {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}},
		"chat_footer_input_invisible":   {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: true}}},
		"chat_show_author_on_message":   {Value: &apiv1.Value{Value: &apiv1.Value_Bool{Bool: false}}},
		"chat_header_background_mobile": {Value: &apiv1.Value{Value: &apiv1.Value_String_{String_: "#2E78FF"}}},
	},
}

func main() {
	ctx := context.Background()
	c := client(ctx)
	//setChatsTypes(ctx, c)

	checkChatsTypes(ctx, c)
}

// nolint
func setChatsTypes(ctx context.Context, client apiv1.SettingsV1Client) {
	for k, v := range chatTypeData {
		_, err := client.SetChatsTypes(ctx, &apiv1.SetChatsTypesRequest{
			ChatType: k.ChatType,
			UserType: k.UserType,
			Settings: v,
		})
		if err != nil {
			logger.Fatalf(ctx, "setting.SetChatsTypes(key=%+v): %s", k, err)
		}
	}
}

func checkChatsTypes(ctx context.Context, client apiv1.SettingsV1Client) {
	for k, v := range chatTypeData {
		fmt.Println(k.ChatType, k.UserType)
		res, err := client.GetChatsTypes(ctx, &apiv1.GetChatsTypesRequest{
			ChatsTypes: []chat_apiv2.ChatType{k.ChatType},
			UsersTypes: []chat_apiv2.UserType{k.UserType},
		})
		if err != nil {
			logger.Fatalf(ctx, "setting.SetChatsTypes(key=%+v): %s", k, err)
		}

		if len(res.ChatTypesSettings) != 1 {
			log.Fatalf("unknown result: %+v", res)
		}

		settings := res.ChatTypesSettings[0].Settings.Settings
		for name, expSet := range v {
			actSet, ok := settings[name]
			if !ok {
				log.Fatalf("setting for %v:%v not found: name=%v", k.ChatType, k.UserType, name)
			}
			if actSet.Value.String() != expSet.Value.String() {
				log.Fatalf("failde %v:%v %v\n\tgot: %v\n\twant: %v", k.ChatType, k.UserType, name, actSet.Value.String(), expSet.Value.String())
			}
			fmt.Println(name, "DONE")
		}
	}
}

func client(ctx context.Context) apiv1.SettingsV1Client {
	//settingsConn, err := mw.Dial("messenger-settings.prod.a.o3.ru:82", grpc.WithInsecure())
	settingsConn, err := mw.Dial("localhost:7002", grpc.WithInsecure())
	if err != nil {
		logger.Fatalf(ctx, "can't create connection err=%v", err)
	}
	return apiv1.NewSettingsV1Client(settingsConn)
}
